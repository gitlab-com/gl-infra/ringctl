/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/ci"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

var ciGenerateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate a ring deployment pipeline",
	RunE: func(cmd *cobra.Command, args []string) error {
		var onlyGitlabUpgrade bool
		var ring tissue.Ring

		cluster, err := getAmpCluster()
		if err != nil {
			return err
		}

		storage := tissue.LocalDiskStorage{}

		patchID, err := cmd.Flags().GetString("patch-id")
		if err != nil {
			return fmt.Errorf("patch-id parameter error: %w", err)
		}

		var patchMetadata *patch.Metadata
		if patchID == "" {
			slog.LogAttrs(cmd.Context(),
				slog.LevelDebug,
				"patch-id not provided, not reading metadata",
			)

			ring, err = getRing(cmd)
			if err != nil {
				return err
			}

			onlyGitlabUpgrade, err = cmd.Flags().GetBool("only-gitlab-upgrade")
			if err != nil {
				return fmt.Errorf("only-gitlab-upgrade parameter error: %w", err)
			}

		} else {
			slog.LogAttrs(cmd.Context(),
				slog.LevelInfo,
				"patch-id provided, using it to govern generated pipeline",
				slog.String("patch-id", patchID),
			)

			patchMetadata, err = storage.GetPatch(cmd.Context(), cluster, patchID)
			if err != nil {
				failure(cmd.Context(), "patch retrieval failed", err, slog.String("patch_id", patchID))

				return nil
			}

			onlyGitlabUpgrade = patchMetadata.OnlyGitlabUpgrade
			ring = cluster.Ring(patchMetadata.Ring)

			slog.LogAttrs(cmd.Context(),
				slog.LevelDebug,
				"patch info",
				slog.String("patch-id", patchMetadata.ID),
				slog.Int("priority", patchMetadata.Priority),
				slog.Int("ring", patchMetadata.Ring),
				slog.Any("pause-after-ring", patchMetadata.PauseAfterRing),
				slog.Bool("only-gitlab-upgrade", patchMetadata.OnlyGitlabUpgrade),
				slog.Bool("rollback", patchMetadata.Rollback),
			)

			slog.LogAttrs(cmd.Context(),
				slog.LevelWarn,
				"a patch-id was provided, some input flags will be ignored",
			)
		}

		cells, err := storage.ListCells(cmd.Context(), ring)
		if err != nil {
			return fmt.Errorf("cannot list cells in ring %s: %w", ring, err)
		}

		pipeline := ci.Generate(ring, cells, onlyGitlabUpgrade, patchMetadata.Rollback)
		if err := pipeline.WriteTo(os.Stdout); err != nil {
			return fmt.Errorf("cannot write the pipeline to stdout: %w", err)
		}

		return nil
	},
}

func init() {
	ciCmd.AddCommand(ciGenerateCmd)
	ciGenerateCmd.Flags().Int("ring", 0, "the ring where the cell is located")
	ciGenerateCmd.Flags().Bool("only-gitlab-upgrade", true, "only generate the gitlab upgrade pipeline")
	ciGenerateCmd.Flags().String("patch-id", "", "specify the patch to read metadata")
}
