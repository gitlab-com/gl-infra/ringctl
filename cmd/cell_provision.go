/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"errors"
	"fmt"
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/template"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

var errProvision = errors.New("cannot provision a cell tenant")

var provisionCmd = &cobra.Command{
	Use:     "provision",
	Short:   "Provision a new cell",
	PreRunE: initClient,
	RunE: func(cmd *cobra.Command, args []string) error {
		ring, err := getRing(cmd)
		if err != nil {
			return err
		}

		c := &cell.Cell{}
		var tmpl string

		slog.LogAttrs(
			cmd.Context(),
			slog.LevelInfo,
			"adding new tenant",
			slog.Any("ring", ring),
			slog.String("amp", ring.AmpCluster()),
		)

		switch ring.AmpCluster() {
		case string(tissue.DevCluster):
			tmpl = template.CellsDev
		case string(tissue.ProdCluster):
			tmpl = template.CellsProd
		}

		if tmpl == "" {
			return fmt.Errorf("could not choose template from %q amp_environment: %w", ring.AmpCluster(), errProvision)
		}

		c, err = cell.Generate(tmpl)
		if err != nil {
			return fmt.Errorf("failed to generate cell tenant model: %w", err)
		}

		slog.LogAttrs(
			cmd.Context(),
			slog.LevelInfo,
			"new tenant generated",
			slog.String("tenant_id", c.TenantID()),
		)

		err = client.UploadCell(cmd.Context(), ring, c)
		if err != nil {
			return fmt.Errorf("failed to upload cell: %w", err)
		}

		return nil
	},
}

func init() {
	cellCmd.AddCommand(provisionCmd)
	requiresRingFlag(provisionCmd)
}
