package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/build"
)

var debug bool

func init() {
	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "verbose logging")
}

func initLogger(cmd *cobra.Command, args []string) {
	opts := slog.HandlerOptions{
		Level: slog.LevelInfo,
	}

	if debug {
		opts.Level = slog.LevelDebug
	}

	logger := slog.New(slog.NewTextHandler(cmd.ErrOrStderr(), &opts))
	slog.SetDefault(logger)

	slog.LogAttrs(cmd.Context(),
		slog.LevelDebug,
		"logger initialized",
		slog.String("version", build.Version().Short()),
	)
}
