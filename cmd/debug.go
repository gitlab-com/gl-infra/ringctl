package cmd

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
)

const waitForDebuggerFlag = "wait-for-debugger"

func init() {
	rootCmd.PersistentFlags().Bool(waitForDebuggerFlag, false, "wait for debugger to attach")

	if err := rootCmd.PersistentFlags().MarkHidden(waitForDebuggerFlag); err != nil {
		panic(err)
	}
}

func waitDebugger(cmd *cobra.Command, _ []string) {
	if cmd.Flags().Changed(waitForDebuggerFlag) && cmd.Flags().Lookup(waitForDebuggerFlag).Value.String() == "true" {
		sigs := make(chan os.Signal, 1)

		signal.Notify(sigs, syscall.SIGINT)

		pid := os.Getpid()

		fmt.Println("PID:", pid)
		fmt.Println("Waiting for debugger to attach... Press ctrl+c to continue.")

		<-sigs
	}
}
