/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/ulid"
)

var rollbackCmd = &cobra.Command{
	Use:     "rollback <version>",
	Short:   "Rollback a ring/cell to a desired version",
	Args:    cobra.ExactArgs(1), // Require exactly one argument for the version
	PreRunE: initClient,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()
		ring, err := getRing(cmd)
		if err != nil {
			failure(ctx, "cannot get ring", err)

			return
		}

		gitlabVersion, err := gitlab.NewVersion(args[0]) // Get version from the first argument, cmd.Args already verified it exists
		if err != nil {
			failure(ctx, "invalid gitlab version", err)

			return
		}

		pauseAfterRing, err := cmd.Flags().GetInt("pause-after-ring")
		if err != nil {
			failure(ctx, "failed to get pause-after-ring flag", err)

			return
		}

		deleteAfterRing, err := cmd.Flags().GetInt("delete-after-ring")
		if err != nil {
			failure(ctx, "failed to get delete-after-ring flag", err)

			return
		}

		relatedTo, err := cmd.Flags().GetStringSlice("related-to")
		if err != nil {
			failure(ctx, "failed to get related-to flag", err)

			return
		}

		rollbackArgs := []string{
			"replace",
			"/prerelease_version",
			gitlabVersion.String(),
		}

		patchID, err := ulid.Make()
		if err != nil {
			failure(ctx, "cannot generate a ULID", err)

			return
		}

		slog.LogAttrs(cmd.Context(),
			slog.LevelInfo,
			"Rollback to desired version",
			slog.Any("desired-version", gitlabVersion),
			slog.Any("ring", ring),
			slog.Any("patch-id", patchID),
		)

		// we need a custom branch name for this command
		client, err := getClientWithBranch(cmd.Context(), "rollback/"+patchID)
		if err != nil {
			failure(ctx, "failed to create tissue client", err)

			return
		}

		createErr := patch.Create(ctx, client, patchID, rollbackArgs, func(meta *patch.Metadata) error { //nolint:wrapcheck
			meta.Rollback = true
			meta.Priority = 0

			if pauseAfterRing >= 0 {
				meta.PauseAfterRing = &pauseAfterRing
			}

			if deleteAfterRing >= 0 {
				meta.DeleteAfterRing = &deleteAfterRing
			}

			meta.RelatedTo = append(meta.RelatedTo, relatedTo...)

			return nil
		})

		if createErr != nil {
			failure(ctx, "cannot create a rollback patch", createErr)

			return
		}
	},
}

func init() {
	rootCmd.AddCommand(rollbackCmd)
	requiresRingFlag(rollbackCmd)
	rollbackCmd.Flags().StringSlice("related-to", []string{}, "issues and merge-requests related to this patch")
	rollbackCmd.Flags().Int("pause-after-ring", -1, "pause the deployment after the specified ring number, -1 to disable pausing")
	rollbackCmd.Flags().Int("delete-after-ring", -1, "complete the deployment after the specified ring number, -1 to continue until the last ring")
}
