/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

// patchApplyCmd represents the patch apply command.
var patchApplyCmd = &cobra.Command{
	Use:     "apply <patch-id>",
	Short:   "Apply a patch to cells",
	Args:    cobra.ExactArgs(1),
	PreRunE: initClient,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()
		patchID := args[0]

		var meta *patch.Metadata
		slog.LogAttrs(ctx, slog.LevelInfo, "loading patch from patch-id", slog.String("patch_id", patchID))

		meta, err := client.GetPatch(ctx, patchID)
		if err != nil {
			failure(ctx, "cannot get the patch", err)

			return
		}

		slog.LogAttrs(
			ctx,
			slog.LevelInfo,
			"patching cells",
			slog.Int("ring", meta.Ring),
			slog.String("patch_id", meta.ID),
		)

		if err := client.Patch(ctx, meta); err != nil {
			failure(ctx, "cannot apply patch", err)
		}
	},
}

func init() {
	patchCmd.AddCommand(patchApplyCmd)
}
