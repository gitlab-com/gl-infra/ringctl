/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"github.com/spf13/cobra"
)

// patchCmd represents the patch command.
var patchCmd = &cobra.Command{
	Use:   "patch",
	Short: "A collection of commands to patch all the cells in a ring",
}

func init() {
	rootCmd.AddCommand(patchCmd)
}
