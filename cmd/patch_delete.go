/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var patchDeleteCmd = &cobra.Command{
	Use:   "delete <patch-id>",
	Short: "Delete a patch",
	Long: `Deletes a patch from the queue.

If no branch is specified a new branch named delete-<patch-id> is created.
A link to generate a merge request is printed on the console.

It is possible to force operation on the main branch by using the --branch=main flag,
but it requires a token that can commit directly to the main branch.`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		augmentContext(cmd)

		patchID := args[0]
		ctx := cmd.Context()

		branch := cmd.Flags().Lookup("branch").Value.String()
		if !cmd.Flags().Changed("branch") {
			branch = fmt.Sprintf("delete-%s", patchID)
		}

		client, err := getClientWithBranch(ctx, branch)
		if err != nil {
			failure(ctx, "cannot get the tissue client", err)

			return
		}

		if err := client.DeletePatch(ctx, patchID); err != nil {
			failure(ctx, "cannot delete the patch", err)

			return
		}
	},
}

func init() {
	patchCmd.AddCommand(patchDeleteCmd)
}
