/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"fmt"
	"log/slog"

	"github.com/spf13/cobra"
)

var lsCmd = &cobra.Command{
	Use:     "ls",
	Short:   "list cells in a given ring",
	PreRunE: initClient,
	RunE: func(cmd *cobra.Command, args []string) error {
		ring, err := getRing(cmd)
		if err != nil {
			return err
		}

		slog.LogAttrs(
			cmd.Context(),
			slog.LevelInfo,
			"listing cells",
			slog.Any("ring", ring),
			slog.String("amp", ring.AmpCluster()),
		)

		cells, err := client.ListCells(cmd.Context(), ring)
		if err != nil {
			return fmt.Errorf("failed to list cells: %w", err)
		}

		for _, cell := range cells {
			fmt.Printf("%s: Version: %s - Prerelease version: %s - URL: https://%s\n", cell.TenantID(), cell.Version(), cell.PrereleaseVersion(), cell.URL())
		}

		return nil
	},
}

func init() {
	cellCmd.AddCommand(lsCmd)
	requiresRingFlag(lsCmd)
}
