/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	patchIDFlag   = "patch-id"
	sourceURLFlag = "source-url"
)

var fireAlertCmd = &cobra.Command{
	Use:     "fire_alert",
	Short:   "Fire an alert to incident.io",
	PreRunE: initClient,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()

		patchID, err := cmd.Flags().GetString(patchIDFlag)
		if err != nil {
			failure(ctx, "patch-id parameter error", err)

			return
		}

		slog.LogAttrs(cmd.Context(),
			slog.LevelInfo,
			"patch-id provided, fetching metadata",
			slog.String("patch-id", patchID),
		)

		patchMetadata, err := client.GetPatch(ctx, patchID)
		if err != nil {
			failure(ctx, "patch retrieval failed", err, slog.String("patch_id", patchID))

			return
		}

		sourceURL, err := cmd.Flags().GetString(sourceURLFlag)
		if err != nil {
			failure(ctx, "source-url parameter error", err)

			return
		}

		if viper.GetBool(viperDryRun) {
			slog.LogAttrs(ctx, slog.LevelInfo, "dry-run: alert creation skipped",
				slog.String("patch-id", patchID),
				slog.String("source-url", sourceURL))

			return
		}

		if err := client.FireAlert(ctx, patchMetadata, sourceURL); err != nil {
			failure(ctx, "alert creation failed", err, slog.String("patch_id", patchID))

			return
		}
	},
}

func init() {
	ciCmd.AddCommand(fireAlertCmd)

	fireAlertCmd.Flags().String(patchIDFlag, "", "The patch id.")
	fireAlertCmd.Flags().String(sourceURLFlag, "", "The event source url.")

	if err := fireAlertCmd.MarkFlagRequired(patchIDFlag); err != nil {
		panic(err)
	}
}
