/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"fmt"
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
)

var rmCmd = &cobra.Command{
	Use:     "rm",
	Short:   "remove a cell",
	PreRunE: initClient,
	RunE: func(cmd *cobra.Command, args []string) error {
		ring, err := getRing(cmd)
		if err != nil {
			return err
		}

		cellID, err := cmd.Flags().GetString("cell")
		if err != nil {
			return fmt.Errorf("cell is required: %w", err)
		}

		slog.LogAttrs(
			cmd.Context(),
			slog.LevelInfo,
			"removing a cell",
			slog.String("cell", cellID),
			slog.Any("ring", ring),
			slog.String("amp", ring.AmpCluster()),
		)

		cellModel := cell.New(cellID)
		if err := client.RemoveCell(cmd.Context(), ring, cellModel); err != nil {
			return fmt.Errorf("cannot delete the cell tenant model: %w", err)
		}

		return nil
	},
}

func init() {
	cellCmd.AddCommand(rmCmd)
	requiresRingFlag(rmCmd)
	rmCmd.PersistentFlags().String("cell", "", "the cell to remove")

	if err := rmCmd.MarkPersistentFlagRequired("cell"); err != nil {
		panic(err)
	}
}
