/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"github.com/spf13/cobra"
)

var ciCmd = &cobra.Command{
	Use:    "ci",
	Short:  "tasks designed for running inside CI",
	Hidden: true,
}

func init() {
	rootCmd.AddCommand(ciCmd)
}
