/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"
	"slices"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

var cellDeployCmd = &cobra.Command{
	Use:     "deploy <cell-id>",
	Short:   "Trigger a cell deployment - only for quarantined cells",
	PreRunE: initClient,
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()
		cellID := args[0]

		onlyGitlabUpgrade, err := cmd.Flags().GetBool("only-gitlab-upgrade")
		if err != nil {
			failure(ctx, "only-gitlab-upgrade parameter error", err)

			return
		}

		isRollback, err := cmd.Flags().GetBool("rollback")
		if err != nil {
			failure(ctx, "rollback parameter error", err)

			return
		}

		amp, _ := getAmpCluster()
		ring := amp.Ring(-1)

		slog.LogAttrs(cmd.Context(),
			slog.LevelInfo,
			"Deploying cells",
			slog.Any("ring", ring),
			slog.String("amp", amp.Name()),
		)

		cells, err := client.ListCells(cmd.Context(), ring)
		if err != nil {
			failure(ctx, "failed to list cells", err)

			return
		}

		cellIdx := slices.IndexFunc(cells, func(c *cell.Cell) bool { return c != nil && c.TenantID() == cellID })
		if cellIdx == -1 {
			failure(ctx, "cell not found in quarantine ring", nil, slog.String("cell_id", cellID), slog.Int("quarantined_cells", len(cells)))

			return
		}

		pipeline, err := client.Deployment(cmd.Context(), ring,
			tissue.WithCellID(cellID),
			tissue.WithOnlyGitlabUpgrade(onlyGitlabUpgrade),
			tissue.IsRollback(isRollback),
		)
		if err != nil {
			failure(ctx, "failed to trigger ring deployment", err)

			return
		}

		if pipeline != nil {
			slog.LogAttrs(
				cmd.Context(),
				slog.LevelInfo,
				"🐿️ Cell deployment running ",
				slog.String("pipeline", pipeline.WebURL),
			)
		}
	},
}

func init() {
	cellCmd.AddCommand(cellDeployCmd)
	cellDeployCmd.Flags().Bool("only-gitlab-upgrade", true, "only generate the gitlab upgrade pipeline")
	cellDeployCmd.Flags().Bool("rollback", false, "generate a rollback pipeline")
}
