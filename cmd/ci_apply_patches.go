/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

var ciApplyPatchesCmd = &cobra.Command{
	Use:     "apply_patches",
	Short:   "Apply the first patch on each ring.",
	PreRunE: initClient,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()
		amp, _ := getAmpCluster()

		lastRing, err := client.LastRing(ctx)
		if err != nil {
			failure(ctx, "cannot count rings", err)

			return
		}

		currentEnv, hasCIEnv := os.LookupEnv(tissue.CIVariableAMPEnvironment)
		currentPatch, isPatchDeployment := os.LookupEnv(tissue.CIVariablePatchID)
		if isPatchDeployment && hasCIEnv && currentEnv == amp.Name() {
			slog.LogAttrs(ctx, slog.LevelDebug, "Patch deployment detected", slog.String("patch_id", currentPatch))

			if err := patch.ProgressCurrent(ctx, amp, client, currentPatch, lastRing); err != nil {
				failure(ctx, "cannot progress current patch", err, slog.String("patch_id", currentPatch))

				return
			}
		}

		patches, err := client.ListPatches(ctx)
		if err != nil {
			failure(ctx, "cannot get patches", err)

			return
		}

		patch.PrintCLITable(patches, iconsMap, patch.WithWriteTo(cmd.OutOrStdout()))

		if err := patch.ApplyNext(cmd.Context(), client, lastRing, patches, amp); err != nil {
			failure(ctx, "patch apply failure", err)
		}
	},
}

func init() {
	ciCmd.AddCommand(ciApplyPatchesCmd)
}
