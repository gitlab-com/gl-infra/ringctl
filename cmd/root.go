/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"context"
	"fmt"
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/build"
	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/incident"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

var rootCmd = &cobra.Command{
	Use:     "ringctl",
	Short:   "GitLab Cells Deployment Ring Control",
	Long:    `A tool to manage GitLab cells.`,
	Version: build.Version().String(),
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		initLogger(cmd, args)
		waitDebugger(cmd, args)
		changeCwd(cmd, args)
		setIconsMap(cmd, args)
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

var (
	cfgFile string
	client  *tissue.Client

	iconsMap patch.OutputOptionsFunc = patch.WithNerdFonts()
)

const (
	viperGitlabToken       = "gitlab_token"
	viperGitlabInstance    = "gitlab_instance"
	viperTissueProjectPath = "tissue_project_path"
	viperIncidentIOURL     = "incident_io_url"
	viperIncidentIOToken   = "incident_io_token"
	viperDryRun            = "dry-run"
	viperLocal             = "local"
)

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is ringctl.yaml)")

	rootCmd.PersistentFlags().StringP("branch", "b", "main", "the branch to use (default is main)")

	if e := viper.BindPFlag("branch", rootCmd.PersistentFlags().Lookup("branch")); e != nil {
		panic(e)
	}

	rootCmd.PersistentFlags().StringP("chdir", "C", "",
		"the path to the tissue repository. Only relevant for --local (default is empty, the current directory is used)")

	if e := viper.BindPFlag("chdir", rootCmd.PersistentFlags().Lookup("chdir")); e != nil {
		panic(e)
	}

	viper.SetDefault(viperGitlabInstance, "https://ops.gitlab.net")
	viper.SetDefault(viperTissueProjectPath, "gitlab-com/gl-infra/cells/tissue")

	for _, variableName := range []string{
		viperGitlabToken,
		viperGitlabInstance,
		viperTissueProjectPath,
		viperIncidentIOURL,
		viperIncidentIOToken,
	} {
		if e := viper.BindEnv(variableName); e != nil { // can only error if we send no parameters to viper.BindEnv
			panic(e)
		}
	}

	rootCmd.PersistentFlags().BoolP("dry-run", "n", false, "dry run mode (default is false)")

	if e := viper.BindPFlag("dry-run", rootCmd.PersistentFlags().Lookup("dry-run")); e != nil {
		panic(e)
	}

	rootCmd.PersistentFlags().BoolP("local", "l", false,
		"local mode, all operations are performed against a local copy of the cells/tissue repository (default is false)")

	if e := viper.BindPFlag("local", rootCmd.PersistentFlags().Lookup("local")); e != nil {
		panic(e)
	}

	// default to test for backward compatibility
	rootCmd.PersistentFlags().StringP("amp_environment", "e", "cellsdev", "the amp cluster to target (default is cellsdev)")

	if e := viper.BindPFlag("amp_environment", rootCmd.PersistentFlags().Lookup("amp_environment")); e != nil {
		panic(e)
	}

	// Icons to be displayed in the output can be selected at the root command level using a flag,
	// an environment variable, or the configuration file. Text will be used by default with a warning
	// to the user asking them to set their prefernce.
	rootCmd.PersistentFlags().StringP(patch.OptionIcons, "", "", "icon set to use in the output (nerd_fonts, emoji, or text)")

	if e := viper.BindPFlag(patch.OptionIcons, rootCmd.PersistentFlags().Lookup(patch.OptionIcons)); e != nil {
		panic(e)
	}

	if e := viper.BindEnv(patch.OptionIcons); e != nil {
		panic(e)
	}
}

// requiresRingFlag ensures that the ring flag is set for the command.
func requiresRingFlag(cmd *cobra.Command) {
	cmd.Flags().Int("ring", 0, "the ring where the cell is located")

	if err := cmd.MarkFlagRequired("ring"); err != nil {
		panic(err)
	}
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Search config in current directory with name "ringctl" (without extension).
		viper.AddConfigPath(".")
		viper.SetConfigName("ringctl")
	}

	if err := viper.ReadInConfig(); err != nil {
		// viper.ConfigFileNotFoundError do not implement Is or Unwrap
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok { //nolint: errorlint
			fmt.Println("Can't read config:", err)
			os.Exit(1)
		}
	}
}

func getClientWithBranch(ctx context.Context, branch string) (*tissue.Client, error) {
	amp, err := getAmpCluster()
	if err != nil {
		slog.LogAttrs(ctx, slog.LevelWarn, err.Error())
	}

	config := tissue.Config{
		Token:      viper.GetString(viperGitlabToken),
		Instance:   viper.GetString(viperGitlabInstance),
		Project:    viper.GetString(viperTissueProjectPath),
		Branch:     branch,
		AmpCluster: amp,
		DryRun:     viper.GetBool(viperDryRun),
		Local:      viper.GetBool(viperLocal),
		AlertManager: &incident.AlertManager{
			URL:   viper.GetString(viperIncidentIOURL),
			Token: viper.GetString(viperIncidentIOToken),
			Amp:   amp.Name(),
		},
	}

	return tissue.NewClient(ctx, &config) //nolint:wrapcheck
}

func getClient(ctx context.Context) (*tissue.Client, error) {
	return getClientWithBranch(ctx, viper.GetString("branch"))
}

func getAmpCluster() (tissue.AmpCluster, error) {
	amp := viper.GetString("amp_environment")

	cluster, err := tissue.WellKnownAmpCluster(amp)
	if err != nil {
		return cluster, fmt.Errorf("unknown amp_environment %s. %w", amp, err)
	}

	return cluster, nil
}

func getRing(cmd *cobra.Command) (tissue.Ring, error) {
	cluster, err := getAmpCluster()
	if err != nil {
		slog.LogAttrs(cmd.Context(), slog.LevelWarn, err.Error())
	}

	ring, err := cmd.Flags().GetInt("ring")
	if err != nil {
		return tissue.Ring{}, fmt.Errorf("ring is required: %w", err)
	}

	return cluster.Ring(ring), nil
}

func augmentContext(cmd *cobra.Command) {
	ctx := tissue.CtxWithRingctlVersion(cmd.Context(), build.Version().Short())
	if jobURL := os.Getenv("CI_JOB_URL"); jobURL != "" {
		ctx = tissue.CtxWithCIJobURL(ctx, jobURL)
	}

	cmd.SetContext(ctx)
}

func initClient(cmd *cobra.Command, args []string) error {
	slog.LogAttrs(cmd.Context(), slog.LevelDebug, "initializing client")
	augmentContext(cmd)

	_client, err := getClient(cmd.Context())
	if err != nil {
		return err
	}

	client = _client

	return nil
}

func failure(ctx context.Context, msg string, err error, fields ...slog.Attr) {
	if err != nil {
		fields = append(fields, slog.Any("error", err))
	}

	slog.LogAttrs(ctx,
		slog.LevelError,
		msg,
		fields...,
	)

	os.Exit(1)
}

func changeCwd(cmd *cobra.Command, _ []string) {
	cwd := viper.GetString("chdir")
	if cwd == "" {
		return
	}

	if err := os.Chdir(cwd); err != nil {
		failure(cmd.Context(), "failed to change working directory", err)

		return
	}
}

func setIconsMap(_ *cobra.Command, _ []string) {
	// Retain this flag for backward compatibility
	if forceEmoji := viper.GetBool("force_emoji_status"); forceEmoji {
		iconsMap = patch.WithEmoji()
		return
	}

	// Use emoji icon set in CI
	if os.Getenv("CI") != "" {
		iconsMap = patch.WithEmoji()
		return
	}

	switch viper.GetString(patch.OptionIcons) {
	case patch.OutputIconsNerdFonts:
		iconsMap = patch.WithNerdFonts()
	case patch.OutputIconsEmoji:
		iconsMap = patch.WithEmoji()
	case patch.OutputIconsTextStatuses:
		iconsMap = patch.WithTextStatuses()
	default:
		slog.Warn("using default text icons; please select your preferred set of icons and store the value in the ringctl.yml file")

		iconsMap = patch.WithTextStatuses()
	}
}
