/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/patch"
)

var patchGetCmd = &cobra.Command{
	Use:     "get patch-id",
	Short:   "get data about a JSON patch",
	PreRunE: initClient,
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		patchID := args[0]
		meta, err := client.GetPatch(cmd.Context(), patchID)
		if err != nil {
			failure(cmd.Context(), "patch retrieval failed", err, slog.String("patch_id", patchID))

			return
		}

		patch.PrintSinglePatch(meta, patch.WithWriteTo(cmd.OutOrStdout()))
	},
}

func init() {
	patchCmd.AddCommand(patchGetCmd)
}
