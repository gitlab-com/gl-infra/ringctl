/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/upgrade"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
)

var upgradeCmd = &cobra.Command{
	Use:     "upgrade <version>",
	Short:   "Upgrade a ring to the given version",
	Args:    cobra.ExactArgs(1),
	PreRunE: initClient,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()

		newVersion, err := gitlab.NewVersion(args[0])
		if err != nil {
			failure(ctx, "invalid gitlab version", err)

			return
		}

		//prevent unintentional rollbacks
		amp, _ := getAmpCluster()
		r0Cells, err := client.ListCells(ctx, amp.Ring(0))
		if err != nil {
			failure(ctx, "cannot fetch existing cells", err)

			return
		}

		for _, cell := range r0Cells {
			currentVersion := cell.PrereleaseVersion()
			if newVersion.LessThan(currentVersion) {
				failure(ctx, "rollback detected. Abort", nil,
					slog.Any("new_version", newVersion),
					slog.String("current_version", currentVersion),
					slog.String("tenant_id", cell.TenantID()),
				)

				return
			}
		}

		relatedTo, err := cmd.Flags().GetStringSlice("related-to")
		if err != nil {
			failure(ctx, "failed to get related-to flag", err)

			return
		}

		if err := upgrade.WithAPatch(ctx, newVersion, relatedTo, client); err != nil {
			failure(ctx, "failed to generate an upgrade patch", err)

			return
		}
	},
}

func init() {
	rootCmd.AddCommand(upgradeCmd)

	upgradeCmd.Flags().Int("ring", 0, "the ring where the cell is located - legacy parameter, only for cellsprod")
	upgradeCmd.Flags().StringSlice("related-to", []string{}, "issues and merge-requests related to this patch")
}
