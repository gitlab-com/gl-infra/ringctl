/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/patch"
)

var patchLsCmd = &cobra.Command{
	Use:     "ls",
	Short:   "List the patches in the queue",
	PreRunE: initClient,
	Long: `
In most of the cases you should have OPS Gitlab token exported. Make sure you have token stored in 1password and run the following:
# eval $(pmv env Cells/Tissue/OPS)

Examples:
1, List all patches on CellsDev cluster
# ringctl patch ls

2, List all patches on CellsProd cluster
# ringctl patch ls -e cellsprod `,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()

		patches, err := client.ListPatches(cmd.Context())
		if err != nil {
			failure(ctx, "cannot get patches", err)

			return
		}

		lastRing, err := client.LastRing(ctx)
		if err != nil {
			failure(ctx, "cannot count rings", err)

			return
		}

		patch.PrintCLITable(patches, iconsMap, patch.WithWriteTo(cmd.OutOrStdout()))

		amp, _ := getAmpCluster()
		slog.LogAttrs(ctx,
			slog.LevelDebug,
			"Total ring count",
			slog.Int("last_ring", lastRing),
			slog.Any("amp", amp),
		)

	},
}

func init() {
	patchCmd.AddCommand(patchLsCmd)
}
