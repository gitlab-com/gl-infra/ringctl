/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/ulid"
)

var patchCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new patch",
	Long: `Generate a new JSON Patch to be applied to the rings of the AMP environment.
It is possible to define all the patch operations on the CLI, for example:
ringctl patch create replace /instrumentor_version v16.416.2 replace /gitlab_version 17.4.1

Not all the operations take the same number of parameters, here is a list of all the operations and the parameters they take:
"add", "replace", and "test": two parameters, the path and the value to set.
"remove": one parameter, the path to remove.
"move" and "copy": two parameters, the source path and the destination path.

For more details please refer to https://jsonpatch.com/

Examples:
1. Create a patch to bump an instrumentor version
# ringctl patch create replace /instrumentor_version v16.416.2

2. Create a patch to add new field to tenant model
# ringctl patch create add /enable_fips "true"

3. Create a patch to add new field with nested struct to tenant model
# ringctl patch create add "/byod" '{"instance": "gitlab.com"}'
`,
	Args: JSONPatchSetArgs(),
	Run: func(cmd *cobra.Command, args []string) {
		ctx := cmd.Context()

		id, err := ulid.Make()
		if err != nil {
			failure(ctx, "cannot create an ULID", err)

			return
		}

		augmentContext(cmd)
		// we need a custom branch name for this command
		client, err := getClientWithBranch(cmd.Context(), "patch/"+id) //nolint:govet
		if err != nil {
			failure(ctx, "failed to create tissue client", err)

			return
		}

		priority, err := cmd.Flags().GetInt("priority")
		if err != nil {
			failure(ctx, "failed to get priority flag", err)

			return
		}

		pauseAfterRing, err := cmd.Flags().GetInt("pause-after-ring")
		if err != nil {
			failure(ctx, "failed to get pause-after-ring flag", err)

			return
		}

		deleteAfterRing, err := cmd.Flags().GetInt("delete-after-ring")
		if err != nil {
			failure(ctx, "failed to get delete-after-ring flag", err)

			return
		}

		relatedTo, err := cmd.Flags().GetStringSlice("related-to")
		if err != nil {
			failure(ctx, "failed to get related-to flag", err)

			return
		}

		origin, err := cmd.Flags().GetString("origin")
		if err != nil {
			failure(ctx, "failed to get origin flag", err)

			return
		}

		createErr := patch.Create(cmd.Context(), client, id, args, func(meta *patch.Metadata) error {
			meta.Priority = priority
			meta.RelatedTo = append(meta.RelatedTo, relatedTo...)
			meta.Origin = origin

			if pauseAfterRing >= 0 {
				meta.PauseAfterRing = &pauseAfterRing
			}

			if deleteAfterRing >= 0 {
				meta.DeleteAfterRing = &deleteAfterRing
			}

			return nil
		})

		if createErr != nil {
			failure(ctx, "cannot create a patch", err)

			return
		}
	},
}

func init() {
	patchCmd.AddCommand(patchCreateCmd)
	patchCreateCmd.Flags().Int("priority", patch.DefaultPriority,
		fmt.Sprintf("patch priority, lower value means higher priority. Default is %d", patch.DefaultPriority))
	patchCreateCmd.Flags().Int("pause-after-ring", -1, "pause the deployment after the specified ring number, -1 to disable pausing")
	patchCreateCmd.Flags().Int("delete-after-ring", -1, "complete the deployment after the specified ring number, -1 to continue until the last ring")
	patchCreateCmd.Flags().StringSlice("related-to", []string{}, "issues and merge-requests related to this patch")
	patchCreateCmd.Flags().String("origin", "", "the origin of the patch, automations can use this field to track their own patches")
}
