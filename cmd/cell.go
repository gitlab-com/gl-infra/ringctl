/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"github.com/spf13/cobra"
)

var cellCmd = &cobra.Command{
	Use:   "cell",
	Short: "A collection of commands to manage individual cells",
}

func init() {
	rootCmd.AddCommand(cellCmd)
}
