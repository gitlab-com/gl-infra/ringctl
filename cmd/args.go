package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

func JSONPatchSetArgs() cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		return patch.ProcessArgs(args, nil) //nolint:wrapcheck
	}
}
