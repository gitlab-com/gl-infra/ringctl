/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/internal/patch"
)

var patchEditCmd = &cobra.Command{
	Use:     "edit <patch-id>",
	Short:   "edit a JSON patch",
	Hidden:  true,
	PreRunE: initClient,
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		patchID := args[0]
		meta, err := client.GetPatch(cmd.Context(), patchID)
		if err != nil {
			failure(cmd.Context(), "patch retrieval failed", err, slog.String("patch_id", patchID))

			return
		}

		if cmd.Flags().Changed("error") {
			errorMsg, err := cmd.Flags().GetString("error")
			if err != nil {
				failure(cmd.Context(), "could not get error message", err)

				return
			}

			meta.Error = errorMsg
		} else {
			failure(cmd.Context(), "at least one option must be specified", nil)

			return
		}

		meta.Updated() // mark the timestamp

		if updateErr := client.UpdatePatchStatus(cmd.Context(), meta); updateErr != nil {
			failure(cmd.Context(), "error updating patch status", updateErr,
				slog.String("patch_id", meta.ID),
			)

			return
		}

		patch.PrintSinglePatch(meta, patch.WithWriteTo(cmd.OutOrStdout()))
	},
}

func init() {
	patchCmd.AddCommand(patchEditCmd)

	patchEditCmd.Flags().String("error", "", "set the patch error message")
}
