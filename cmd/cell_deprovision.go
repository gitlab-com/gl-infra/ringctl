/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package cmd

import (
	"fmt"
	"log/slog"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
)

var deprovisionCmd = &cobra.Command{
	Use:     "deprovision",
	Short:   "deprovision a cell",
	PreRunE: initClient,
	RunE: func(cmd *cobra.Command, args []string) error {
		ring, err := getRing(cmd)
		if err != nil {
			return err
		}

		cellID, err := cmd.Flags().GetString("cell")
		if err != nil {
			return fmt.Errorf("cell is required: %w", err)
		}

		slog.LogAttrs(
			cmd.Context(),
			slog.LevelInfo,
			"💥 deprovisioning cell",
			slog.String("cell", cellID),
			slog.Any("ring", ring),
			slog.String("amp", ring.AmpCluster()),
		)

		cellModel := cell.New(cellID)
		pipeline, err := client.TearDownCell(cmd.Context(), ring, cellModel)
		if err != nil {
			return fmt.Errorf("cannot trigger a tear down pipeline: %w", err)
		}

		if pipeline != nil {
			slog.LogAttrs(
				cmd.Context(),
				slog.LevelInfo,
				"🐿️ tear down pipeline",
				slog.String("url", pipeline.WebURL),
			)
		}

		return nil
	},
}

func init() {
	cellCmd.AddCommand(deprovisionCmd)
	requiresRingFlag(deprovisionCmd)
	deprovisionCmd.PersistentFlags().String("cell", "", "the cell to deprovision")

	if err := deprovisionCmd.MarkPersistentFlagRequired("cell"); err != nil {
		panic(err)
	}
}
