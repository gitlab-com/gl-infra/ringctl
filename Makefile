LIBS=$(wildcard lib/**/*)
CMD=$(wildcard cmd/**/* internal/**/* go.*)

all: lint test build

.PHONY: clean dist

build: ringctl

clean:
	rm -rf dist ringctl */*/mock_*.go

lint: app-lint lib-lint

app-lint:
	golangci-lint run

lib-lint: $(LIBS)
	cd lib && golangci-lint run

test: mocks app-test lib-test acceptance

app-test: $(CMD) main.go $(LIBS)
	go test ./...

lib-test: $(LIBS)
	cd lib && go test ./...

ringctl: $(CMD) main.go $(LIBS)
	go build

mocks:
	mockery

acceptance:
	./scripts/acceptance_tests.sh

dist:
	goreleaser release --snapshot --clean
