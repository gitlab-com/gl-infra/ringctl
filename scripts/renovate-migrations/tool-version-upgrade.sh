#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

case $RENOVATE_POSTUPG_DEP_NAME in
"vektra/mockery")
  mise install mockery --yes
  mise x mockery -- mockery
  ;;
*)
  echo "./scripts/renovate-migrations/tool-version-upgrade.sh do not handle this package"
  echo "Unmanaged dependency: $RENOVATE_POSTUPG_DEP_NAME"

  export | grep RENOVATE_POSTUPG_
  ;;
esac
