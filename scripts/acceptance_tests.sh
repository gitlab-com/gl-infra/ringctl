#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

run() {
  ./ringctl --config ringctl.ci.yaml "$@"
}

go build

run cell ls --ring 0

run patch ls

for p in lib/test/patches/fixture1/*.json; do
  patch_id=$(basename -s .json "$p")

  run patch get "$patch_id"
done
