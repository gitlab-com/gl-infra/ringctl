package cell

import (
	"strings"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/ulid"
)

const (
	idLen int = 17
)

// See https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/cell_arch_tooling/#naming-conventions-for-cells
// for documentation on the naming convention.
func makeID() (string, error) {
	fullUlid, err := ulid.Make()
	if err != nil {
		return "", err //nolint:wrapcheck
	}

	return "c" + strings.ToLower(fullUlid[0:idLen]), nil
}
