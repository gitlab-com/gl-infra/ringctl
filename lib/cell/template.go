package cell

import (
	"bytes"
	"encoding/json"
	"fmt"
	"text/template"
)

const (
	cellPrefix string = "cell-"
)

type cellTemplateData struct {
	ID         string
	PrefixedID string
}

func Generate(templateBody string) (*Cell, error) {
	id, err := makeID()
	if err != nil {
		return nil, fmt.Errorf("cannot generate ulid: %w", err)
	}

	data := &cellTemplateData{
		ID:         id,
		PrefixedID: cellPrefix + id,
	}

	var b bytes.Buffer

	cell := &Cell{tenantID: data.ID}

	templ, err := template.New("tenant").Parse(templateBody)
	if err != nil {
		return nil, fmt.Errorf("cannot parse template: %w", err)
	}

	err = templ.Execute(&b, data)
	if err != nil {
		return nil, fmt.Errorf("cannot execute template: %w", err)
	}

	err = json.Unmarshal(b.Bytes(), &cell.Model)
	if err != nil {
		return nil, fmt.Errorf("cannot unmarshal cell model: %w", err)
	}

	return cell, nil
}
