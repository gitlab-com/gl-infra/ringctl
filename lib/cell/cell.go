package cell

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"

	jsonpatch "github.com/evanphx/json-patch/v5"
)

var ErrTenantModel = errors.New("invalid tenant model")

type Cell struct {
	tenantID          string
	version           string
	prereleaseVersion string
	uRL               string
	Model             map[string]interface{}
}

func Read(r io.Reader) (*Cell, error) {
	cell := &Cell{}
	dec := json.NewDecoder(r)

	if err := dec.Decode(&cell.Model); err != nil {
		return nil, fmt.Errorf("cannot decode cell: %w", err)
	}

	return cell, cell.parseTenantModel()
}

func New(id string) *Cell {
	return &Cell{
		tenantID: id,
	}
}

func (c *Cell) TenantModelFileName() string {
	return c.tenantID + ".json"
}

func (c *Cell) TenantID() string {
	return c.tenantID
}

func (c *Cell) Version() string {
	return c.version
}

func (c *Cell) PrereleaseVersion() string {
	return c.prereleaseVersion
}

func (c *Cell) URL() string {
	return c.uRL
}

func (c *Cell) Upgrade(version string) {
	c.prereleaseVersion = version

	c.Model["prerelease_version"] = c.prereleaseVersion
}

func (c *Cell) ToString() (string, error) {
	buff := new(bytes.Buffer)
	enc := json.NewEncoder(buff)
	enc.SetIndent("", "  ")

	if err := enc.Encode(c.Model); err != nil {
		return "", fmt.Errorf("cannot encode model: %w", err)
	}

	return buff.String(), nil
}

func (c *Cell) parseTenantModel() error {
	ID, ok := c.Model["tenant_id"].(string)
	if !ok {
		return fmt.Errorf("tenant_id is not a string: %w", ErrTenantModel)
	}

	c.tenantID = ID

	gitlabVersion, ok := c.Model["gitlab_version"].(string)
	if !ok {
		return fmt.Errorf("gitlab_version is not a string: %w", ErrTenantModel)
	}

	c.version = gitlabVersion

	prerelease, present := c.Model["prerelease_version"]
	if !present {
		c.prereleaseVersion = ""
	} else {
		prereleaseVersion, ok := prerelease.(string) //nolint:govet
		if !ok {
			return fmt.Errorf("prerelease_version is not a string: %w", ErrTenantModel)
		}

		c.prereleaseVersion = prereleaseVersion
	}

	u, ok := c.Model["managed_domain"].(string)
	if !ok {
		return fmt.Errorf("managed_domain is not a string: %w", ErrTenantModel)
	}

	c.uRL = u

	return nil
}

// Patch applies a JSON patch to the model.
//
// The function returns a new cell with the patched model and an error if the
// patch could not be applied.
// The current model is not modified.
func (c *Cell) Patch(patch jsonpatch.Patch) (*Cell, error) {
	model, err := c.ToString()
	if err != nil {
		return nil, fmt.Errorf("cannot render model as string: %w", err)
	}

	newModel, err := patch.Apply(([]byte)(model))
	if err != nil {
		return nil, fmt.Errorf("cannot apply patch: %w", err)
	}

	return Read(bytes.NewReader(newModel))
}
