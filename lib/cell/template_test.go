package cell

import (
	"path"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/template"
)

func TestGenerateTestEnv(t *testing.T) {
	cell, err := Generate(template.CellsDev)
	require.NoError(t, err)

	require.NotEmpty(t, cell.Model["$schema"])
}

//nolint:dupl
func TestGenerate(t *testing.T) {
	templates := []struct {
		name   string
		tmpl   string
		assert func(t *testing.T, c *Cell)
	}{
		{
			name: "CellsDev",
			tmpl: template.CellsDev,
			assert: func(t *testing.T, c *Cell) {
				t.Helper()

				require.Equal(t, "amp-b6f1", c.Model["amp_gcp_project_id"])
				require.False(t, c.Model["audit_logging"].(bool)) //nolint:forcetypeassert
				require.Equal(t, "211125640907", c.Model["dns_aws_account_id"])
				require.Equal(t, "211125640907", c.Model["aws_account_id"])
				require.Equal(t,
					"//iam.googleapis.com/projects/1002415312824/locations/global/workloadIdentityPools/gitlab-pool-oidc-amp-1290/providers/gitlab-jwt-amp-1290", //nolint:lll
					c.Model["gcp_oidc_audience"],
				)
				require.Regexpf(t,
					`^cell-c[a-z0-9]{17}.gitlab-cells.dev$`,
					c.Model["managed_domain"],
					"expected cell URL %s to match pattern",
					c.Model["managed_domain"],
				)
				require.Equal(t, "mg.staging.gitlab.com", c.Model["external_smtp_parameters"].(map[string]interface{})["domain"])              //nolint:forcetypeassert,lll
				require.Equal(t, "gitlab@mg.gitlab.com", c.Model["external_smtp_parameters"].(map[string]interface{})["from"])                 //nolint:forcetypeassert,lll
				require.Equal(t, "postmaster@mg.staging.gitlab.com", c.Model["external_smtp_parameters"].(map[string]interface{})["username"]) //nolint:forcetypeassert,lll
			},
		},
		{
			name: "CellsProd",
			tmpl: template.CellsProd,
			assert: func(t *testing.T, c *Cell) {
				t.Helper()

				require.Equal(t, "amp-3c0d", c.Model["amp_gcp_project_id"])
				require.True(t, c.Model["audit_logging"].(bool)) //nolint:forcetypeassert
				require.Equal(t, "058264099888", c.Model["aws_account_id"])
				require.Equal(t, "058264099888", c.Model["dns_aws_account_id"])
				require.Equal(
					t,
					"//iam.googleapis.com/projects/136182366286/locations/global/workloadIdentityPools/gitlab-pool-oidc-amp-2368/providers/gitlab-jwt-amp-2368",
					c.Model["gcp_oidc_audience"],
				)
				require.Regexpf(t,
					`^cell-c[a-z0-9]{17}.cells.gitlab.com$`,
					c.Model["managed_domain"],
					"expected cell URL %s to match pattern",
					c.Model["managed_domain"],
				)
				require.Equal(t, "mg.gitlab.com", c.Model["external_smtp_parameters"].(map[string]interface{})["domain"])                  //nolint:forcetypeassert,lll
				require.Equal(t, "gitlab@mg.gitlab.com", c.Model["external_smtp_parameters"].(map[string]interface{})["from"])             //nolint:forcetypeassert,lll
				require.Equal(t, "dotcom-rotated@mg.gitlab.com", c.Model["external_smtp_parameters"].(map[string]interface{})["username"]) //nolint:forcetypeassert,lll
			},
		},
	}

	for _, tt := range templates {
		t.Run(tt.name, func(t *testing.T) {
			c, err := Generate(tt.tmpl)
			require.NoError(t, err)
			tt.assert(t, c)

			require.Equal(t, "us-central1", c.Model["backup_region"])
			require.Equal(t, "gcp", c.Model["cloud_provider"])
			require.Equal(t, "us-east1", c.Model["gcp_onboarding_state_region"])
			require.Equal(t, "v16.377.1", c.Model["instrumentor_version"])
			require.Equal(t, "17.2.202407052000-e57b36937d5.6d3194f378c", c.Model["prerelease_version"])
			require.Equal(t, "https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/tenant-model-schema/v1.36.0/tenant-model.json", c.Model["$schema"])
			require.Regexpf(t, `^c[a-z0-9]{17}$`, c.TenantID(), "expected tenant ID %s to match pattern", c.TenantID())
		})
	}
}

func TestGenerateErrors(t *testing.T) {
	templates := []string{
		"testdata/invalid_json.json.tmpl",
		"testdata/missing_template.json.tmpl",
		"testdata/invalid_template.json.tmpl",
	}
	for _, template := range templates {
		t.Run(path.Base(template), func(t *testing.T) {
			cell, err := Generate(template)
			require.Error(t, err)

			require.Nil(t, cell)
		})
	}
}
