package cell

import (
	"bytes"
	"fmt"
	"os"
	"testing"

	jsonpatch "github.com/evanphx/json-patch/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestReadTenantModelFile(t *testing.T) {
	modelFile, err := os.Open("testdata/cell1b22.json")
	require.NoError(t, err, "cannot open tenant model file")

	cell, err := Read(modelFile)
	require.NoError(t, err, "cannot read tenant model")

	require.NotNil(t, cell, "cell is nil")

	require.Equal(t, "cell1b22", cell.TenantID())
	require.Equal(t, "cell1b22.cells.example.com", cell.URL())
	require.Equal(t, "16.9.0", cell.Version())
	require.Equal(t, "16.10.202403071400-a5c67111ad3.727b99aba03", cell.PrereleaseVersion())
}

func TestUpgrade(t *testing.T) {
	modelFile, err := os.Open("testdata/cell1b22.json")
	require.NoError(t, err, "cannot open tenant model file")

	cell, err := Read(modelFile)
	require.NoError(t, err, "cannot read tenant model")

	require.NotNil(t, cell, "cell is nil")

	require.Equal(t, "16.10.202403071400-a5c67111ad3.727b99aba03", cell.PrereleaseVersion())

	newVersion := "42.42.0-rc42"
	cell.Upgrade(newVersion)

	json, err := cell.ToString()
	require.NoError(t, err, "cannot convert cell to string")
	require.Contains(t, json, fmt.Sprintf("\"prerelease_version\": \"%s\"", newVersion))
}

func TestPatch(t *testing.T) {
	modelFile, err := os.Open("testdata/cell1b22.json")
	require.NoError(t, err, "cannot open tenant model file")

	cell, err := Read(modelFile)
	require.NoError(t, err, "cannot read tenant model")
	require.NotNil(t, cell, "cell is nil")

	require.Equal(t, "16.10.202403071400-a5c67111ad3.727b99aba03", cell.PrereleaseVersion())
	require.Equal(t, "cell1b22.cells.example.com", cell.URL())

	patch, err := os.Open("testdata/patch_upgrade.json")
	require.NoError(t, err, "cannot open patch file")

	buff := new(bytes.Buffer)
	_, err = buff.ReadFrom(patch)
	require.NoError(t, err, "cannot read patch file")

	patchDecoded, err := jsonpatch.DecodePatch(buff.Bytes())
	require.NoError(t, err, "cannot decode patch")

	patchedCell, err := cell.Patch(patchDecoded)
	require.NoError(t, err, "cannot patch cell")
	assert.NotNil(t, patchedCell, "new cell is nil")

	require.Equal(t, "42.42.2", patchedCell.PrereleaseVersion())
	require.Equal(t, "replaced.example.com", patchedCell.URL())

	// Test jsonpatch.Patch can be applied multiple times
	patchedCell2, err := cell.Patch(patchDecoded)
	require.NoError(t, err, "cannot patch cell")
	assert.NotNil(t, patchedCell2, "new cell is nil")

	require.Equal(t, "42.42.2", patchedCell2.PrereleaseVersion())
	require.Equal(t, "replaced.example.com", patchedCell2.URL())
}
