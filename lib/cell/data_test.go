package cell

import (
	"testing"

	"github.com/stretchr/testify/require"
)

// Test_makeIDFormat tests that the generated ID conforms to the naming convention:
// https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/cells/infrastructure/cell_arch_tooling/#naming-conventions-for-cells
func Test_makeIDFormat(t *testing.T) {
	id, err := makeID()
	require.NoError(t, err)
	require.Regexpf(t, `^c[a-z0-9]{17}$`, id, "generated ID %s does not conform to the requirements", id)
}
