package gitlab

import (
	"context"
	"errors"
	"net/http"
	"net/url"
	"testing"
)

var errTestSentinel = errors.New("test error")

func TestRetryConcurrentPushes(t *testing.T) {
	tests := []struct {
		name      string
		ctx       context.Context //nolint:containedctx
		resp      *http.Response
		err       error
		wantRetry bool
		wantErr   error
	}{
		{
			name: "context cancelled",
			ctx: func() context.Context {
				ctx, cancel := context.WithCancel(context.Background())
				cancel()

				return ctx
			}(),
			resp:      &http.Response{},
			err:       nil,
			wantRetry: false,
			wantErr:   context.Canceled,
		},
		{
			name:      "error present",
			ctx:       context.Background(),
			resp:      &http.Response{},
			err:       errTestSentinel,
			wantRetry: false,
			wantErr:   errTestSentinel,
		},
		{
			name:      "too many requests",
			ctx:       context.Background(),
			resp:      &http.Response{StatusCode: http.StatusTooManyRequests},
			err:       nil,
			wantRetry: true,
			wantErr:   nil,
		},
		{
			name:      "server error",
			ctx:       context.Background(),
			resp:      &http.Response{StatusCode: http.StatusInternalServerError},
			err:       nil,
			wantRetry: true,
			wantErr:   nil,
		},
		{
			name: "concurrent push error",
			ctx:  context.Background(),
			resp: &http.Response{
				StatusCode: http.StatusBadRequest,
				Request: &http.Request{
					URL: &url.URL{
						Path: "/api/v4/projects/123/repository/commits",
					},
				},
			},
			err:       nil,
			wantRetry: true,
			wantErr:   nil,
		},
		{
			name:      "success status",
			ctx:       context.Background(),
			resp:      &http.Response{StatusCode: http.StatusOK},
			err:       nil,
			wantRetry: false,
			wantErr:   nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotRetry, gotErr := retryConcurrentPushes(tt.ctx, tt.resp, tt.err)

			if tt.wantErr != nil && gotErr == nil {
				t.Errorf("retryConcurrentPushes() error = nil, wantErr %v", tt.wantErr)
				return
			}

			if tt.wantErr == nil && gotErr != nil {
				t.Errorf("retryConcurrentPushes() unexpected error = %v", gotErr)
				return
			}

			if tt.wantErr != nil && gotErr != nil && tt.wantErr.Error() != gotErr.Error() {
				t.Errorf("retryConcurrentPushes() error = %v, wantErr %v", gotErr, tt.wantErr)
				return
			}

			if gotRetry != tt.wantRetry {
				t.Errorf("retryConcurrentPushes() retry = %v, want %v", gotRetry, tt.wantRetry)
			}
		})
	}
}
