package gitlab

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestValidVersions(t *testing.T) {
	versions := []string{
		"16.5.0",
		"16.5.0-rc1",
		"17.5.202409242201-d263ec23e08.a15b2f7c376",
	}

	for _, version := range versions {
		t.Run(version, func(t *testing.T) {
			v, err := NewVersion(version)
			require.NoError(t, err)
			assert.True(t, v.IsValid())
			assert.Equal(t, version, v.String())
		})
	}
}

func TestInvalidVersions(t *testing.T) {
	versions := []string{
		"foo",
		"16.5.0-ee",
		"16.5.0-ee",
		"16.5.0-rc1.ee.0",
		"16.5.0-rc1.ee",
		"v16.5.0",
		"v17.5.202409242201-d263ec23e08.a15b2f7c376",
		"16.5.0-ee.0",
		"16.5.0-rc1.ee.0-g62f1e5300d-master",
		"16.5.0-rc1.ee.0-g62f1e5300d",
	}

	for _, version := range versions {
		t.Run(version, func(t *testing.T) {
			v, err := NewVersion(version)
			require.Error(t, err)
			assert.False(t, v.IsValid())
			assert.Equal(t, version, v.String())
		})
	}
}

func TestVersion_String(t *testing.T) {
	originalVersion := "16.5.0-rc1"
	v, err := NewVersion(originalVersion)
	require.NoError(t, err)
	assert.Equal(t, originalVersion, v.String())
}

func TestLessThan(t *testing.T) {
	v, err := NewVersion("17.10.202502210906-19c11d91297.32e455681a0")
	require.NoError(t, err)

	assert.False(t, v.LessThan("16.9.202502202306-392fa64b92e.f93f35a6636"), "previous major")
	assert.False(t, v.LessThan("17.9.202502202306-392fa64b92e.f93f35a6636"), "previous minor")
	assert.False(t, v.LessThan("17.10.202502210905-19c11d91297.32e455681a0"), "previous patch")
	assert.True(t, v.LessThan("17.10.202502210907-19c11d91297.32e455681a0"), "next patch")
	assert.True(t, v.LessThan("18.9.202502202306-392fa64b92e.f93f35a6636"), "next major")
	assert.True(t, v.LessThan("17.11.202502202306-392fa64b92e.f93f35a6636"), "next minor")
	assert.False(t, v.LessThan("invalid version"), "an invalid version")
}
