package gitlab

import gitlab "gitlab.com/gitlab-org/api/client-go"

type Client interface {
	CreatePipeline(pid interface{}, opt *gitlab.CreatePipelineOptions,
		options ...gitlab.RequestOptionFunc) (*gitlab.Pipeline, *gitlab.Response, error)
	GetPipeline(pid interface{}, pipelineID int,
		options ...gitlab.RequestOptionFunc) (*gitlab.Pipeline, *gitlab.Response, error)
	CreateMergeRequest(pid interface{}, opt *gitlab.CreateMergeRequestOptions,
		options ...gitlab.RequestOptionFunc) (*gitlab.MergeRequest, *gitlab.Response, error)
	CreateCommit(pid interface{}, opt *gitlab.CreateCommitOptions,
		options ...gitlab.RequestOptionFunc) (*gitlab.Commit, *gitlab.Response, error)
	ListTree(pid interface{}, opt *gitlab.ListTreeOptions,
		options ...gitlab.RequestOptionFunc) ([]*gitlab.TreeNode, *gitlab.Response, error)
	GetBranch(pid interface{}, branch string,
		options ...gitlab.RequestOptionFunc) (*gitlab.Branch, *gitlab.Response, error)
	CreateBranch(pid interface{}, opt *gitlab.CreateBranchOptions,
		options ...gitlab.RequestOptionFunc) (*gitlab.Branch, *gitlab.Response, error)
	GetRawFile(pid interface{}, filepath string, opt *gitlab.GetRawFileOptions,
		options ...gitlab.RequestOptionFunc) ([]byte, *gitlab.Response, error)
}
