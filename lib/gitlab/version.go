package gitlab

import (
	"errors"
	"fmt"
	"regexp"

	"github.com/Masterminds/semver/v3"
)

const rawVersionRegexp = `\A(?<major>\d+)\.(?<minor>\d+)(\.(?<patch>\d+))?(-(?<rc>rc(?<rc_number>\d*)))?(-[0-9a-fA-F]{11}\.[0-9a-fA-F]{11})?\z`

var (
	ErrInvalidVersion = errors.New("invalid GitLab version")

	versionRegexp = regexp.MustCompile(rawVersionRegexp)
)

// A Version represents a GitLab product version (not a package version or tag).
//
// It could be a released version like 16.5.0, a release candidate version like 16.5.0-rc1,
// or an auto-deploy version like 17.5.202409242201-d263ec23e08.a15b2f7c376.
//
// A version is created with the NewVersion function and can be checked for validity with the IsValid method.
type Version struct {
	string
	semver *semver.Version
}

func NewVersion(v string) (*Version, error) {
	version := &Version{string: v}
	sv, err := semver.NewVersion(v)

	if sv != nil {
		version.semver = sv
	}

	if err != nil {
		return version, fmt.Errorf("%w: %w", ErrInvalidVersion, err)
	}

	if !version.IsValid() {
		return version, ErrInvalidVersion
	}

	return version, nil
}

func (v *Version) IsValid() bool {
	return versionRegexp.MatchString(v.string)
}

func (v *Version) String() string {
	return v.string
}

func (v *Version) LessThan(other string) bool {
	o, err := NewVersion(other)
	if err != nil {
		return false
	}

	return v.semver.LessThan(o.semver)
}
