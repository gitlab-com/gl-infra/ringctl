//nolint:wrapcheck
package gitlab

import (
	"context"
	"log/slog"
	"net/http"
	"strings"

	"github.com/hashicorp/go-retryablehttp"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

// clientWrapper is a wrapper struct for the gitlab.Client struct.
// It implements the Client interface.
//
// It is used to mock the gitlab.Client struct in tests.
//
// To add a new method to the Client interface, add a new method to this struct and to the Client interface.
// If possible keep the same signature of the original method.
type clientWrapper struct {
	*gitlab.Client
}

var retryConcurrentPushes retryablehttp.CheckRetry = func(ctx context.Context, resp *http.Response, err error) (bool, error) {
	var statusCode slog.Attr
	if resp != nil {
		statusCode = slog.Int("status_code", resp.StatusCode)
	} else {
		statusCode = slog.Int("status_code", 0)
	}

	slog.LogAttrs(ctx, slog.LevelDebug, "gitlab API retry check", slog.Any("error", err), statusCode)

	// default code ported from https://gitlab.com/gitlab-org/api/client-go/-/blob/v0.116.0/gitlab.go?ref_type=tags#L483
	if ctx.Err() != nil {
		return false, ctx.Err()
	}
	if err != nil {
		return false, err
	}
	if resp == nil {
		return true, nil
	}
	if resp.StatusCode == http.StatusTooManyRequests || resp.StatusCode >= 500 {
		return true, nil
	}

	// custom handling for tissue
	// see https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20647
	// see https://gitlab.com/gitlab-org/gitlab/-/issues/431398#note_1647177057
	if resp.StatusCode == http.StatusBadRequest &&
		resp.Request != nil && resp.Request.URL != nil &&
		strings.HasSuffix(resp.Request.URL.String(), "/repository/commits") {
		return true, nil
	}

	return false, nil
}

func NewClient(token string, options ...gitlab.ClientOptionFunc) (*clientWrapper, error) {
	options = append(options, gitlab.WithCustomRetry(retryConcurrentPushes))

	client, err := gitlab.NewClient(token, options...)
	if err != nil {
		return nil, err
	}

	return &clientWrapper{Client: client}, nil
}

// CreatePipeline is a wrapper for the CreatePipeline method in the gitlab.Client struct.
func (c *clientWrapper) CreatePipeline(pid interface{}, opt *gitlab.CreatePipelineOptions,
	options ...gitlab.RequestOptionFunc) (*gitlab.Pipeline, *gitlab.Response, error) {
	return c.Client.Pipelines.CreatePipeline(pid, opt, options...)
}

// GetPipeline is a wrapper for the GetPipeline method in the gitlab.Client struct.
func (c *clientWrapper) GetPipeline(pid interface{}, pipelineID int,
	options ...gitlab.RequestOptionFunc) (*gitlab.Pipeline, *gitlab.Response, error) {
	return c.Client.Pipelines.GetPipeline(pid, pipelineID, options...)
}

// CreateMergeRequest is a wrapper for the CreateMergeRequest method in the gitlab.Client struct.
func (c *clientWrapper) CreateMergeRequest(pid interface{}, opt *gitlab.CreateMergeRequestOptions,
	options ...gitlab.RequestOptionFunc) (*gitlab.MergeRequest, *gitlab.Response, error) {
	return c.Client.MergeRequests.CreateMergeRequest(pid, opt, options...)
}

// CreateCommit is a wrapper for the CreateCommit method in the gitlab.Client struct.
func (c *clientWrapper) CreateCommit(pid interface{}, opt *gitlab.CreateCommitOptions,
	options ...gitlab.RequestOptionFunc) (*gitlab.Commit, *gitlab.Response, error) {
	return c.Client.Commits.CreateCommit(pid, opt, options...)
}

// ListTree is a wrapper for the ListTree method in the gitlab.Client struct.
func (c *clientWrapper) ListTree(pid interface{}, opt *gitlab.ListTreeOptions,
	options ...gitlab.RequestOptionFunc) ([]*gitlab.TreeNode, *gitlab.Response, error) {
	return c.Client.Repositories.ListTree(pid, opt, options...)
}

// GetRawFile is a wrapper for the GetRawFile method in the gitlab.Client struct.
func (c *clientWrapper) GetRawFile(pid interface{}, filepath string, opt *gitlab.GetRawFileOptions,
	options ...gitlab.RequestOptionFunc) ([]byte, *gitlab.Response, error) {
	return c.Client.RepositoryFiles.GetRawFile(pid, filepath, opt, options...)
}

// GetBranch is a wrapper for the GetBranch method in the gitlab.Client struct.
func (c *clientWrapper) GetBranch(pid interface{}, branch string,
	options ...gitlab.RequestOptionFunc) (*gitlab.Branch, *gitlab.Response, error) {
	return c.Client.Branches.GetBranch(pid, branch, options...)
}

// CreateBranch is a wrapper for the CreateBranch method in the gitlab.Client struct.
func (c *clientWrapper) CreateBranch(pid interface{}, opt *gitlab.CreateBranchOptions,
	options ...gitlab.RequestOptionFunc) (*gitlab.Branch, *gitlab.Response, error) {
	return c.Client.Branches.CreateBranch(pid, opt, options...)
}
