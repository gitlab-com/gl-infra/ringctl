package ci

import (
	"bytes"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/test"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

func TestGenerate(t *testing.T) {
	fixtures := test.GetFixturesCluster()
	ring := fixtures.Ring(0)

	modelFile, err := os.Open(ring.CellPath("cell1b22.json"))
	require.NoError(t, err, "cannot open tenant model file")

	c, err := cell.Read(modelFile)
	require.NoError(t, err, "cannot read tenant model")
	require.NotNil(t, c, "cell is nil")

	for _, configureOnly := range []bool{true, false} {
		rollbackOnly := false

		t.Run(fmt.Sprint("configure_only_", configureOnly), func(t *testing.T) {
			p := Generate(ring, []*cell.Cell{c}, configureOnly, rollbackOnly)
			require.NotEmpty(t, p)

			buff := bytes.Buffer{}
			err = p.WriteTo(&buff)
			require.NoError(t, err)

			expectedYml, err := os.ReadFile(fmt.Sprintf("testdata/pipeline_configure_only_%v.yml", configureOnly))
			require.NoError(t, err)

			require.YAMLEq(
				t,
				string(expectedYml),
				buff.String(),
			)
		})
	}

	for _, rollback := range []bool{true, false} {
		configureOnly := true

		t.Run(fmt.Sprint("rollback_", rollback), func(t *testing.T) {
			p := Generate(ring, []*cell.Cell{c}, configureOnly, rollback)
			require.NotEmpty(t, p)

			buff := bytes.Buffer{}
			err = p.WriteTo(&buff)
			require.NoError(t, err)

			expectedYml, err := os.ReadFile(fmt.Sprintf("testdata/rollback_%v.yml", rollback))
			require.NoError(t, err)

			require.YAMLEq(
				t,
				string(expectedYml),
				buff.String(),
			)
		})
	}
}

func TestDetermineQALevel(t *testing.T) {
	fixtures := test.GetFixturesCluster()

	testCases := []struct {
		name              string
		ring              tissue.Ring
		onlyGitlabUpgrade bool
		rollback          bool
		expected          string
	}{
		{
			name:              "rollback operation returns none",
			ring:              fixtures.Ring(0),
			onlyGitlabUpgrade: true,
			rollback:          true,
			expected:          "none",
		},
		{
			name:              "auto-deployment return all, when not a rollback",
			ring:              fixtures.Ring(0),
			onlyGitlabUpgrade: true,
			rollback:          false,
			expected:          "all",
		},
		{
			name:              "rollback operation returns none even for infrastructure deployment",
			ring:              fixtures.Ring(0),
			onlyGitlabUpgrade: false,
			rollback:          true,
			expected:          "none",
		},
		{
			name:              "infrastructure deployment returns all for ring 0",
			ring:              fixtures.Ring(0),
			onlyGitlabUpgrade: false,
			rollback:          false,
			expected:          "all",
		},
		{
			name:              "auto-deployment returns all for ring -1",
			ring:              fixtures.Ring(-1),
			onlyGitlabUpgrade: true,
			rollback:          false,
			expected:          "all",
		},
		{
			name:              "auto-deployment returns all for ring 0",
			ring:              fixtures.Ring(0),
			onlyGitlabUpgrade: true,
			rollback:          false,
			expected:          "all",
		},
		{
			name:              "auto-deployment returns smoke for ring 1",
			ring:              fixtures.Ring(1),
			onlyGitlabUpgrade: true,
			rollback:          false,
			expected:          "smoke",
		},
		{
			name:              "auto-deployment returns none for ring 2",
			ring:              fixtures.Ring(2),
			onlyGitlabUpgrade: true,
			rollback:          false,
			expected:          "none",
		},
		{
			name:              "auto-deployment returns none for all rings above 1",
			ring:              fixtures.Ring(42),
			onlyGitlabUpgrade: true,
			rollback:          false,
			expected:          "none",
		},
	}

	t.Parallel()

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual := determineQALevel(tc.ring, tc.onlyGitlabUpgrade, tc.rollback)
			require.Equal(t, tc.expected, actual)
		})
	}
}

func TestTemplateJob(t *testing.T) {
	testCases := []struct {
		name              string
		onlyGitlabUpgrade bool
		expected          string
	}{
		{
			name:              "full-deploy",
			onlyGitlabUpgrade: false,
			expected:          "templates/single-cell-infra-deployment.gitlab-ci.yml",
		},
		{
			name:              "auto-deploy",
			onlyGitlabUpgrade: true,
			expected:          "templates/single-cell-auto-deploy.gitlab-ci.yml",
		},
	}

	t.Parallel()

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual := templateJob(tc.onlyGitlabUpgrade)
			if actual != tc.expected {
				t.Errorf("TenantModel(%v) = %q, want %q", tc.onlyGitlabUpgrade, actual, tc.expected)
			}
		})
	}
}
