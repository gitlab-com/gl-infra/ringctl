package ci

import (
	"fmt"
	"io"
	"strconv"

	"gopkg.in/yaml.v3"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

const (
	// CIVariableQALevel defines the CI/CD variable name for controlling QA test levels.
	CIVariableQALevel = "QA_LEVEL"
)

type pipeline map[string]*job

type trigger struct {
	Strategy string
	Include  []string
}

type job struct {
	Needs     []string
	Stage     string
	Variables map[string]string
	Trigger   trigger
}

func Generate(ring tissue.Ring, cells []*cell.Cell, onlyGitlabUpgrade bool, rollback bool) pipeline {
	p := pipeline{}

	template := templateJob(onlyGitlabUpgrade)

	for _, c := range cells {
		j := job{
			Stage: "deploy",
			Variables: map[string]string{
				tissue.CIVariableAMPEnvironment:    ring.AmpCluster(),
				tissue.CIVariableCellID:            c.TenantID(),
				tissue.CIVariableOnlyGitlabUpgrade: strconv.FormatBool(onlyGitlabUpgrade),
				tissue.CIVariableRing:              ring.String(),
				tissue.CIVariableRollback:          strconv.FormatBool(rollback),
				CIVariableQALevel:                  determineQALevel(ring, onlyGitlabUpgrade, rollback),
			},
			Trigger: trigger{
				Strategy: "depend",
				Include:  []string{template},
			},
		}

		p[fmt.Sprintf("%s - ring %d", c.TenantID(), ring.Number())] = &j
	}

	return p
}

const yamlIndent = 2

func (p pipeline) WriteTo(w io.Writer) error {
	enc := yaml.NewEncoder(w)
	enc.SetIndent(yamlIndent)

	err := enc.Encode(p)
	if err != nil {
		return fmt.Errorf("pipeline encoding failed: %w", err)
	}

	return nil
}

// determineQALevel determines what level of QA testing should be performed based on
// the ring number, whether it's an auto-deployment or infrastructure deployment,
// and whether it's a rollback operation.
// Returns:
// - "none" for all rollback operations
// - "all" for rings -1/0 auto-deployments and all infrastructure deployments
// - "smoke" for ring 1 auto-deployments
// - "none" for ring 2+ auto-deployments.
func determineQALevel(ring tissue.Ring, onlyGitlabUpgrade bool, rollback bool) string {
	// If this is a rollback operation, skip all QA
	if rollback {
		return "none"
	}

	// If it's not an auto-deployment (infrastructure deployment), run all QA
	if !onlyGitlabUpgrade {
		return "all"
	}

	// For auto-deployments, determine based on ring number
	ringNum := ring.Number()

	switch {
	case ringNum <= 0: // Ring -1 and 0
		return "all"
	case ringNum == 1: // Ring 1
		return "smoke"
	default: // Ring 2 and beyond
		return "none"
	}
}

// templateJob: if we are only upgrading the GitLab application, there's no
// need to run a full blown deployment, so we have a template dedicated to this
// Otherwise, return our template that would be geared towards running all
// stages required for a Cell upgrade.
func templateJob(onlyGitlabUpgrade bool) string {
	if onlyGitlabUpgrade {
		return "templates/single-cell-auto-deploy.gitlab-ci.yml"
	} else {
		return "templates/single-cell-infra-deployment.gitlab-ci.yml"
	}
}
