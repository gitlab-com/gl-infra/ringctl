package incident

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

// AlertManager is a client for interacting with an incident manager API.
// It is used to fire alerts to incident.io.
type AlertManager struct {
	URL   string
	Token string
	Amp   string
}

var ErrAlertEventNotAccepted = fmt.Errorf("alert event not accepted")
var ErrMissingConfiguration = fmt.Errorf("missing incident.io API URL or token")

func (am *AlertManager) FireAlert(ctx context.Context, patch *patch.Metadata, sourceURL string) error {
	_, err := am.sendAlertEvent(ctx, statusFiring, patch, sourceURL)

	return err
}

func (am *AlertManager) ResolveAlert(ctx context.Context, patch *patch.Metadata, sourceURL string) error {
	_, err := am.sendAlertEvent(ctx, statusResolved, patch, sourceURL)

	return err
}

func (am *AlertManager) sendAlertEvent(ctx context.Context, status status, patch *patch.Metadata, sourceURL string) (*alertEventResponse, error) {
	if am.URL == "" || am.Token == "" {
		slog.LogAttrs(ctx, slog.LevelError, "missing incident.io API URL or token",
			slog.Bool("missing_token", am.Token == ""), slog.Bool("missing_url", am.URL == ""))

		return nil, ErrMissingConfiguration
	}

	deduplicationKey := fmt.Sprintf("%s-%d-%s", am.Amp, patch.Ring, patch.ID)

	event := alertEvent{
		DeduplicationKey: deduplicationKey,
		SourceURL:        sourceURL,
		Status:           status,
		Title:            fmt.Sprintf("[%s] deployment of %s failed", am.Amp, patch.ID),
		Metadata: &alertMetadata{
			Service: "tissue",
			Amp:     am.Amp,
			Ring:    patch.Ring,
			Patch:   patch.ID,
		},
	}

	// Convert body to JSON
	jsonBody, err := json.Marshal(event)
	if err != nil {
		return nil, fmt.Errorf("error marshaling JSON: %w", err)
	}

	// Create request
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, am.URL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, fmt.Errorf("error creating request: %w", err)
	}

	// Set headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+am.Token)

	// Make the request
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error making request: %w", err)
	}

	// Read the response body
	alertEventResp := alertEventResponse{}

	defer resp.Body.Close()
	dec := json.NewDecoder(resp.Body)

	if err := dec.Decode(&alertEventResp); err != nil {
		return nil, fmt.Errorf("error reading response body: %w", err)
	}

	level := slog.LevelInfo
	if alertEventResp.Status != "accepted" {
		level = slog.LevelError
		err = ErrAlertEventNotAccepted
	}

	slog.LogAttrs(ctx, level, "incident.io alert event",
		slog.String("message", alertEventResp.Message),
		slog.String("status", alertEventResp.Status),
		slog.String("deduplication_key", alertEventResp.DeduplicationKey),
	)

	return &alertEventResp, err
}
