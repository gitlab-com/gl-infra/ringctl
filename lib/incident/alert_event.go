package incident

type status string

const (
	statusFiring   status = "firing"
	statusResolved status = "resolved"
)

// alertEvent represents the event data that should be provided to incident.io API
// to fire or resolve an alert.
// The event may be enriched with various metadata and descriptive data.
// More details about available parameters and their requirements can be found here:
//
//	https://api-docs.incident.io/tag/Alert-Events-V2#operation/Alert%20Events%20V2_CreateHTTP
type alertEvent struct {
	DeduplicationKey string         `json:"deduplication_key,omitempty"`
	Description      string         `json:"description,omitempty"`
	Metadata         *alertMetadata `json:"metadata,omitempty"`
	SourceURL        string         `json:"source_url,omitempty"`
	Status           status         `json:"status"`
	Title            string         `json:"title"`
}

type alertMetadata struct {
	Service string `json:"service"`
	Amp     string `json:"amp"`
	Ring    int    `json:"ring"`
	Patch   string `json:"patch"`
}

type alertEventResponse struct {
	DeduplicationKey string `json:"deduplication_key"`
	Message          string `json:"message"`
	Status           string `json:"status"`
}
