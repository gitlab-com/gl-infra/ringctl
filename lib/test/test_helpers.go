package test

import (
	"errors"
	"path"
	"runtime"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"

	_ "embed"
)

const PatchIDFixture1 = "01JAQKKQQ5PHP7EF7T24BF0JPA"

//go:embed patches/fixture1/01JAQKKQQ5PHP7EF7T24BF0JPA.json
var Patch01JAQKKQQ5PHP7EF7T24BF0JPAFixture1 string

var ErrPathDiscovery = errors.New("unable to discover fixture path")
var testFixturePath string

func init() {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		panic(ErrPathDiscovery)
	}

	testFixturePath = path.Dir(filename)

	if Patch01JAQKKQQ5PHP7EF7T24BF0JPAFixture1 == "" {
		panic("patch embedding failed")
	}
}

type testCluster struct {
	Fixture string
}

var _ tissue.Cluster = (*testCluster)(nil)

func (t *testCluster) RingsPath() string {
	return path.Join(testFixturePath, "rings", t.Fixture)
}

func (t *testCluster) PatchesPath() string {
	return path.Join(testFixturePath, "patches", t.Fixture)
}

func (t *testCluster) Ring(nr int) tissue.Ring {
	return tissue.NewRing(t.Fixture, t.RingsPath(), nr)
}

func (t *testCluster) Name() string {
	return t.Fixture
}

func GetFixturesCluster() *testCluster {
	return &testCluster{Fixture: "fixture1"}
}
