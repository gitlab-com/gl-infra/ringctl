package test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/incident"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

// MockTissueClient creates a tissue client with mocked storage and gitlab client
//
// conf allows customizing the client parameters.
func MockTissueClient(t *testing.T, conf func(*tissue.Config)) (*tissue.Client, *tissue.MockCellsStorage, *gitlab.MockClient) {
	t.Helper()

	gitlabClient := gitlab.NewMockClient(t)
	storage := tissue.NewMockCellsStorage(t)
	cfg := &tissue.Config{
		AmpCluster: GetFixturesCluster(),
		Gitlab:     gitlabClient,
		Storage:    storage,
		Branch:     "main",
		AlertManager: &incident.AlertManager{
			URL:   "https://www.example.com",
			Token: "not-a-real-token",
			Amp:   GetFixturesCluster().Name(),
		},
	}

	if conf != nil {
		conf(cfg)
	}

	client, err := tissue.NewClient(context.Background(), cfg)

	require.NoError(t, err, "client init failure")
	require.NotNil(t, client)

	return client, storage, gitlabClient
}
