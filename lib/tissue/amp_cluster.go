package tissue

import (
	"errors"
	"path"
)

type AmpCluster string

type Cluster interface {
	Name() string
	RingsPath() string
	PatchesPath() string
	Ring(nr int) Ring
}

const (
	DevCluster  AmpCluster = AmpCluster("cellsdev")
	ProdCluster AmpCluster = AmpCluster("cellsprod")
)

var ErrUnknownAmp = errors.New("unknown APM cluster")

func WellKnownAmpClusters() []AmpCluster {
	return []AmpCluster{DevCluster, ProdCluster}
}

func WellKnownAmpCluster(name string) (AmpCluster, error) {
	cluster := AmpCluster(name)

	for _, ampEnv := range WellKnownAmpClusters() {
		if cluster == ampEnv {
			return ampEnv, nil
		}
	}

	return cluster, ErrUnknownAmp
}

func (a AmpCluster) RingsPath() string {
	return path.Join("rings", string(a))
}

func (a AmpCluster) Ring(nr int) Ring {
	return NewRing(string(a), a.RingsPath(), nr)
}

func (a AmpCluster) PatchesPath() string {
	return path.Join("patches", (string)(a))
}

func (a AmpCluster) Name() string {
	return string(a)
}
