package tissue

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCommitMessage(t *testing.T) {
	amp := AmpCluster("cluster")
	c := NewCommitMessage(amp, "Title")
	assert.Equal(t, "[cluster] Title\n\n", c.String())
}

func TestAppendLine(t *testing.T) {
	c := CommitMessage{}

	c.AppendLine("Line 1")
	c.AppendLine("Line 2")

	assert.Equal(t, "Line 1\nLine 2\n", c.String())
}

func TestTrailers(t *testing.T) {
	expectedCommitMsg := `[cluster] Title

Description

Co-Authored-by: Foo Bar <foo@example.com>
`

	c := NewCommitMessage(AmpCluster("cluster"), "Title")
	c.AppendLine("Description")
	c.AddTrailer("Co-Authored-by", "Foo Bar <foo@example.com>")

	assert.Equal(t, expectedCommitMsg, c.String())
}

func TestSkipCI(t *testing.T) {
	expectedCommitMsg := `[cluster] Title

[SKIP-CI]
`

	c := NewCommitMessage(AmpCluster("cluster"), "Title")
	c.SkipCI()

	assert.Equal(t, expectedCommitMsg, c.String())
}
