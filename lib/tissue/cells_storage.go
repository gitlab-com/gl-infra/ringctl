package tissue

import (
	"context"
	"errors"

	gitlab "gitlab.com/gitlab-org/api/client-go"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

type CellsAction gitlab.FileActionValue

const (
	CreateCells CellsAction = CellsAction(gitlab.FileCreate)
	EditCells   CellsAction = CellsAction(gitlab.FileUpdate)
	DeleteCell  CellsAction = CellsAction(gitlab.FileDelete)
)

var ErrPatchNotFound = errors.New("patch not found")

type CellsStorage interface {
	AddPatch(ctx context.Context, cluster Cluster, meta *patch.Metadata) error
	GetPatch(ctx context.Context, cluster Cluster, patchID string) (*patch.Metadata, error)
	DeletePatch(ctx context.Context, cluster Cluster, patchID string) error
	UpdatePatchStatus(ctx context.Context, cluster Cluster, meta *patch.Metadata) error
	Update(ctx context.Context, ring Ring, cells []*cell.Cell, action CellsAction, commitMsg string) error
	UpdateWithPatch(ctx context.Context, cluster Cluster, patch *patch.Metadata, cells []*cell.Cell, commitMsg string) error
	ListCells(ctx context.Context, ring Ring) ([]*cell.Cell, error)
	ListPatches(ctx context.Context, cluster Cluster) ([]*patch.Metadata, error)
	LastRing(ctx context.Context, cluster Cluster) (int, error)
}
