package tissue

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/url"
	"os"
	"path"
	"strconv"
	"strings"
	"text/template"

	gitlab "gitlab.com/gitlab-org/api/client-go"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	gl "gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"

	_ "embed"
)

//go:embed templates/patch_mr.md
var patchMrTemplate string

type gitlabCellsStorage struct {
	git        gl.Client
	dryRun     bool
	project    string
	branch     string
	projectURL url.URL
}

type patchMrTemplateData struct {
	Cluster  Cluster
	Metadata *patch.Metadata
}

func createPatchMergeRequestDescription(data *patchMrTemplateData) (string, error) {
	mrTemplate, err := template.New("mr").Parse(patchMrTemplate)
	if err != nil {
		return "", fmt.Errorf("cannot parse MR template: %w", err)
	}

	mrDescription := bytes.Buffer{}

	if err := mrTemplate.Execute(&mrDescription, data); err != nil {
		return "", fmt.Errorf("cannot execute MR template: %w", err)
	}

	return mrDescription.String(), nil
}

func (g *gitlabCellsStorage) createPatchMergeRequest(ctx context.Context, amp Cluster, meta *patch.Metadata) (string, error) {
	templateData := &patchMrTemplateData{
		Cluster:  amp,
		Metadata: meta,
	}

	description, err := createPatchMergeRequestDescription(templateData)
	if err != nil {
		return "", fmt.Errorf("cannot generate MR description: %w", err)
	}

	if g.dryRun {
		fmt.Println("[DRY-RUN] generating merge request")
		fmt.Println(description)

		return "https://dry-run.example.com/tissue/merge-requests/1", nil
	}

	if err := g.ensureBranchExists(ctx, io.Discard); err != nil {
		return "", fmt.Errorf("cannot create branch %q: %w", g.branch, err)
	}

	mr, _, errMR := g.git.CreateMergeRequest(g.project, &gitlab.CreateMergeRequestOptions{
		Title:              gitlab.Ptr(fmt.Sprintf("[%s] New patch %s", amp.Name(), meta.ID)),
		Description:        gitlab.Ptr(description),
		RemoveSourceBranch: gitlab.Ptr(true),
		SourceBranch:       gitlab.Ptr(g.branch),
		TargetBranch:       gitlab.Ptr(defaultBranch),
	}, gitlab.WithContext(ctx))
	if errMR != nil {
		return "", fmt.Errorf("failed to create merge request: %w", errMR)
	}

	return mr.WebURL, nil
}

func (g *gitlabCellsStorage) AddPatch(ctx context.Context, amp Cluster, meta *patch.Metadata) error {
	if len(meta.Patch) == 0 {
		return fmt.Errorf("patch not loaded in memory: %w", ErrNoPatchSpecified)
	}

	patchesRoot := amp.PatchesPath()

	if g.branch != defaultBranch {
		mergeRequestURL, err := g.createPatchMergeRequest(ctx, amp, meta)
		if err != nil {
			return fmt.Errorf("cannot generate a merge request: %w", err)
		}

		meta.RelatedTo = append([]string{mergeRequestURL}, meta.RelatedTo...)

		slog.LogAttrs(
			ctx,
			slog.LevelInfo,
			"merge request created",
			slog.String("url", mergeRequestURL),
		)
	}

	msg := NewCommitMessage(amp, fmt.Sprintf("New patch %s", meta.ID))
	for _, url := range meta.RelatedTo {
		msg.AddTrailer("Related-to", url)
	}

	AppendMetadataTrailers(ctx, msg)

	metaAction, err := patchMetaToCommitActionOptions(meta, patchesRoot)
	if err != nil {
		return fmt.Errorf("cannot convert patch meta into gitlab commit action: %w", err)
	}

	metaAction.Action = gitlab.Ptr(gitlab.FileCreate)

	commit, err := g.createCommit(ctx, msg.String(), []*gitlab.CommitActionOptions{metaAction})
	if err != nil {
		return err //nolint:wrapcheck
	}

	slog.LogAttrs(
		ctx,
		slog.LevelInfo,
		"patch created",
		slog.String("url", commit.WebURL),
	)

	return nil
}

func (g *gitlabCellsStorage) GetPatch(ctx context.Context, amp Cluster, patchID string) (*patch.Metadata, error) {
	patchPath := path.Join(amp.PatchesPath(), patchID+".json")

	return g.getPatch(ctx, patchPath) //nolint:wrapcheck
}

func (g *gitlabCellsStorage) DeletePatch(ctx context.Context, amp Cluster, patchID string) error {
	if err := g.ensureBranchExists(ctx, os.Stdout); err != nil {
		return fmt.Errorf("cannot ensure branch %q exists: %w", g.branch, err)
	}

	meta, err := g.GetPatch(ctx, amp, patchID)
	if err != nil {
		return fmt.Errorf("cannot find the patch: %w", err)
	}

	patchPath := path.Join(amp.PatchesPath(), patchID+".json")

	msg := NewCommitMessage(amp, fmt.Sprintf("Delete %s", meta.ID))
	msg.SkipCI()

	for _, url := range meta.RelatedTo {
		msg.AddTrailer("Related-to", url)
	}

	AppendMetadataTrailers(ctx, msg)

	c, err := g.createCommit(ctx, msg.String(), []*gitlab.CommitActionOptions{{
		Action:   gitlab.Ptr(gitlab.FileDelete),
		FilePath: gitlab.Ptr(patchPath),
	}})

	if err != nil {
		return fmt.Errorf("cannot delete the patch: %w", err)
	}

	slog.LogAttrs(
		ctx,
		slog.LevelInfo,
		"patch deleted",
		slog.String("patch_id", patchID),
		slog.String("url", c.WebURL),
	)

	return nil
}

func (g *gitlabCellsStorage) UpdatePatchStatus(ctx context.Context, amp Cluster, meta *patch.Metadata) error {
	msg := NewCommitMessage(amp, fmt.Sprintf("%s is now %s at ring %d", meta.ID, meta.Status(), meta.Ring))
	msg.SkipCI()

	for _, url := range meta.RelatedTo {
		msg.AddTrailer("Related-to", url)
	}

	AppendMetadataTrailers(ctx, msg)

	metaAction, err := patchMetaToCommitActionOptions(meta, amp.PatchesPath())
	if err != nil {
		return fmt.Errorf("cannot convert patch meta into gitlab commit action: %w", err)
	}

	if _, err := g.createCommit(ctx, msg.String(), []*gitlab.CommitActionOptions{metaAction}); err != nil {
		return err //nolint:wrapcheck
	}

	slog.LogAttrs(
		ctx,
		slog.LevelInfo,
		"patch updated",
	)

	return nil
}
func (g *gitlabCellsStorage) UpdateWithPatch(ctx context.Context, amp Cluster, patch *patch.Metadata, cells []*cell.Cell, commitMsg string) error {
	metaUpdate, err := patchMetaToCommitActionOptions(patch, amp.PatchesPath())
	if err != nil {
		return fmt.Errorf("cannot convert patch meta into gitlab commit action: %w", err)
	}

	return g.updateWithExtraActions(ctx, amp.Ring(patch.Ring), cells, EditCells, commitMsg, metaUpdate)
}

func (g *gitlabCellsStorage) Update(ctx context.Context, ring Ring, cells []*cell.Cell, action CellsAction, commitMsg string) error {
	return g.updateWithExtraActions(ctx, ring, cells, action, commitMsg)
}

func (g *gitlabCellsStorage) updateWithExtraActions(
	ctx context.Context,
	ring Ring,
	cells []*cell.Cell,
	action CellsAction,
	commitMsg string,
	extraActions ...*gitlab.CommitActionOptions) error {
	actions := make([]*gitlab.CommitActionOptions, 0, len(cells)+len(extraActions))

	for _, cell := range cells {
		commitAction := &gitlab.CommitActionOptions{
			Action:   gitlab.Ptr(gitlab.FileActionValue(action)),
			FilePath: gitlab.Ptr(ring.CellPath(cell.TenantModelFileName())),
		}

		if action == DeleteCell {
			continue
		}

		content, err := cell.ToString()
		if err != nil {
			return fmt.Errorf("cannot convert cell to JSON: %w", err)
		}

		commitAction.Content = gitlab.Ptr(content)
		actions = append(actions, commitAction)
	}

	actions = append(actions, extraActions...)

	if err := g.ensureBranchExists(ctx, os.Stdout); err != nil {
		return err
	}

	createCommitOpts := &gitlab.CreateCommitOptions{
		Branch:        gitlab.Ptr(g.branch),
		CommitMessage: gitlab.Ptr(commitMsg),
		Actions:       actions,
	}
	if g.dryRun {
		dryRunCreateCommit(createCommitOpts)

		return nil
	}

	commit, _, err := g.git.CreateCommit(g.project, createCommitOpts, gitlab.WithContext(ctx))
	if err != nil {
		return err //nolint:wrapcheck
	}

	slog.LogAttrs(
		ctx,
		slog.LevelInfo,
		"cell operation",
		slog.Any("ring", ring),
		slog.String("amp", ring.AmpCluster()),
		slog.String("action", string(action)),
		slog.String("url", commit.WebURL),
	)

	return nil
}

func (g *gitlabCellsStorage) listTree(ctx context.Context, path string, processor func(*gitlab.TreeNode) error) ([]*gitlab.TreeNode, error) {
	params := &gitlab.ListTreeOptions{
		Path: gitlab.Ptr(path),
		Ref:  gitlab.Ptr(g.branch),
		ListOptions: gitlab.ListOptions{
			Pagination: "keyset",
		},
	}

	allNodes := []*gitlab.TreeNode{}
	nodes, resp, err := g.git.ListTree(g.project, params, gitlab.WithContext(ctx))

	for {
		if err != nil {
			return nil, fmt.Errorf("failed to list tree for %v (branch %q): %w", path, g.branch, err)
		}

		for _, node := range nodes {
			allNodes = append(allNodes, node)

			if processor != nil {
				if err := processor(node); err != nil {
					return nil, err
				}
			}
		}

		if resp.NextLink == "" {
			break
		}

		nodes, resp, err = g.git.ListTree(g.project, params, gitlab.WithContext(ctx), gitlab.WithKeysetPaginationParameters(resp.NextLink))
	}

	return allNodes, nil
}

func (g *gitlabCellsStorage) ListCells(ctx context.Context, ring Ring) ([]*cell.Cell, error) {
	var cells []*cell.Cell
	ringPath := ring.Path() //nolint:wsl

	_, err := g.listTree(ctx, ringPath, func(node *gitlab.TreeNode) error {
		if node.Type != "blob" {
			return nil
		}

		if !strings.HasSuffix(node.Path, ".json") {
			return nil
		}

		cell, err := g.getCell(ctx, path.Base(node.Path), ring)
		if err != nil {
			return fmt.Errorf("failed to get cell %v: %w", node.Path, err)
		}

		cells = append(cells, cell)

		return nil
	})

	if err != nil {
		return nil, fmt.Errorf("failed to list Cells: %w", err)
	}

	return cells, nil
}

func (g *gitlabCellsStorage) ListPatches(ctx context.Context, amp Cluster) ([]*patch.Metadata, error) {
	var metas []*patch.Metadata
	patchesRoot := amp.PatchesPath() //nolint:wsl

	_, err := g.listTree(ctx, patchesRoot, func(node *gitlab.TreeNode) error {
		if node.Type != "blob" {
			return nil
		}

		if !strings.HasSuffix(node.Path, ".json") {
			return nil
		}

		meta, err := g.getPatch(ctx, node.Path)
		if err != nil {
			return fmt.Errorf("failed to get patch: %w", err)
		}

		metas = append(metas, meta)

		return nil
	})

	if err != nil {
		return nil, fmt.Errorf("failed to list patches: %w", err)
	}

	return metas, nil
}

func (g *gitlabCellsStorage) getCell(ctx context.Context, fileName string, ring Ring) (*cell.Cell, error) {
	path := ring.CellPath(fileName)

	data, _, err := g.git.GetRawFile(g.project, path, &gitlab.GetRawFileOptions{
		Ref: gitlab.Ptr(g.branch),
	}, gitlab.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("failed to get cell %v: %w", path, err)
	}

	r := bytes.NewBuffer(data)

	cell, err := cell.Read(r)
	if err != nil {
		return nil, fmt.Errorf("failed to read cell %v: %w", path, err)
	}

	return cell, nil
}

func (g *gitlabCellsStorage) getPatch(ctx context.Context, patchPath string) (*patch.Metadata, error) {
	data, _, err := g.git.GetRawFile(g.project, patchPath,
		&gitlab.GetRawFileOptions{
			Ref: gitlab.Ptr(g.branch),
		}, gitlab.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("failed to get patch meta %v: %w", patchPath, err)
	}

	r := bytes.NewBuffer(data)

	meta := &patch.Metadata{}
	if err := json.NewDecoder(r).Decode(meta); err != nil {
		return nil, fmt.Errorf("cannot decode a patch %q. %w", patchPath, err)
	}

	return meta, nil
}

func (g *gitlabCellsStorage) ensureBranchExists(ctx context.Context, out io.Writer) error {
	if out == nil {
		out = io.Discard
	}

	_, _, err := g.git.GetBranch(g.project, g.branch, gitlab.WithContext(ctx))
	if err == nil {
		// Branch exists, nothing to do
		return nil
	}

	if !errors.Is(err, gitlab.ErrNotFound) {
		return fmt.Errorf("failed to get branch: %w", err)
	}

	mrURL := g.projectURL.JoinPath("-", "merge_requests", "new")
	queryParams := url.Values{
		"merge_request[source_branch]": []string{g.branch},
	}

	mrURL.RawQuery = queryParams.Encode()

	fmt.Fprintf(out, "New branch: %s\n", g.branch)
	fmt.Fprintf(out, "You can open a merge request visiting\n\t%s\n", mrURL)

	if g.dryRun {
		return nil
	}

	_, _, createErr := g.git.CreateBranch(g.project, &gitlab.CreateBranchOptions{
		Branch: gitlab.Ptr(g.branch),
		Ref:    gitlab.Ptr("refs/heads/main"),
	}, gitlab.WithContext(ctx))
	if createErr != nil {
		return fmt.Errorf("failed to create branch: %w", createErr)
	}

	return nil
}

func (g *gitlabCellsStorage) createCommit(ctx context.Context, msg string, actions []*gitlab.CommitActionOptions) (*gitlab.Commit, error) {
	createCommitOpts := &gitlab.CreateCommitOptions{
		Branch:        gitlab.Ptr(g.branch),
		CommitMessage: gitlab.Ptr(msg),
		Actions:       actions,
	}

	if g.dryRun {
		dryRunCreateCommit(createCommitOpts)

		return &gitlab.Commit{}, nil
	}

	for _, action := range actions {
		slog.LogAttrs(ctx, slog.LevelInfo, "committing file", slog.String("path", *action.FilePath), slog.String("action", string(*action.Action)))
	}

	commit, _, err := g.git.CreateCommit(g.project, createCommitOpts, gitlab.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("failed to create commit: %w", err)
	}

	return commit, nil
}

func patchMetaToCommitActionOptions(meta *patch.Metadata, patchesRoot string) (*gitlab.CommitActionOptions, error) {
	patchMeta, err := json.MarshalIndent(meta, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("cannot marshal patch meta: %w", err)
	}

	return &gitlab.CommitActionOptions{
		Action:   gitlab.Ptr(gitlab.FileUpdate),
		FilePath: gitlab.Ptr(path.Join(patchesRoot, meta.ID+".json")),
		Content:  gitlab.Ptr(string(patchMeta)),
	}, nil
}

func (g *gitlabCellsStorage) LastRing(ctx context.Context, amp Cluster) (int, error) {
	lastRing := int64(-1)

	_, err := g.listTree(ctx, amp.RingsPath(), func(entry *gitlab.TreeNode) error {
		if entry.Type != "tree" {
			return nil
		}

		ring, err := strconv.ParseInt(entry.Name, 10, 0)
		if err != nil {
			slog.LogAttrs(ctx,
				slog.LevelWarn,
				"Unexpected folder in rings",
				slog.String("folder", entry.Name),
				slog.Any("amp", amp),
				slog.String("rings_path", entry.Path),
			)
		} else if ring > lastRing {
			lastRing = ring
		}

		return nil
	})

	return int(lastRing), err
}
