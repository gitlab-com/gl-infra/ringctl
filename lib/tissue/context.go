package tissue

import (
	"context"
	"fmt"
	"log/slog"
)

type ContextKey string

const (
	KeyRingCtlVersion = ContextKey("ringctl_version")
	KeyCIJobURL       = ContextKey("ci_job_url")
)

func CtxWithRingctlVersion(ctx context.Context, version string) context.Context {
	return context.WithValue(ctx, KeyRingCtlVersion, version)
}

func CtxWithCIJobURL(ctx context.Context, url string) context.Context {
	return context.WithValue(ctx, KeyCIJobURL, url)
}

func RingctlVersion(ctx context.Context) string {
	vers := extractString(ctx, KeyRingCtlVersion)
	if vers == "" {
		slog.LogAttrs(ctx, slog.LevelError, "ringctl version not set in context")
	}

	return vers
}

func extractString(ctx context.Context, key ContextKey) string {
	val := ctx.Value(key)
	if str, ok := val.(string); ok {
		return str
	}

	return ""
}

func AppendMetadataTrailers(ctx context.Context, msg *CommitMessage) {
	msg.AddTrailer("Generated-by", fmt.Sprintf("ringctl %s", RingctlVersion(ctx)))

	if ciJobURL := GetCIJobURL(ctx); ciJobURL != "" {
		msg.AddTrailer("Originating-pipeline", ciJobURL)
	}
}

func GetCIJobURL(ctx context.Context) string {
	return extractString(ctx, KeyCIJobURL)
}
