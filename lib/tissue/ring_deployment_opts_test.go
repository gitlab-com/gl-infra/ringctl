package tissue

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRingDeploymentOptions(t *testing.T) {
	t.Run("WithPatchID", func(t *testing.T) {
		opts := &deploymentOpts{}
		WithPatchID("test-patch-id")(opts)
		require.Equal(t, "test-patch-id", opts.patchID)
	})

	t.Run("WithOnlyGitlabUpgrades", func(t *testing.T) {
		t.Run("true", func(t *testing.T) {
			opts := &deploymentOpts{}
			WithOnlyGitlabUpgrade(true)(opts)
			require.True(t, opts.onlyGitlabUpgrade)
		})

		t.Run("false", func(t *testing.T) {
			opts := &deploymentOpts{}
			WithOnlyGitlabUpgrade(false)(opts)
			require.False(t, opts.onlyGitlabUpgrade)
		})
	})

	t.Run("CompleteInstrumentorDeployment", func(t *testing.T) {
	})
}
