package tissue

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRing(t *testing.T) {
	name := "my_cluster"
	amp := AmpCluster(name)

	ring0 := amp.Ring(0)

	assert.Equal(t, name, ring0.AmpCluster())
	assert.Equal(t, 0, ring0.Number())
	assert.Equal(t, "rings/my_cluster/0", ring0.Path())
}

func TestIsValidAmpCluster(t *testing.T) {
	unknownClusterName := "unknown-cluster"
	cluster, err := WellKnownAmpCluster(unknownClusterName)

	assert.Equal(t, unknownClusterName, cluster.Name())
	assert.ErrorIs(t, err, ErrUnknownAmp)
}

func TestValidAmpClusters(t *testing.T) {
	clusters := WellKnownAmpClusters()

	assert.NotEmpty(t, clusters)

	for _, cluster := range clusters {
		testedCluster := cluster
		clusterName := string(cluster)
		t.Run(clusterName, func(t *testing.T) {
			cluster, err := WellKnownAmpCluster(clusterName)

			assert.Equal(t, testedCluster, cluster)
			assert.NoError(t, err)
		})
	}
}

func TestPatchesPath(t *testing.T) {
	cluster, err := WellKnownAmpCluster("cellsdev")
	require.NoError(t, err)

	assert.Equal(t, "patches/cellsdev", cluster.PatchesPath())
}
