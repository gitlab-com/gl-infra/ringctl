package tissue

import (
	"path"
	"strconv"
)

type Ring struct {
	clusterName string
	rootPath    string
	number      int
}

// NewRing returns a Ring struct for the cluster AMP environment.
// The ring number is the identifier for the ring.
// The rootPath is the path to the root of the rings folder in the cells/tissue repository.
func NewRing(cluster, rootPath string, number int) Ring {
	return Ring{
		clusterName: cluster,
		rootPath:    rootPath,
		number:      number,
	}
}

func (r Ring) AmpCluster() string {
	return r.clusterName
}

func (r Ring) Path() string {
	return path.Join(r.rootPath, r.String())
}

func (r Ring) CellPath(fileName string) string {
	return path.Join(r.Path(), fileName)
}

func (r Ring) Number() int {
	return r.number
}

func (r Ring) String() string {
	return strconv.FormatInt(int64(r.number), 10)
}
