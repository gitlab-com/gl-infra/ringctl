package tissue_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/test"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

func TestListCells(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	cluster := test.GetFixturesCluster()
	client := tissue.LocalDiskStorage{}
	ring0, err := client.ListCells(ctx, cluster.Ring(0))
	require.NoError(t, err, "cannot list cells in ring 0")
	require.NotEmpty(t, ring0, "no cells found in ring 0")

	ring1, err := client.ListCells(ctx, cluster.Ring(1))
	require.NoError(t, err, "cannot list cells in ring 1")
	require.NotEmpty(t, ring1, "no cells found in ring 1")

	ring2, err := client.ListCells(ctx, cluster.Ring(2))
	require.NoError(t, err, "cannot list cells in ring 2")
	require.NotEmpty(t, ring2, "no cells found in ring 2")

	require.Len(t, ring0, 3)
	require.Len(t, ring1, 1)
	require.Len(t, ring2, 2)

	cellc02e := ring1[0]

	require.Equal(t, "cellc02e", cellc02e.TenantID())
	require.Equal(t, "cellc02e.json", cellc02e.TenantModelFileName())
	require.Equal(t, "16.10.202403071400-a5c67111ad3.727b99aba03", cellc02e.PrereleaseVersion())
}

func TestLastRingLocal(t *testing.T) {
	ctx := context.Background()

	cluster := test.GetFixturesCluster()
	client := tissue.LocalDiskStorage{}
	lastRing, err := client.LastRing(ctx, cluster)
	require.NoError(t, err, "cannot find the last ring")
	require.Equal(t, 2, lastRing)
}

func TestGetPatchLocal(t *testing.T) {
	ctx := context.Background()

	cluster := test.GetFixturesCluster()
	client := tissue.LocalDiskStorage{}

	expectedPatch, err := patch.MetadataFromJSON(test.Patch01JAQKKQQ5PHP7EF7T24BF0JPAFixture1)
	require.NoError(t, err)

	tests := []struct {
		name         string
		patchID      string
		expectedErr  error
		expectedMeta *patch.Metadata
	}{
		{
			name:         "patch exists",
			patchID:      test.PatchIDFixture1,
			expectedMeta: expectedPatch,
		},
		{
			name:        "patch does not exists",
			patchID:     "not-existing-patch",
			expectedErr: tissue.ErrPatchNotFound,
		},
		{
			name:        "path traversal",
			patchID:     "../../rings/fixture1/0/cell1b22",
			expectedErr: tissue.ErrPatchNotFound,
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			patch, err := client.GetPatch(ctx, cluster, testCase.patchID)

			if testCase.expectedErr != nil {
				require.ErrorIs(t, err, testCase.expectedErr)
			} else {
				require.NoError(t, err)
			}

			require.Equal(t, testCase.expectedMeta, patch)
		})
	}
}
