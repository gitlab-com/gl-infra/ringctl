package tissue

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVersionInContext(t *testing.T) {
	tests := []struct {
		name            string
		ctx             context.Context //nolint:containedctx
		expectedVersion string
	}{
		{
			name:            "not set",
			ctx:             context.Background(),
			expectedVersion: "",
		},
		{
			name:            "with version",
			ctx:             CtxWithRingctlVersion(context.Background(), "a version"),
			expectedVersion: "a version",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			version := RingctlVersion(tt.ctx)

			assert.Equal(t, tt.expectedVersion, version)
		})
	}
}
