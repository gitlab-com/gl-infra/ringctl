## What


This merge request adds the patch ID `{{ .Metadata.ID }}` to `{{ .Cluster.Name }}` with priority `{{ .Metadata.Priority }}`.

---
This MR was generated using 🤖 [ringctl](https://gitlab.com/gitlab-com/gl-infra/ringctl)

/assign me
/label ~"tissue-patch"
