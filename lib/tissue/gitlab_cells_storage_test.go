package tissue

import (
	"context"
	"encoding/json"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

func TestPatchMRDescription(t *testing.T) {
	data := &patchMrTemplateData{
		Cluster: AmpCluster("test-cluster"),
		Metadata: &patch.Metadata{
			ID:       "the-patch-id",
			Priority: 0,
		},
	}
	description, err := createPatchMergeRequestDescription(data)
	require.NoError(t, err)

	assert.Containsf(t, description, "/assign me", "The MR should be self assigned")
	assert.Containsf(t, description, "/label ~\"tissue-patch\"", "The MR should be labeled with ~tissue-patch")
	assert.Containsf(t, description, "`test-cluster`", "PatchesRoot not present")
	assert.Containsf(t, description, "`the-patch-id`", "Patch ID not present")
	assert.Containsf(t, description, "priority `0`", "Priority not mentioned")
}

func TestGetPatchGitlab(t *testing.T) {
	client := gitlab.NewMockClient(t)
	amp := DevCluster
	aPatch := &patch.Metadata{ID: "a-patch", Priority: 5}
	patchID := aPatch.ID

	expectedFilePath := path.Join(amp.PatchesPath(), patchID+".json")
	patchRaw, err := json.Marshal(aPatch)
	require.NoError(t, err)

	client.EXPECT().
		GetRawFile(mock.Anything, expectedFilePath, mock.Anything, mock.Anything).
		Return(patchRaw, nil, nil)

	storage := &gitlabCellsStorage{
		git: client,
	}

	patch, err := storage.GetPatch(context.Background(), amp, patchID)
	require.NoError(t, err)
	require.Equal(t, aPatch, patch)
}
