// Code generated by mockery v2.52.2. DO NOT EDIT.

package tissue

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	patch "gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

// MockIncidentEvents is an autogenerated mock type for the IncidentEvents type
type MockIncidentEvents struct {
	mock.Mock
}

type MockIncidentEvents_Expecter struct {
	mock *mock.Mock
}

func (_m *MockIncidentEvents) EXPECT() *MockIncidentEvents_Expecter {
	return &MockIncidentEvents_Expecter{mock: &_m.Mock}
}

// FireAlert provides a mock function with given fields: ctx, _a1, sourceURL
func (_m *MockIncidentEvents) FireAlert(ctx context.Context, _a1 *patch.Metadata, sourceURL string) error {
	ret := _m.Called(ctx, _a1, sourceURL)

	if len(ret) == 0 {
		panic("no return value specified for FireAlert")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *patch.Metadata, string) error); ok {
		r0 = rf(ctx, _a1, sourceURL)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// MockIncidentEvents_FireAlert_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'FireAlert'
type MockIncidentEvents_FireAlert_Call struct {
	*mock.Call
}

// FireAlert is a helper method to define mock.On call
//   - ctx context.Context
//   - _a1 *patch.Metadata
//   - sourceURL string
func (_e *MockIncidentEvents_Expecter) FireAlert(ctx interface{}, _a1 interface{}, sourceURL interface{}) *MockIncidentEvents_FireAlert_Call {
	return &MockIncidentEvents_FireAlert_Call{Call: _e.mock.On("FireAlert", ctx, _a1, sourceURL)}
}

func (_c *MockIncidentEvents_FireAlert_Call) Run(run func(ctx context.Context, _a1 *patch.Metadata, sourceURL string)) *MockIncidentEvents_FireAlert_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(*patch.Metadata), args[2].(string))
	})
	return _c
}

func (_c *MockIncidentEvents_FireAlert_Call) Return(_a0 error) *MockIncidentEvents_FireAlert_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *MockIncidentEvents_FireAlert_Call) RunAndReturn(run func(context.Context, *patch.Metadata, string) error) *MockIncidentEvents_FireAlert_Call {
	_c.Call.Return(run)
	return _c
}

// ResolveAlert provides a mock function with given fields: ctx, _a1, sourceURL
func (_m *MockIncidentEvents) ResolveAlert(ctx context.Context, _a1 *patch.Metadata, sourceURL string) error {
	ret := _m.Called(ctx, _a1, sourceURL)

	if len(ret) == 0 {
		panic("no return value specified for ResolveAlert")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *patch.Metadata, string) error); ok {
		r0 = rf(ctx, _a1, sourceURL)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// MockIncidentEvents_ResolveAlert_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'ResolveAlert'
type MockIncidentEvents_ResolveAlert_Call struct {
	*mock.Call
}

// ResolveAlert is a helper method to define mock.On call
//   - ctx context.Context
//   - _a1 *patch.Metadata
//   - sourceURL string
func (_e *MockIncidentEvents_Expecter) ResolveAlert(ctx interface{}, _a1 interface{}, sourceURL interface{}) *MockIncidentEvents_ResolveAlert_Call {
	return &MockIncidentEvents_ResolveAlert_Call{Call: _e.mock.On("ResolveAlert", ctx, _a1, sourceURL)}
}

func (_c *MockIncidentEvents_ResolveAlert_Call) Run(run func(ctx context.Context, _a1 *patch.Metadata, sourceURL string)) *MockIncidentEvents_ResolveAlert_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(*patch.Metadata), args[2].(string))
	})
	return _c
}

func (_c *MockIncidentEvents_ResolveAlert_Call) Return(_a0 error) *MockIncidentEvents_ResolveAlert_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *MockIncidentEvents_ResolveAlert_Call) RunAndReturn(run func(context.Context, *patch.Metadata, string) error) *MockIncidentEvents_ResolveAlert_Call {
	_c.Call.Return(run)
	return _c
}

// NewMockIncidentEvents creates a new instance of MockIncidentEvents. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMockIncidentEvents(t interface {
	mock.TestingT
	Cleanup(func())
}) *MockIncidentEvents {
	mock := &MockIncidentEvents{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
