package tissue

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

const newDirPermission os.FileMode = 0755

var ErrPathTraversal = fmt.Errorf("path traversal attempt detected")
var ErrEmptyFilename = fmt.Errorf("empty filename not allowed")

type UnknownActionError struct {
	Action CellsAction
}

func (e *UnknownActionError) Error() string {
	return fmt.Sprintf("Unknow action %q", e.Action)
}

type LocalDiskStorage struct {
}

func joinNoPathTraversal(baseDir, filename string) (string, error) {
	baseDir, err := filepath.Abs(baseDir)
	if err != nil {
		return "", fmt.Errorf("impossible to get baseDir absolute path: %w", ErrPathTraversal)
	}

	fullPath := path.Clean(path.Join(baseDir, filename))

	if fullPath == baseDir {
		return "", ErrEmptyFilename
	}

	if !strings.HasPrefix(fullPath, baseDir) {
		return "", ErrPathTraversal
	}

	return fullPath, nil
}

func openNoPathTraversal(baseDir, filename string) (*os.File, error) {
	fullPath, err := joinNoPathTraversal(baseDir, filename)
	if err != nil {
		return nil, err
	}

	file, err := os.Open(fullPath)
	if err != nil {
		return nil, fmt.Errorf("failed to open file: %w", err)
	}

	return file, nil
}

func (l *LocalDiskStorage) AddPatch(ctx context.Context, amp Cluster, meta *patch.Metadata) error {
	if len(meta.Patch) == 0 {
		return fmt.Errorf("patch not defined: %w", ErrNoPatchSpecified)
	}

	if errMkdir := os.MkdirAll(amp.PatchesPath(), newDirPermission); errMkdir != nil {
		return fmt.Errorf("cannot create the patch folder: %w", errMkdir)
	}

	if err := l.UpdatePatchStatus(ctx, amp, meta); err != nil {
		return fmt.Errorf("cannot write patch metadata: %w", err)
	}

	return nil
}

func (l *LocalDiskStorage) GetPatch(ctx context.Context, amp Cluster, patchID string) (*patch.Metadata, error) {
	fileName := patchID + ".json"

	meta, err := readPatch(amp.PatchesPath(), fileName)
	if err != nil {
		return nil, fmt.Errorf("%w :%w", ErrPatchNotFound, err)
	}

	return meta, nil
}

func (l *LocalDiskStorage) DeletePatch(ctx context.Context, amp Cluster, patchID string) error {
	fileName := patchID + ".json"

	patchPath, err := joinNoPathTraversal(amp.PatchesPath(), fileName)
	if err != nil {
		return fmt.Errorf("cannot find patch. %w", err)
	}

	if err := os.Remove(patchPath); err != nil {
		return fmt.Errorf("cannot delete patch. %w", err)
	}

	slog.LogAttrs(
		ctx,
		slog.LevelInfo,
		"patch deleted",
		slog.String("patch_id", patchID),
	)

	return nil
}

func (l *LocalDiskStorage) UpdatePatchStatus(_ context.Context, amp Cluster, meta *patch.Metadata) error {
	metaPath := path.Join(amp.PatchesPath(), meta.ID+".json")

	f, err := os.Create(metaPath)
	if err != nil {
		return fmt.Errorf("cannot create the patch meta file: %w", err)
	}

	defer f.Close()

	metaEnc := json.NewEncoder(f)
	metaEnc.SetIndent("", "  ")

	if errEnc := metaEnc.Encode(meta); errEnc != nil {
		return fmt.Errorf("cannot encode the patch meta: %w", errEnc)
	}

	return nil
}

func (l *LocalDiskStorage) ListPatches(_ context.Context, amp Cluster) ([]*patch.Metadata, error) {
	rootFolder := amp.PatchesPath()

	patches, err := os.ReadDir(rootFolder)
	if err != nil {
		return nil, fmt.Errorf("cannot list patches. %w", err)
	}

	metas := make([]*patch.Metadata, 0, len(patches))

	for _, patchMeta := range patches {
		if patchMeta.IsDir() {
			continue
		}

		if !strings.HasSuffix(patchMeta.Name(), ".json") {
			continue
		}

		meta, err := readPatch(rootFolder, patchMeta.Name())
		if err != nil {
			return nil, fmt.Errorf("cannot read patch %q: %w", patchMeta.Name(), err)
		}

		metas = append(metas, meta)
	}

	return metas, nil
}

func (l *LocalDiskStorage) UpdateWithPatch(ctx context.Context, amp Cluster, patch *patch.Metadata, cells []*cell.Cell, commitMsg string) error {
	ring := amp.Ring(patch.Ring)
	if err := l.Update(ctx, ring, cells, EditCells, ""); err != nil {
		return err
	}

	return l.UpdatePatchStatus(ctx, amp, patch)
}

func (l *LocalDiskStorage) Update(_ context.Context, ring Ring, cells []*cell.Cell, action CellsAction, commitMsg string) error {
	for _, cell := range cells {
		tenantModelPath := ring.CellPath(cell.TenantModelFileName())

		switch action {
		case DeleteCell:
			if err := os.Remove(tenantModelPath); err != nil {
				return fmt.Errorf("cannot delete cell: %w", err)
			}
		case EditCells, CreateCells:
			if err := l.writeCell(tenantModelPath, cell); err != nil {
				return err
			}
		default:
			return &UnknownActionError{Action: action}
		}
	}

	return nil
}

func (l *LocalDiskStorage) writeCell(tenantModelPath string, cell *cell.Cell) error {
	folder := path.Dir(tenantModelPath)
	if err := os.MkdirAll(folder, newDirPermission); err != nil {
		return fmt.Errorf("cannot create the ring folder: %w", err)
	}

	f, err := os.Create(tenantModelPath)
	if err != nil {
		return fmt.Errorf("cannot create the tenant model file: %w", err)
	}

	defer f.Close()

	content, err := cell.ToString()
	if err != nil {
		return fmt.Errorf("cannot convert the tenant model to JSON: %w", err)
	}

	_, err = f.WriteString(content)
	if err != nil {
		return fmt.Errorf("cannot write the tenant model to file: %w", err)
	}

	return nil
}

func (l *LocalDiskStorage) ListCells(_ context.Context, ring Ring) ([]*cell.Cell, error) {
	cells := make([]*cell.Cell, 0)
	ringFolder := ring.Path()

	models, err := os.ReadDir(ringFolder)
	if err != nil {
		return nil, fmt.Errorf("cannot read the ring folder: %w", err)
	}

	for _, model := range models {
		if model.IsDir() {
			continue
		}

		if !strings.HasSuffix(model.Name(), ".json") {
			continue
		}

		f, err := openNoPathTraversal(ringFolder, model.Name())
		if err != nil {
			return nil, fmt.Errorf("cannot open the tenant model file: %w", err)
		}
		defer f.Close()

		cell, err := cell.Read(f)
		if err != nil {
			return nil, fmt.Errorf("cannot parse the tenant model file: %w", err)
		}

		cells = append(cells, cell)
	}

	return cells, nil
}

func (l *LocalDiskStorage) LastRing(ctx context.Context, amp Cluster) (int, error) {
	entries, err := os.ReadDir(amp.RingsPath())
	if err != nil {
		return -1, fmt.Errorf("cannot read the ring folder: %w", err)
	}

	lastRing := int64(-1)

	for _, entry := range entries {
		if !entry.IsDir() {
			continue
		}

		ring, err := strconv.ParseInt(entry.Name(), 10, 0)
		if err != nil {
			slog.LogAttrs(ctx,
				slog.LevelWarn,
				"Unexpected folder in rings",
				slog.String("folder", entry.Name()),
				slog.Any("amp", amp),
				slog.String("rings_path", amp.RingsPath()),
			)
		} else if ring > lastRing {
			lastRing = ring
		}
	}

	return int(lastRing), nil
}

func readPatch(rootFolder, fileName string) (*patch.Metadata, error) {
	f, err := openNoPathTraversal(rootFolder, fileName)
	if err != nil {
		return nil, fmt.Errorf("cannot open a patch. %w", err)
	}
	defer f.Close()

	meta := &patch.Metadata{}
	if err := json.NewDecoder(f).Decode(meta); err != nil {
		return nil, fmt.Errorf("cannot decode a patch %q. %w", f.Name(), err)
	}

	return meta, nil
}
