package tissue

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/url"
	"os"
	"strconv"

	jsonpatch "github.com/evanphx/json-patch/v5"
	gitlab "gitlab.com/gitlab-org/api/client-go"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/cell"
	gl "gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

type Client struct {
	instance   string
	project    string
	branch     string
	dryRun     bool
	ampCluster Cluster
	local      bool

	projectURL url.URL
	git        gl.Client
	storage    CellsStorage

	alertManager IncidentEvents
}

type Config struct {
	Token      string
	Instance   string
	Project    string
	Branch     string
	AmpCluster Cluster
	DryRun     bool
	Local      bool

	Storage      CellsStorage
	Gitlab       gl.Client
	AlertManager IncidentEvents
}

type IncidentEvents interface {
	FireAlert(ctx context.Context, patch *patch.Metadata, sourceURL string) error
	ResolveAlert(ctx context.Context, patch *patch.Metadata, sourceURL string) error
}

const defaultBranch = "main"

var ErrCastToGitlabStorage = errors.New("cannot cast storage to gitlabCellsStorage")
var ErrNoPatchSpecified = errors.New("no patch specified")
var ErrConflictingDeploymentArgumnts = errors.New("conflicting deployment arguments")

func logError(err error) slog.Attr {
	return slog.Any("error", err)
}

func NewClient(ctx context.Context, config *Config) (*Client, error) {
	instanceURL, errParse := url.Parse(config.Instance)
	if errParse != nil {
		slog.LogAttrs(ctx, slog.LevelError, "Failed to parse GitLab instance",
			logError(errParse), slog.String("instance", config.Instance),
		)

		return nil, fmt.Errorf("failed to parse GitLab instance URL: %w", errParse)
	}

	apiURL := instanceURL.JoinPath("api", "v4")
	projectURL := instanceURL.JoinPath(config.Project)

	var git gl.Client

	if config.Gitlab != nil {
		git = config.Gitlab
	} else {
		gitlabCLient, err := gl.NewClient(config.Token, gitlab.WithBaseURL(apiURL.String()))
		if err != nil {
			slog.LogAttrs(ctx, slog.LevelError, "Failed to create client", logError(err))

			return nil, fmt.Errorf("failed to create client: %w", err)
		}

		git = gitlabCLient
	}

	var storage CellsStorage

	if config.Storage != nil {
		storage = config.Storage
	} else if config.Local {
		storage = &LocalDiskStorage{}
	} else {
		storage = &gitlabCellsStorage{
			dryRun:     config.DryRun,
			git:        git,
			project:    config.Project,
			branch:     config.Branch,
			projectURL: *projectURL,
		}
	}

	slog.LogAttrs(ctx, slog.LevelInfo, "tissue client initialized",
		slog.String("instance", config.Instance),
		slog.String("branch", config.Branch),
		slog.Bool("dry_run", config.DryRun),
		slog.String("amp", config.AmpCluster.Name()),
		slog.String("project", config.Project),
		slog.String("version", RingctlVersion(ctx)),
		slog.Bool("local", config.Local))

	return &Client{
		instance:   config.Instance,
		project:    config.Project,
		branch:     config.Branch,
		dryRun:     config.DryRun,
		ampCluster: config.AmpCluster,
		local:      config.Local,

		git:          git,
		projectURL:   *projectURL,
		storage:      storage,
		alertManager: config.AlertManager,
	}, nil
}

// Local returns true when the client is working on local files.
func (c *Client) Local() bool {
	return c.local
}

func (c *Client) UploadCells(ctx context.Context, ring Ring, cells []*cell.Cell, commitMsg *CommitMessage) error {
	if len(cells) == 0 {
		return nil
	}

	err := c.storage.Update(ctx,
		ring,
		cells,
		EditCells,
		commitMsg.String())
	if err != nil {
		return fmt.Errorf("cannot upload cells to ring %s. %w", ring, err)
	}

	return nil
}

func (c *Client) UploadCell(ctx context.Context, ring Ring, model *cell.Cell) error {
	msg := NewCommitMessage(
		c.ampCluster,
		fmt.Sprintf("Add cell %s to ring %s", model.TenantID(), ring),
	)
	AppendMetadataTrailers(ctx, msg)

	err := c.storage.Update(ctx,
		ring,
		[]*cell.Cell{model},
		CreateCells,
		msg.String())
	if err != nil {
		return fmt.Errorf("cannot upload cell %s to ring %s. %w", model.TenantID(), ring, err)
	}

	return nil
}

func (c *Client) AddPatch(ctx context.Context, meta *patch.Metadata) error {
	return c.storage.AddPatch(ctx, c.ampCluster, meta) //nolint:wrapcheck
}

func (c *Client) DeletePatch(ctx context.Context, patchID string) error {
	return c.storage.DeletePatch(ctx, c.ampCluster, patchID) //nolint:wrapcheck
}

func (c *Client) UpdatePatchStatus(ctx context.Context, meta *patch.Metadata) error {
	return c.storage.UpdatePatchStatus(ctx, c.ampCluster, meta) //nolint:wrapcheck
}

func (c *Client) RemoveCell(ctx context.Context, ring Ring, model *cell.Cell) error {
	msg := NewCommitMessage(c.ampCluster, fmt.Sprintf("Remove cell %s from ring %s", model.TenantID(), ring))
	AppendMetadataTrailers(ctx, msg)

	err := c.storage.Update(ctx,
		ring,
		[]*cell.Cell{model},
		DeleteCell,
		msg.String(),
	)

	if err != nil {
		return fmt.Errorf("cannot remove cell %s from ring %s. %w", model.TenantID(), ring, err)
	}

	return nil
}

func (c *Client) TearDownCell(ctx context.Context, ring Ring, cell *cell.Cell) (*Pipeline, error) {
	vars := []*gitlab.PipelineVariableOptions{
		{
			Key:   gitlab.Ptr(CIVariableAMPEnvironment),
			Value: gitlab.Ptr(ring.AmpCluster()),
		},
		{
			Key:   gitlab.Ptr(CIVariableCellID),
			Value: gitlab.Ptr(cell.TenantID()),
		},
		{
			Key:   gitlab.Ptr(CIVariableRing),
			Value: gitlab.Ptr(ring.String()),
		},
		{
			Key:   gitlab.Ptr("TEAR_DOWN"),
			Value: gitlab.Ptr("true"),
		},
	}
	createPipelineOpts := &gitlab.CreatePipelineOptions{
		Ref:       gitlab.Ptr(c.branch),
		Variables: &vars,
	}

	if c.dryRun {
		dryRunCreatePipeline(&c.projectURL, createPipelineOpts)

		return nil, nil
	}

	p, _, err := c.git.CreatePipeline(c.project, createPipelineOpts, gitlab.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("failed to create pipeline: %w", err)
	}

	slog.LogAttrs(
		ctx,
		slog.LevelDebug,
		"a tear down pipeline started",
		slog.String("url", p.WebURL),
	)

	return newPipeline(p), nil
}

func (c *Client) ListCells(ctx context.Context, ring Ring) ([]*cell.Cell, error) {
	return c.storage.ListCells(ctx, ring) //nolint:wrapcheck
}

func (c *Client) Deployment(ctx context.Context, ring Ring, options ...DeploymentOptionFunc) (*Pipeline, error) {
	config := new(deploymentOpts)
	for _, fn := range options {
		fn(config)
	}

	if config.patchID != "" && config.cellID != "" {
		return nil, ErrConflictingDeploymentArgumnts
	}

	vars := []*gitlab.PipelineVariableOptions{
		{
			Key:   gitlab.Ptr(CIVariableAMPEnvironment),
			Value: gitlab.Ptr(c.ampCluster.Name()),
		},
		{
			Key:   gitlab.Ptr(CIVariableRing),
			Value: gitlab.Ptr(ring.String()),
		},
		{
			Key:   gitlab.Ptr(CIVariableOnlyGitlabUpgrade),
			Value: gitlab.Ptr(strconv.FormatBool(config.onlyGitlabUpgrade)),
		},
	}

	if config.patchID != "" {
		vars = append(vars, &gitlab.PipelineVariableOptions{
			Key:   gitlab.Ptr(CIVariablePatchID),
			Value: gitlab.Ptr(config.patchID),
		})
	}

	if config.cellID != "" {
		vars = append(vars,
			&gitlab.PipelineVariableOptions{
				Key:   gitlab.Ptr(CIVariableCellID),
				Value: gitlab.Ptr(config.cellID),
			})
	}

	if config.rollback {
		vars = append(vars, &gitlab.PipelineVariableOptions{
			Key:   gitlab.Ptr(CIVariableRollback),
			Value: gitlab.Ptr("true"),
		})
	}

	createPipelineOpts := &gitlab.CreatePipelineOptions{
		Ref:       gitlab.Ptr(c.branch),
		Variables: &vars,
	}

	if c.dryRun {
		dryRunCreatePipeline(&c.projectURL, createPipelineOpts)

		return nil, nil
	}

	p, _, err := c.git.CreatePipeline(c.project, createPipelineOpts, gitlab.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("failed to create pipeline: %w", err)
	}

	return newPipeline(p), nil
}

func (c *Client) GetPipeline(ctx context.Context, id int) (*Pipeline, error) {
	p, _, err := c.git.GetPipeline(c.project, id, gitlab.WithContext(ctx))

	return newPipeline(p), err
}

func dryRunCreateCommit(opts *gitlab.CreateCommitOptions) {
	fmt.Printf("[DRY-RUN] CreateCommit - branch: %s\n", *opts.Branch)
	fmt.Printf("[DRY-RUN] CreateCommit - message: %s\n", *opts.CommitMessage)

	for _, action := range opts.Actions {
		fmt.Printf("[DRY-RUN] CreateCommit - action: %s - path: %s\n", *action.Action, *action.FilePath)

		if action.Content != nil {
			fmt.Println(*action.Content)
		}
	}
}

func dryRunCreatePipeline(projectURL *url.URL, opts *gitlab.CreatePipelineOptions) {
	pipelineURL := projectURL.JoinPath("-", "pipelines", "new")
	queryParams := url.Values{}

	queryParams.Add("ref", *opts.Ref)
	fmt.Printf("[DRY-RUN] CreatePipeline - ref: %s\n", *opts.Ref)

	for _, variable := range *opts.Variables {
		queryParams.Add(fmt.Sprintf("var[%s]", *variable.Key), *variable.Value)
		fmt.Printf("[DRY-RUN] \tvariable %q: %q\n", *variable.Key, *variable.Value)
	}

	pipelineURL.RawQuery = queryParams.Encode()
	fmt.Printf("Yon can start this pipeline visiting\n\t%s\n", pipelineURL)
}

func (c *Client) listCellsOnMain(ctx context.Context, ring Ring) ([]*cell.Cell, error) {
	gtlb, ok := c.storage.(*gitlabCellsStorage)
	if !ok {
		return nil, ErrCastToGitlabStorage
	}

	gtlb.branch = defaultBranch
	defer func() {
		gtlb.branch = c.branch
	}()

	return c.ListCells(ctx, ring)
}

// Patch applies a patch to the whole ring.
func (c *Client) Patch(ctx context.Context, meta *patch.Metadata) error {
	amp := c.ampCluster
	ring := amp.Ring(meta.Ring)

	patch, err := jsonpatch.DecodePatch(meta.Patch)
	if err != nil {
		return fmt.Errorf("cannot decode patch: %w", err)
	}

	cells, err := c.ListCells(ctx, ring)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			slog.LogAttrs(ctx, slog.LevelWarn, "ring is empty", slog.Int("ring", meta.Ring))

			return nil
		}

		// When the targeted branch does not exist, retrieving cells from the main branch is possible.
		// This is useful when creating a new branch for the first time.
		if errors.Is(err, gitlab.ErrNotFound) && c.branch != defaultBranch {
			cells, err = c.listCellsOnMain(ctx, ring)
		}

		if err != nil {
			return fmt.Errorf("failed to list cells: %w", err)
		}
	}

	patchedCells := make([]*cell.Cell, 0, len(cells))

	for _, cell := range cells {
		patched, patchErr := cell.Patch(patch)
		if patchErr != nil {
			return fmt.Errorf("cannot patch cell: %w", patchErr)
		}

		patchedCells = append(patchedCells, patched)
		fmt.Printf("%s: Version: %s - Prerelease version: %s - URL: %s\n",
			patched.TenantID(), patched.Version(), patched.PrereleaseVersion(), patched.URL())
	}

	commitMsg := c.generatePatchCommitMessage(ctx, meta)

	var updateError error
	if meta.ID == "" { // apply patch from local file - no tracking metadata
		updateError = c.UploadCells(ctx, ring, patchedCells, commitMsg)
	} else {
		updateError = c.storage.UpdateWithPatch(ctx, amp, meta, patchedCells, commitMsg.String())
	}
	if updateError != nil { //nolint:wsl
		return fmt.Errorf("failed to upload cells: %w", updateError)
	}

	return nil
}

func (c *Client) ListPatches(ctx context.Context) (patch.PatchesByRing, error) {
	metas, err := c.storage.ListPatches(ctx, c.ampCluster)
	if err != nil {
		return nil, err //nolint:wrapcheck
	}

	return patch.Sort(metas), nil
}

func (c *Client) GetPatch(ctx context.Context, patchID string) (*patch.Metadata, error) {
	return c.storage.GetPatch(ctx, c.ampCluster, patchID) //nolint:wrapcheck
}

func (c *Client) LastRing(ctx context.Context) (int, error) {
	return c.storage.LastRing(ctx, c.ampCluster) //nolint:wrapcheck
}

func (c *Client) generatePatchCommitMessage(ctx context.Context, meta *patch.Metadata) *CommitMessage {
	commitMsg := NewCommitMessage(
		c.ampCluster,
		fmt.Sprintf("Apply patch %s in ring %d", meta.ID, meta.Ring))

	commitMsg.AppendLine("\n\n```json")
	commitMsg.AppendLine(string(meta.Patch))
	commitMsg.AppendLine("```")
	commitMsg.SkipCI()

	if meta.ID != "" {
		commitMsg.AddTrailer("Patch-ID", meta.ID)
	}

	commitMsg.AddTrailer("Patch-Priority", fmt.Sprintf("%d", meta.Priority))
	AppendMetadataTrailers(ctx, commitMsg)

	for _, link := range meta.RelatedTo {
		commitMsg.AddTrailer("Related-to", link)
	}

	return commitMsg
}

// FireAlert implements IncidentEvents interface by calling the same function in alertManager.
func (c *Client) FireAlert(ctx context.Context, patch *patch.Metadata, sourceURL string) error {
	return c.alertManager.FireAlert(ctx, patch, sourceURL) //nolint:wrapcheck
}

// ResolveAlert implements IncidentEvents interface by calling the same function in alertManager.
func (c *Client) ResolveAlert(ctx context.Context, patch *patch.Metadata, sourceURL string) error {
	return c.alertManager.ResolveAlert(ctx, patch, sourceURL) //nolint:wrapcheck
}
