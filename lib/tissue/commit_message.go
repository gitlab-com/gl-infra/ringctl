package tissue

import (
	"fmt"
	"strings"
)

const skipCIMarker = "[SKIP-CI]"

// CommitMessage is used to create a commit message in a human-readable way.
type CommitMessage struct {
	description strings.Builder
	trailers    []string

	finalized bool
}

// NewCommitMessage creates a new commit message with the given title.
//
// title is prefixed with the cluster name in square brackets.
// Extra lines can be added using the AppendLine method.
// Git trailers can be added using the AddTrailer method.
// Calling SkipCI will add the [SKIP-CI] marker.
//
// Changing the commit message after calling the String method will have no effect.
func NewCommitMessage(amp Cluster, title string) *CommitMessage {
	commitMsg := &CommitMessage{}
	commitMsg.AppendLine(fmt.Sprintf("[%s] %s\n", amp.Name(), title))

	return commitMsg
}

// AppendLine appends a line to the description.
// Lines added after String() call will be ignored.
func (c *CommitMessage) AppendLine(msg string) {
	if c.finalized {
		return
	}

	// Builder.WriteString always returns a nil error
	_, _ = c.description.WriteString(msg + "\n")
}

// SkipCI adds a marker to the commit message to skip CI.
func (c *CommitMessage) SkipCI() {
	c.AppendLine(skipCIMarker)
}

// AddTrailer adds a trailer to the commit message.
// Trailers will be appended to the description only when the String() method is called.
// Trailers added after String() call will be ignored.
func (c *CommitMessage) AddTrailer(key, value string) {
	if c.finalized {
		return
	}

	c.trailers = append(c.trailers, fmt.Sprintf("%s: %s", key, value))
}

func (c *CommitMessage) String() string {
	if !c.finalized {
		if len(c.trailers) > 0 {
			c.AppendLine("")
		}

		for _, trailer := range c.trailers {
			c.AppendLine(trailer)
		}

		c.finalized = true
	}

	return c.description.String()
}
