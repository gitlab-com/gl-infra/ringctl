package tissue

type deploymentOpts struct {
	cellID            string
	patchID           string
	onlyGitlabUpgrade bool
	rollback          bool
}

const (
	// CIVariablePatchID is the name of the CI variable used to store the patch ID.
	CIVariablePatchID = "PATCH_ID"

	// CIVariableAMPEnvironment is the name of the CI variable used to store the AMP environment.
	CIVariableAMPEnvironment = "AMP_ENVIRONMENT"

	// CIVariableOnlyGitlabUpgrade is the name of the CI variable used to store the only gitlab upgrades flag.
	CIVariableOnlyGitlabUpgrade = "CONFIGURE_ONLY"

	// CIVariableRollback is the name of the CI variable used to store the rollback flag.
	CIVariableRollback = "ROLLBACK"

	// CIVariableCellID is the name of the CI variable used to store the cell ID.
	CIVariableCellID = "TENANT_ID"

	// CIVariableRing is the name of the CI variable used to store the ring.
	CIVariableRing = "RING"
)

type DeploymentOptionFunc func(*deploymentOpts)

// IsRollback sets the ring deployment to be a rollback.
func IsRollback(status bool) DeploymentOptionFunc {
	return func(o *deploymentOpts) {
		o.rollback = status
	}
}

// WithPatchID sets the patch ID for the ring deployment.
//
// Setting the PatchID is not compatible with setting the CellID.
func WithPatchID(ID string) DeploymentOptionFunc {
	return func(o *deploymentOpts) {
		o.patchID = ID
	}
}

// WithCellID sets the cell ID for the deployment.
//
// Setting the CellID is not compatible with setting the PatchID.
func WithCellID(ID string) DeploymentOptionFunc {
	return func(o *deploymentOpts) {
		o.cellID = ID
	}
}

// WithOnlyGitlabUpgrade sets the ring deployment to only upgrade GitLab.
//
// If status is true, only a subset of the configure stage is executed.
// This option should only be used when the only change is the GitLab version.
// If status is false, all the instrumentor stages are executed.
func WithOnlyGitlabUpgrade(status bool) DeploymentOptionFunc {
	return func(o *deploymentOpts) {
		o.onlyGitlabUpgrade = status
	}
}
