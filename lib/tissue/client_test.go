package tissue

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

type ClientOptionFunc func(*Client) error

func TestNewClientProjectURL(t *testing.T) {
	config := Config{
		Instance:   "https://example.com",
		Project:    "my-group/my-project",
		AmpCluster: DevCluster,
	}

	client, err := NewClient(context.Background(), &config)
	require.NotNil(t, client)
	require.NoError(t, err)

	require.Equal(t, "https://example.com/my-group/my-project", client.projectURL.String())
}

func TestListCells(t *testing.T) {
	ctx := context.Background()
	amp := DevCluster
	ring := amp.Ring(42)
	anError := fmt.Errorf("an error") //nolint:goerr113

	storage := NewMockCellsStorage(t)
	storage.EXPECT().ListCells(ctx, ring).Return(nil, anError).Once()

	client, err := NewClient(ctx, &Config{
		Instance:   "https://example.com",
		Project:    "my-group/my-project",
		Branch:     defaultBranch,
		AmpCluster: DevCluster,
	})
	require.NotNil(t, client)
	require.NoError(t, err)

	client.storage = storage

	cells, err := client.ListCells(ctx, ring)
	require.Nil(t, cells)
	require.ErrorIs(t, err, anError)
}
