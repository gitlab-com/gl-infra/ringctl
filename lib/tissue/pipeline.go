package tissue

import gitlab "gitlab.com/gitlab-org/api/client-go"

type Pipeline struct {
	ID     int
	WebURL string
	Status string
}

func newPipeline(p *gitlab.Pipeline) *Pipeline {
	if p == nil {
		return nil
	}

	return &Pipeline{
		ID:     p.ID,
		Status: p.Status,
		WebURL: p.WebURL,
	}
}
