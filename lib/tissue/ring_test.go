package tissue

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRingCellPath(t *testing.T) {
	amp := DevCluster

	r0 := amp.Ring(0)

	require.Equal(t, "rings/cellsdev/0/foobar.json", r0.CellPath("foobar.json"))
}
