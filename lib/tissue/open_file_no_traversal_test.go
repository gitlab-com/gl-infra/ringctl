package tissue

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_openNoPathTraveral(t *testing.T) {
	// Create a temporary directory for testing
	tempDir := t.TempDir()

	// Create a test file
	testFilePath := filepath.Join(tempDir, "testfile.txt")
	err := os.WriteFile(testFilePath, []byte("test content"), 0600)
	require.NoError(t, err)

	tests := []struct {
		name        string
		baseDir     string
		filename    string
		expectError bool
		errorType   error
	}{
		{
			name:        "Valid file",
			baseDir:     tempDir,
			filename:    "testfile.txt",
			expectError: false,
		},
		{
			name:        "Path traversal attempt",
			baseDir:     tempDir,
			filename:    "../testfile.txt",
			expectError: true,
			errorType:   ErrPathTraversal,
		},
		{
			name:        "Non-existent file",
			baseDir:     tempDir,
			filename:    "nonexistent.txt",
			expectError: true,
		},
		{
			name:        "Empty filename",
			baseDir:     tempDir,
			filename:    "",
			expectError: true,
			errorType:   ErrEmptyFilename,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			file, err := openNoPathTraversal(tt.baseDir, tt.filename)
			if file != nil {
				defer file.Close()
			}

			if tt.expectError {
				require.Error(t, err)

				if tt.errorType != nil {
					require.ErrorIs(t, err, tt.errorType)
				}

				require.Nil(t, file)
			} else {
				require.NoError(t, err)
				require.NotNil(t, file)
			}
		})
	}
}

func TestJoinNoPathTraversal(t *testing.T) {
	baseDir, err := filepath.Abs("/tmp")
	if err != nil {
		t.Fatalf("Failed to get absolute path: %v", err)
	}

	tests := []struct {
		name        string
		baseDir     string
		filename    string
		expected    string
		expectedErr error
	}{
		{
			name:        "Valid path",
			baseDir:     baseDir,
			filename:    "test.txt",
			expected:    filepath.Join(baseDir, "test.txt"),
			expectedErr: nil,
		},
		{
			name:        "Path traversal attempt",
			baseDir:     baseDir,
			filename:    "../../../etc/passwd",
			expected:    "",
			expectedErr: ErrPathTraversal,
		},
		{
			name:        "Empty filename",
			baseDir:     baseDir,
			filename:    "",
			expected:    "",
			expectedErr: ErrEmptyFilename,
		},
		{
			name:        "Filename with current directory",
			baseDir:     baseDir,
			filename:    "./test.txt",
			expected:    filepath.Join(baseDir, "test.txt"),
			expectedErr: nil,
		},
		{
			name:        "Filename with parent directory",
			baseDir:     baseDir,
			filename:    "subdir/../test.txt",
			expected:    filepath.Join(baseDir, "test.txt"),
			expectedErr: nil,
		},
		{
			name:        "Absolute path as filename",
			baseDir:     baseDir,
			filename:    "/etc/passwd",
			expected:    "/tmp/etc/passwd",
			expectedErr: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, err := joinNoPathTraversal(tt.baseDir, tt.filename)

			require.ErrorIs(t, err, tt.expectedErr)
			assert.Equal(t, tt.expected, result)
		})
	}
}
