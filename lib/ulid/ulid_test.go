package ulid

import (
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestMake(t *testing.T) {
	u1, err1 := Make()
	require.NoError(t, err1)

	// ulid is monotonic with a millisecond precision
	<-time.After(1 * time.Millisecond)

	u2, err2 := Make()
	require.NoError(t, err2)

	require.Equal(t, -1,
		strings.Compare(u1, u2))
}
