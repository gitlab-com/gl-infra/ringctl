package ulid

import (
	"crypto/rand"

	oklogUlid "github.com/oklog/ulid/v2"
)

func Make() (string, error) {
	ulid, err := oklogUlid.New(oklogUlid.Now(), rand.Reader)
	if err != nil {
		return "", err //nolint:wrapcheck
	}

	return ulid.String(), nil
}
