module gitlab.com/gitlab-com/gl-infra/ringctl/lib

go 1.23.4 //renovate:managed

require (
	github.com/evanphx/json-patch/v5 v5.9.11
	github.com/hashicorp/go-retryablehttp v0.7.7
	github.com/oklog/ulid/v2 v2.1.0
	github.com/stretchr/testify v1.10.0
	gitlab.com/gitlab-org/api/client-go v0.123.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/Masterminds/semver/v3 v3.3.1
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	golang.org/x/oauth2 v0.25.0 // indirect
	golang.org/x/time v0.9.0 // indirect
)
