package patch

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSort(t *testing.T) {
	patches := []*Metadata{
		{
			ID:       "IDX",
			Ring:     2,
			Priority: 10,
		},
		{
			ID:       "ID0",
			Priority: 4,
			Ring:     0,
		},
		{
			ID:       "ID1",
			Ring:     2,
			Priority: DefaultPriority,
		},
		{
			ID:       "ID2",
			Priority: 0,
			Ring:     0,
		},
	}

	sorted := Sort(patches)

	require.NotEmpty(t, sorted)
	require.Len(t, sorted, 3)

	// ring 0
	require.Equal(t, []*Metadata{patches[3], patches[1]}, sorted[0], "ring 0 not sorted")
	//Priority 0 sanity check
	p0 := sorted[0][0]
	require.Equal(t, "ID2", p0.ID)
	require.Equal(t, 0, p0.Priority)

	// ring 1
	require.Nil(t, sorted[1], "ring 1 should have no patches")

	// ring 2
	require.Equal(t, []*Metadata{patches[2], patches[0]}, sorted[2], "ring 2 not sorted")
}

func TestSortWithPaused(t *testing.T) {
	PauseAfterRing1 := 1
	patches := []*Metadata{
		{
			ID:       "IDX",
			Ring:     2,
			Priority: 10,
		},
		{
			ID:       "ID0",
			Priority: 4,
			Ring:     0,
		},
		{
			ID:             "ID1",
			Ring:           2,
			Priority:       DefaultPriority,
			PauseAfterRing: &PauseAfterRing1,
		},
		{
			ID:       "ID2",
			Priority: 0,
			Ring:     0,
		},
	}

	sorted := Sort(patches)

	require.NotEmpty(t, sorted)
	require.Len(t, sorted, 3)

	// ring 2
	require.Equal(t, []*Metadata{patches[0], patches[2]}, sorted[2], "ring 2 not sorted")
}

func TestSortWithInRollout(t *testing.T) {
	ring := 2
	lowPriority := &Metadata{
		ID:       "low-priority",
		Ring:     ring,
		Priority: 10,
	}
	defaultPriority := &Metadata{
		ID:       "default-priority",
		Ring:     ring,
		Priority: DefaultPriority,
	}
	defaultPriorityPaused := &Metadata{
		ID:             "default-priority-paused",
		Ring:           ring,
		Priority:       DefaultPriority,
		PauseAfterRing: ptr(ring - 1),
	}
	highPriority := &Metadata{
		ID:       "higher-priority",
		Priority: 0,
		Ring:     ring,
	}
	lowPriorityInRollout := &Metadata{
		ID:        "low-priority-in-rollout",
		Ring:      ring,
		Priority:  10,
		InRollout: true,
	}
	patches := []*Metadata{
		lowPriority,
		defaultPriority,
		defaultPriorityPaused,
		highPriority,
		lowPriorityInRollout,
	}

	sorted := Sort(patches)

	require.NotEmpty(t, sorted)
	require.Len(t, sorted, ring+1)

	require.Equalf(t,
		[]*Metadata{
			lowPriorityInRollout,
			highPriority,
			defaultPriority,
			lowPriority,
			defaultPriorityPaused,
		},
		sorted[ring],
		"ring %d not sorted", ring)
}

func TestSortWithFailures(t *testing.T) {
	ring := 2
	lowPriority := &Metadata{
		ID:       "low-priority",
		Ring:     ring,
		Priority: 10,
	}
	defaultPriority := &Metadata{
		ID:       "default-priority",
		Ring:     ring,
		Priority: DefaultPriority,
	}
	defaultPriorityPaused := &Metadata{
		ID:             "default-priority-paused",
		Ring:           ring,
		Priority:       DefaultPriority,
		PauseAfterRing: ptr(ring - 1),
	}
	highPriority := &Metadata{
		ID:       "higher-priority",
		Priority: 0,
		Ring:     ring,
	}
	lowPriorityWithError := &Metadata{
		ID:       "low-priority-with-error",
		Ring:     ring,
		Priority: 10,
		Error:    "some error occurred",
	}
	patches := []*Metadata{
		lowPriority,
		defaultPriority,
		defaultPriorityPaused,
		highPriority,
		lowPriorityWithError,
	}

	sorted := Sort(patches)

	require.NotEmpty(t, sorted)
	require.Len(t, sorted, ring+1)

	require.Equalf(t,
		[]*Metadata{
			lowPriorityWithError,
			highPriority,
			defaultPriority,
			lowPriority,
			defaultPriorityPaused,
		},
		sorted[ring],
		"ring %d not sorted", ring)
}
