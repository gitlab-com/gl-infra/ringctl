package patch

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Operation struct {
	Op    string          `json:"op"`
	Path  string          `json:"path"`
	From  string          `json:"from,omitempty"`
	Value json.RawMessage `json:"value,omitempty"`
}

type Patch []Operation

// String returns a compact, human-readable string representation of the patch.
func (p Patch) String() string {
	var b strings.Builder

	for i, op := range p {
		if i != 0 {
			b.WriteString("\n")
		}

		if op.Op == "remove" {
			b.WriteString(fmt.Sprintf("remove %s", op.Path))
			continue
		}

		b.WriteString(fmt.Sprintf("%s: %s -> %s", op.Op, op.Path, op.Value))
	}

	return b.String()
}
