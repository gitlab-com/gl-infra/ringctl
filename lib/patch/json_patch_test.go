package patch

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPatch_String(t *testing.T) {
	tests := []struct {
		name     string
		patch    Patch
		expected string
	}{
		{
			name:     "empty patch",
			patch:    Patch{},
			expected: "",
		},
		{
			name: "single remove operation",
			patch: Patch{
				{
					Op:   "remove",
					Path: "/path/to/remove",
				},
			},
			expected: "remove /path/to/remove",
		},
		{
			name: "single add operation",
			patch: Patch{
				{
					Op:    "add",
					Path:  "/path/to/add",
					Value: json.RawMessage(`"new value"`),
				},
			},
			expected: `add: /path/to/add -> "new value"`,
		},
		{
			name: "multiple operations",
			patch: Patch{
				{
					Op:    "replace",
					Path:  "/first",
					Value: json.RawMessage(`42`),
				},
				{
					Op:   "remove",
					Path: "/second",
				},
				{
					Op:    "add",
					Path:  "/third",
					Value: json.RawMessage(`{"key": "value"}`),
				},
			},
			expected: `replace: /first -> 42
remove /second
add: /third -> {"key": "value"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := tt.patch.String()
			assert.Equal(t, tt.expected, result, "The string representation should match the expected output")
		})
	}
}
