package patch

import (
	"context"
	"encoding/json"
	"fmt"
)

var ErrUnknownOperation = fmt.Errorf("unknown JSONPatch operation")
var ErrTooFewArguments = fmt.Errorf("too few arguments")
var ErrEmptyPatch = fmt.Errorf("cannot create an empty patch, please provide at least one patch operation")

var JSONPatchOpsParamsCnt = map[string]int{
	"add":     2,
	"replace": 2,
	"remove":  1,
	"test":    2,
	"move":    2,
	"copy":    2,
}

const DefaultPriority = 4

type PatchUploader interface {
	AddPatch(ctx context.Context, meta *Metadata) error
}

type OpParser func(op string, args []string) error
type MetaEditor func(meta *Metadata) error

// Create generated and upload a new JSON Patch to the tissue repository.
//
// The optional edit function is called with the metadata of the patch, it can be used to set custom values.
// If it return an error, the patch is not uploaded.
func Create(ctx context.Context, client PatchUploader, patchID string, args []string, edit MetaEditor) error {
	patch, err := Generate(args)
	if err != nil {
		return fmt.Errorf("failed to generate patch: %w", err)
	}

	buf, err := json.MarshalIndent(patch, "", "  ")
	if err != nil {
		return fmt.Errorf("failed to encode patch: %w", err)
	}

	meta := &Metadata{
		ID:                patchID,
		Priority:          DefaultPriority,
		RelatedTo:         []string{},
		Patch:             buf,
		OnlyGitlabUpgrade: true,
	}

	for _, op := range *patch {
		if op.Path != "/prerelease_version" {
			meta.OnlyGitlabUpgrade = false

			break
		}
	}

	if edit != nil {
		err := edit(meta)
		if err != nil {
			return fmt.Errorf("failed to configure metadata: %w", err)
		}
	}

	return client.AddPatch(ctx, meta) //nolint:wrapcheck
}

func ProcessArgs(args []string, processor OpParser) error {
	if len(args) == 0 {
		return ErrEmptyPatch
	}

	for lastCmdIdx := 0; lastCmdIdx < len(args); {
		cmd := args[lastCmdIdx]

		argsN, ok := JSONPatchOpsParamsCnt[cmd]
		if !ok {
			return fmt.Errorf("unknown op %q: %w", cmd, ErrUnknownOperation)
		}

		if lastCmdIdx+argsN+1 > len(args) {
			return fmt.Errorf("JSON Patch op %q requires %d parameters: %w", cmd, argsN, ErrTooFewArguments)
		}

		if processor != nil {
			err := processor(cmd, args[lastCmdIdx+1:lastCmdIdx+argsN+1])
			if err != nil {
				return fmt.Errorf("cannot process JSON Patch op %q: %w", cmd, err)
			}
		}

		lastCmdIdx += argsN + 1
	}

	return nil
}

func Generate(args []string) (*Patch, error) {
	var patch Patch

	err := ProcessArgs(args, func(op string, params []string) error {
		operation, err := newOperation(op, params)
		if err != nil {
			return err
		}

		patch = append(patch, *operation)

		return nil
	})

	if err != nil {
		return nil, err
	}

	return &patch, nil
}

func newOperation(cmd string, params []string) (*Operation, error) {
	op := Operation{
		Op: cmd,
	}

	switch cmd {
	case "add", "replace", "test":
		op.Path = params[0]

		value := params[1]
		// simplify sending strings as parameters, wrapping them in quotes.
		if !json.Valid([]byte(value)) {
			value = fmt.Sprintf("%q", value)
		}

		op.Value = json.RawMessage(value)
	case "remove":
		op.Path = params[0]
	case "move", "copy":
		op.From = params[0]
		op.Path = params[1]
	default:
		return nil, fmt.Errorf("unknown op %q: %w", cmd, ErrUnknownOperation)
	}

	return &op, nil
}
