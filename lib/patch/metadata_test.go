package patch

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMetadata_Paused(t *testing.T) {
	tests := []struct {
		name       string
		metadata   Metadata
		wantPaused bool
	}{
		{
			name: "Not Paused - Ring matches PauseAfterRing",
			metadata: Metadata{
				Ring:           3,
				PauseAfterRing: ptr(3),
			},
			wantPaused: false,
		},
		{
			name: "Not Paused - Ring lower than PauseAfterRing",
			metadata: Metadata{
				Ring:           2,
				PauseAfterRing: ptr(3),
			},
			wantPaused: false,
		},
		{
			name: "Not Paused - PauseAfterRing is nil",
			metadata: Metadata{
				Ring:           3,
				PauseAfterRing: nil,
			},
			wantPaused: false,
		},
		{
			name: "Paused - Ring bigger than PauseAfterRing",
			metadata: Metadata{
				Ring:           2,
				PauseAfterRing: ptr(1),
			},
			wantPaused: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, tt.wantPaused, tt.metadata.Paused())
		})
	}
}

// Helper function to create a pointer to an int32.
func ptr(i int) *int {
	return &i
}

func TestMetadata_Link(t *testing.T) {
	tests := []struct {
		name     string
		metadata Metadata
		wantLink string
	}{
		{
			name: "RelatedTo with one element",
			metadata: Metadata{
				RelatedTo: []string{"https://example.com/link1"},
			},
			wantLink: "https://example.com/link1",
		},
		{
			name: "RelatedTo with multiple elements",
			metadata: Metadata{
				RelatedTo: []string{"https://example.com/link1", "https://example.com/link2"},
			},
			wantLink: "https://example.com/link1",
		},
		{
			name: "Empty RelatedTo",
			metadata: Metadata{
				RelatedTo: []string{},
			},
			wantLink: "",
		},
		{
			name: "Nil RelatedTo",
			metadata: Metadata{
				RelatedTo: nil,
			},
			wantLink: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			link := tt.metadata.Link()
			require.Equal(t, tt.wantLink, link, "Metadata.Link() returned unexpected result")
		})
	}
}

func TestMetadata_Status(t *testing.T) {
	tests := []struct {
		name     string
		metadata Metadata
		expected Status
	}{
		{
			name: "Failed status",
			metadata: Metadata{
				Error: "Some error occurred",
			},
			expected: StatusFailed,
		},
		{
			name: "Failed status - when paused",
			metadata: Metadata{
				Error:          "Some error occurred",
				Ring:           2,
				PauseAfterRing: ptr(1),
			},
			expected: StatusFailed,
		},
		{
			name: "Failed status - when in rollout",
			metadata: Metadata{
				Error:     "Some error occurred",
				InRollout: true,
			},
			expected: StatusFailed,
		},
		{
			name: "Paused status",
			metadata: Metadata{
				Ring:           3,
				PauseAfterRing: ptr(2),
			},
			expected: StatusPaused,
		},
		{
			name: "In rollout status",
			metadata: Metadata{
				InRollout: true,
			},
			expected: StatusInRollout,
		},
		{
			name:     "Pending status - the default",
			metadata: Metadata{},
			expected: StatusPending,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			status := tt.metadata.Status()
			require.Equal(t, tt.expected, status)
		})
	}
}

func TestMetadata_Updated(t *testing.T) {
	t.Run("LastStatusUpdate is nil", func(t *testing.T) {
		m := &Metadata{
			LastStatusUpdate: nil,
		}

		m.Updated()

		require.NotNil(t, m.LastStatusUpdate, "LastStatusUpdate should be set")
		require.WithinDuration(t, time.Now().UTC(), *m.LastStatusUpdate, time.Second, "LastStatusUpdate should be set to a recent time")
	})

	t.Run("LastStatusUpdate is already set", func(t *testing.T) {
		oldTime := time.Now().Add(-1 * time.Hour).UTC()
		oldTimeRef := oldTime

		m := &Metadata{
			LastStatusUpdate: &oldTime,
		}

		m.Updated()

		require.NotNil(t, m.LastStatusUpdate, "LastStatusUpdate should not be nil")
		require.NotEqual(t, oldTimeRef, *m.LastStatusUpdate, "LastStatusUpdate should be updated")
		require.WithinDuration(t, time.Now().UTC(), *m.LastStatusUpdate, time.Second, "LastStatusUpdate should be set to a recent time")
	})
}

func TestMetadata_Description(t *testing.T) {
	tests := []struct {
		name     string
		patch    json.RawMessage
		expected string
	}{
		{
			name:     "nil patch",
			patch:    nil,
			expected: "Empty patch",
		},
		{
			name:     "invalid patch json",
			patch:    json.RawMessage(`{"invalid json"`),
			expected: "Invalid patch",
		},
		{
			name:     "valid patch",
			patch:    json.RawMessage(`[{"op":"add","path":"/test","value":"test"}]`),
			expected: "add: /test -> \"test\"",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Metadata{
				Patch: tt.patch,
			}

			got := m.Description()
			assert.Equal(t, tt.expected, got, "Description() should return expected value")
		})
	}
}
