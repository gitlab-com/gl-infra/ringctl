package patch

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

var errSentinel = errors.New("a test error")

func TestGenerate(t *testing.T) {
	testCases := []struct {
		name     string
		args     []string
		expected *Patch
		wantErr  bool
	}{
		{
			name: "add operation",
			args: []string{"add", "/path", "value"},
			expected: &Patch{
				{
					Op:    "add",
					Path:  "/path",
					Value: json.RawMessage(`"value"`),
				},
			},
		},
		{
			name: "replace operation",
			args: []string{"replace", "/path", "newValue"},
			expected: &Patch{
				{
					Op:    "replace",
					Path:  "/path",
					Value: json.RawMessage(`"newValue"`),
				},
			},
		},
		{
			name: "remove operation",
			args: []string{"remove", "/path"},
			expected: &Patch{
				{
					Op:   "remove",
					Path: "/path",
				},
			},
		},
		{
			name:    "invalid operation",
			args:    []string{"invalid", "/path"},
			wantErr: true,
		},
		{
			name: "multiple operations",
			args: []string{"move", "/from", "/path", "replace", "/path", "newValue"},
			expected: &Patch{
				{
					Op:   "move",
					From: "/from",
					Path: "/path",
				},
				{
					Op:    "replace",
					Path:  "/path",
					Value: json.RawMessage(`"newValue"`),
				},
			},
		},
		{
			name:    "multiple operations (with invalid op)",
			args:    []string{"move", "/from", "/path", "replace", "/path", "newValue", "not-an-op", "/path"},
			wantErr: true,
		},
		{
			name:    "empty patch",
			args:    []string{},
			wantErr: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, err := Generate(tc.args)

			if tc.wantErr {
				assert.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tc.expected, result)
			}
		})
	}
}

func TestNewOperation(t *testing.T) {
	testCases := []struct {
		name     string
		cmd      string
		params   []string
		expected *Operation
		wantErr  bool
	}{
		{
			name:   "add operation",
			cmd:    "add",
			params: []string{"/path", "value"},
			expected: &Operation{
				Op:    "add",
				Path:  "/path",
				Value: json.RawMessage(`"value"`),
			},
		},
		{
			name:   "replace operation with JSON value",
			cmd:    "replace",
			params: []string{"/path", `{"key": "value"}`},
			expected: &Operation{
				Op:    "replace",
				Path:  "/path",
				Value: json.RawMessage(`{"key": "value"}`),
			},
		},
		{
			name:   "remove operation",
			cmd:    "remove",
			params: []string{"/path"},
			expected: &Operation{
				Op:   "remove",
				Path: "/path",
			},
		},
		{
			name:   "move operation",
			cmd:    "move",
			params: []string{"/from", "/path"},
			expected: &Operation{
				Op:   "move",
				From: "/from",
				Path: "/path",
			},
		},
		{
			name:   "copy operation",
			cmd:    "copy",
			params: []string{"/from", "/path"},
			expected: &Operation{
				Op:   "copy",
				From: "/from",
				Path: "/path",
			},
		},
		{
			name:    "invalid operation",
			cmd:     "invalid",
			params:  []string{"/path"},
			wantErr: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, err := newOperation(tc.cmd, tc.params)

			if tc.wantErr {
				assert.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tc.expected, result)
			}
		})
	}
}

func TestProcessArgs(t *testing.T) {
	tests := []struct {
		name           string
		args           []string
		expectedCalls  int
		expectedError  string
		processorError error
	}{
		{
			name:          "Valid add operation",
			args:          []string{"add", "/path", "value"},
			expectedCalls: 1,
		},
		{
			name:          "Valid remove operation",
			args:          []string{"remove", "/path"},
			expectedCalls: 1,
		},
		{
			name:          "Multiple valid operations",
			args:          []string{"add", "/path1", "value1", "remove", "/path2"},
			expectedCalls: 2,
		},
		{
			name:          "Unknown operation",
			args:          []string{"unknown", "/path", "value"},
			expectedError: "unknown op \"unknown\": unknown JSONPatch operation",
		},
		{
			name:          "Too few arguments",
			args:          []string{"add", "/path"},
			expectedError: "JSON Patch op \"add\" requires 2 parameters: too few arguments",
		},
		{
			name:          "Too many arguments",
			args:          []string{"add", "/path", "value", "extra"},
			expectedCalls: 1,
			expectedError: "unknown op \"extra\": unknown JSONPatch operation",
		},
		{
			name:           "Processor error",
			args:           []string{"remove", "/path"},
			expectedCalls:  1,
			processorError: fmt.Errorf("processor error"), //nolint:goerr113
			expectedError:  "cannot process JSON Patch op \"remove\": processor error",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			calls := 0
			processor := func(cmd string, args []string) error {
				calls++
				return tt.processorError
			}

			err := ProcessArgs(tt.args, processor)

			if tt.expectedError != "" {
				require.EqualError(t, err, tt.expectedError)
			} else {
				require.NoError(t, err)
			}

			require.Equal(t, tt.expectedCalls, calls, "Unexpected number of processor calls")
		})
	}
}

func TestCreate(t *testing.T) {
	tests := []struct {
		name                      string
		patchID                   string
		args                      []string
		expectedPatch             []Operation
		addPatchError             error
		expectedError             string
		expectedOnlyGitlabUpgrade bool
	}{
		{
			name:                      "Successful patch creation - only GitLab upgrade",
			patchID:                   "test-patch-1",
			args:                      []string{"add", "/prerelease_version", "1.2.3"},
			expectedOnlyGitlabUpgrade: true,
			expectedPatch: []Operation{
				{Op: "add", Path: "/prerelease_version", Value: json.RawMessage(`"1.2.3"`)},
			},
		},
		{
			name:                      "Successful patch creation - not only GitLab upgrade",
			patchID:                   "test-patch-2",
			args:                      []string{"add", "/some_path", "value"},
			expectedOnlyGitlabUpgrade: false,
			expectedPatch: []Operation{
				{Op: "add", Path: "/some_path", Value: json.RawMessage(`"value"`)},
			},
		},
		{
			name:                      "Successful patch creation with multiple operations - not only GitLab upgrade",
			patchID:                   "test-patch-1",
			args:                      []string{"replace", "/prerelease_version", "1.2.3", "replace", "/instrumentor_version", "4.5.6"},
			expectedOnlyGitlabUpgrade: false,
			expectedPatch: []Operation{
				{Op: "replace", Path: "/prerelease_version", Value: json.RawMessage(`"1.2.3"`)},
				{Op: "replace", Path: "/instrumentor_version", Value: json.RawMessage(`"4.5.6"`)},
			},
		},
		{
			name:          "Generate patch error",
			patchID:       "test-patch-3",
			args:          []string{"invalid"},
			expectedError: "failed to generate patch: unknown op \"invalid\": unknown JSONPatch operation",
		},
		{
			name:                      "AddPatch error",
			patchID:                   "test-patch-4",
			args:                      []string{"add", "/prerelease_version", "1.2.3"},
			addPatchError:             errors.New("upload error"), //nolint:goerr113
			expectedError:             "upload error",
			expectedOnlyGitlabUpgrade: true,
			expectedPatch: []Operation{
				{Op: "add", Path: "/prerelease_version", Value: json.RawMessage(`"1.2.3"`)},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockUploader := NewMockPatchUploader(t)

			if tt.expectedPatch != nil {
				mockUploader.EXPECT().AddPatch(context.Background(), mock.Anything).
					RunAndReturn(func(_ context.Context, meta *Metadata) error {
						assert.Equal(t, tt.patchID, meta.ID, "ID does not match")
						assert.Equal(t, tt.expectedOnlyGitlabUpgrade, meta.OnlyGitlabUpgrade, "OnlyGitlabUpgrade does not match")
						assert.Equal(t, DefaultPriority, meta.Priority, "Priority does not match")
						assert.Empty(t, meta.RelatedTo)

						var patch []Operation
						err := json.Unmarshal(meta.Patch, &patch)
						require.NoError(t, err)
						assert.Equal(t, tt.expectedPatch, patch)

						return tt.addPatchError
					}).
					Once()
			}

			err := Create(context.Background(), mockUploader, tt.patchID, tt.args, nil)

			if tt.expectedError != "" {
				assert.EqualError(t, err, tt.expectedError)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestGenerateEdit(t *testing.T) {
	t.Run("with error", func(t *testing.T) {
		mockUploader := NewMockPatchUploader(t)

		err := Create(context.Background(), mockUploader, "test-patch", []string{"add", "/field", "value"}, func(_ *Metadata) error {
			return errSentinel
		})

		assert.ErrorIs(t, err, errSentinel)
	})

	t.Run("successful edit", func(t *testing.T) {
		mockUploader := NewMockPatchUploader(t)
		callbackRun := false

		err := Create(context.Background(), mockUploader, "test-patch", []string{"add", "/field", "value"}, func(m *Metadata) error {
			mockUploader.EXPECT().AddPatch(context.Background(), m).Return(nil)

			callbackRun = true

			return nil
		})

		assert.True(t, callbackRun, "callback not invoked")
		assert.NoError(t, err)
	})
}
