package patch

import (
	"slices"
	"strings"
)

type PatchesByRing [][]*Metadata

// metadataCompare(a, b) should return a negative number when a < b, a positive number when
// a > b and zero when a == b.
// We sort by:
// - Ring
// - Status
// - Priority
// - ID - IDs are lexicografically sorted at generation time
//
// See sort_test.go for some examples.
func metadataCompare(a, b *Metadata) int {
	if a.Ring != b.Ring {
		return a.Ring - b.Ring
	}

	if byStatus := statusCompare(a.Status(), b.Status()); byStatus != 0 {
		return byStatus
	}

	if a.Priority != b.Priority {
		return a.Priority - b.Priority
	}

	return strings.Compare(a.ID, b.ID)
}

// statusCompare(a, b) compare status using the Statuses list as priority.
func statusCompare(a, b Status) int {
	if a == b {
		return 0
	}

	aVal := slices.Index(Statuses, a)
	bVal := slices.Index(Statuses, b)

	if aVal < bVal {
		return -1
	}

	return 1
}

func Sort(patches []*Metadata) PatchesByRing {
	if len(patches) == 0 {
		return nil
	}

	patches = slices.Clone(patches)
	slices.SortFunc(patches, metadataCompare)
	maxRing := patches[len(patches)-1].Ring

	sorted := make(PatchesByRing, maxRing+1)

	for _, patch := range patches {
		ring := patch.Ring
		sorted[ring] = append(sorted[ring], patch)
	}

	return sorted
}
