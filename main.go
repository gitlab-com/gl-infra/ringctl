/*
Copyright © 2024-present GitLab B.V.
This file is part of ringctl CLI
*/
package main

import "gitlab.com/gitlab-com/gl-infra/ringctl/cmd"

func main() {
	cmd.Execute()
}
