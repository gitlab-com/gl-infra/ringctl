# ringctl

Ring deployment control-tool for [🧫 Cells Tissue](https://gitlab.com/gitlab-com/gl-infra/cells/tissue).

## Hacking on ringctl

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
1. Run `pmv capture gitlab --description "My Tissue OPS Access Token" --tags "Cells/Tissue/OPS" --gitlab-instance "https://ops.gitlab.net"` and generate a token for OPS with the `api` scope
1. Load your variables with `eval $(pmv env Cells/Tissue/OPS)`
1. Test ringctl is working with `go run ./ patch ls`

#### Fonts

Some of `ringctl`'s subcommands print tables in their output which contain icons. `ringctl` supports
[nerd fonts](https://www.nerdfonts.com/),
[emoji](https://www.unicode.org/emoji/charts/full-emoji-list.html), and text-based icons. Text-based
icons are the default, but users are encouraged to set up their preference using the `ringctl.yml`
file and ensure that the preferred icon set can be displayed correctly in their terminal.

The user's preference can be applied across all commands by updating the `ringctl.yml` configuration
file:

``` yaml
icons: nerd_fonts
```

Note that when `ringctl` is used in CI, emoji icons will be used, irrespective of user preference.

> **NOTE:** Please be aware that the table on your terminal may not be displayed correctly when
> using Nerd Fonts or emoji.

### Adding new commands

New commands are scaffolded using `./scripts/cobra-cli`, to build the scaffold run `./scripts/cobra-cli add newCommand`, for nested commands append `-p parentCommand`.

### Debugging with VSCode

It is possible to debug `ringctl` with delve, with VSCode is quite simple. In a shell with the credential loaded with `pmv` run the following commands:

1. compile with `go build`
1. run `./ringctl` with the relevant arguments plus `--wait-for-debugger`, the application will output its PID and pause until you sent `ctrl+c`
1. in VSCode click "Run->Start Debugging (F5)" and paste the PID in the dropdown
1. happy debugging
