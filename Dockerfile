FROM alpine:3.21

# Install SSL ca certificates.
RUN apk add --update --no-cache ca-certificates && update-ca-certificates

# Copy our static executable
COPY ringctl /usr/local/bin/ringctl
