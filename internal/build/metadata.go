package build

import (
	"fmt"
	"runtime/debug"
	"time"
)

var (
	// injected at build time during a release.
	tag = "dev"

	// detected from BuildInfo (not available on go run).
	commit   = "unknow"
	date     = ""
	modified = true
)

func init() {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			switch setting.Key {
			case "vcs.modified":
				modified = setting.Value == "true"
			case "vcs.time":
				date = setting.Value
			case "vcs.revision":
				commit = setting.Value
			}
		}
	}

	if modified || date == "" {
		date = time.Now().UTC().Format(time.RFC3339)
	}
}

type VersionInfo struct {
	Tag      string
	Commit   string
	Date     string
	Modified bool
}

// Version returns the version of the build.
func Version() *VersionInfo {
	return &VersionInfo{
		Tag:      tag,
		Commit:   commit,
		Date:     date,
		Modified: modified,
	}
}

func (v *VersionInfo) String() string {
	return fmt.Sprintf("%s%s commit=%s build_date=%s", v.Tag, v.modifiedString(), v.ShortCommit(), v.Date)
}

func (v *VersionInfo) ShortCommit() string {
	if len(v.Commit) > 7 {
		return v.Commit[:7]
	}

	return v.Commit
}

func (v *VersionInfo) Short() string {
	return fmt.Sprintf("%s-g%s%s", v.Tag, v.ShortCommit(), v.modifiedString())
}

func (v *VersionInfo) modifiedString() string {
	if v.Modified {
		return "-dirty"
	}

	return ""
}
