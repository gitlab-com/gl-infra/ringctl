package patch

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/test"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

func TestProgressCurrentNoPatch(t *testing.T) {
	client, _, _ := test.MockTissueClient(t, nil)
	err := ProgressCurrent(context.Background(), test.GetFixturesCluster(), client, "", 1)
	require.NoError(t, err)
}

func TestProgressCurrent(t *testing.T) {
	amp := tissue.AmpCluster("test-progress")
	currentRing := 1
	metaPatch := &patch.Metadata{
		ID:        "a-patch-id",
		InRollout: true,
		Ring:      currentRing,
	}
	patchID := metaPatch.ID

	client, storage, _ := test.MockTissueClient(t, func(c *tissue.Config) { c.AmpCluster = amp })

	storage.EXPECT().GetPatch(mock.Anything, amp, patchID).Return(metaPatch, nil).Once()
	storage.EXPECT().UpdatePatchStatus(mock.Anything, amp, metaPatch).Return(nil).Once()

	err := ProgressCurrent(context.Background(), amp, client, patchID, currentRing+1)
	require.NoError(t, err)

	require.False(t, metaPatch.InRollout, "in rollout status not reset")
	require.Equal(t, currentRing+1, metaPatch.Ring, "ring not increased")
	require.Empty(t, metaPatch.Error)
}

func TestProgressCurrentLastRing(t *testing.T) {
	amp := tissue.AmpCluster("test-progress")
	currentRing := 1
	metaPatch := &patch.Metadata{
		ID:        "a-patch-id",
		InRollout: true,
		Ring:      currentRing,
	}
	patchID := metaPatch.ID

	client, storage, _ := test.MockTissueClient(t, func(c *tissue.Config) { c.AmpCluster = amp })

	storage.EXPECT().GetPatch(mock.Anything, amp, patchID).Return(metaPatch, nil).Once()
	storage.EXPECT().DeletePatch(mock.Anything, amp, patchID).Return(nil).Once()

	err := ProgressCurrent(context.Background(), amp, client, patchID, currentRing)
	require.NoError(t, err)
}

func TestProgressCurrentCompleteAfterRing(t *testing.T) {
	amp := tissue.AmpCluster("test-progress")
	currentRing := 1
	metaPatch := &patch.Metadata{
		ID:              "a-patch-id",
		InRollout:       true,
		Ring:            currentRing,
		DeleteAfterRing: &currentRing,
	}
	patchID := metaPatch.ID

	client, storage, _ := test.MockTissueClient(t, func(c *tissue.Config) { c.AmpCluster = amp })

	storage.EXPECT().GetPatch(mock.Anything, amp, patchID).Return(metaPatch, nil).Once()
	storage.EXPECT().DeletePatch(mock.Anything, amp, patchID).Return(nil).Once()

	err := ProgressCurrent(context.Background(), amp, client, patchID, currentRing+5)
	require.NoError(t, err)
}

func TestProgressCurrentWithAFailedPatch(t *testing.T) {
	amp := tissue.AmpCluster("test-progress")
	currentRing := 1
	metaPatch := &patch.Metadata{
		ID:        "a-patch-id",
		InRollout: true,
		Ring:      currentRing,
		Error:     "an error",
	}
	patchID := metaPatch.ID

	incidentManager := tissue.NewMockIncidentEvents(t)
	client, storage, _ := test.MockTissueClient(t, func(c *tissue.Config) {
		c.AmpCluster = amp
		c.AlertManager = incidentManager
	})

	storage.EXPECT().GetPatch(mock.Anything, amp, patchID).Return(metaPatch, nil).Once()
	storage.EXPECT().UpdatePatchStatus(mock.Anything, amp, metaPatch).Return(nil).Once()
	incidentManager.EXPECT().ResolveAlert(mock.Anything, metaPatch, tissue.GetCIJobURL(context.Background())).Return(nil).Once()

	err := ProgressCurrent(context.Background(), amp, client, patchID, currentRing+1)
	require.NoError(t, err)

	require.False(t, metaPatch.InRollout, "in rollout status not reset")
	require.Equal(t, currentRing+1, metaPatch.Ring, "ring not increased")
	require.Empty(t, metaPatch.Error, "error not reset")
}
