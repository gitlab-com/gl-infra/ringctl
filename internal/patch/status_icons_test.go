package patch

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

func TestStatusNotFound(t *testing.T) {
	m := iconsMap{patch.StatusFailed: "failed"}

	assert.Equal(t, "failed", m.Status2Icon(patch.StatusFailed))
	assert.Equal(t, "", m.Status2Icon(patch.StatusInRollout))
}
