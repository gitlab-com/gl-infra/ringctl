package patch

import (
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/mergestat/timediff"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

func PrintCLITable(patches patch.PatchesByRing, opts ...OutputOptionsFunc) {
	conf := buildOpts(opts)
	t := makeTable(conf.out)
	t.AppendHeader(table.Row{"Ring", "#", "Info", "Since", "Description"})
	t.SetColumnConfigs([]table.ColumnConfig{
		{
			Name:        "Info",
			Align:       text.AlignLeft,
			Transformer: patchSummary(conf.icons),
		},
		{
			Name:  "Since",
			Align: text.AlignRight,
			Transformer: func(value interface{}) string {
				since, isTime := value.(*time.Time)
				if !isTime || since == nil {
					return ""
				}

				return timediff.TimeDiff(*since)
			},
		},
	})

	for _, ringPatches := range patches {
		for _, patch := range ringPatches {
			t.AppendRow(table.Row{
				patch.Ring,
				patch.ID,
				patch,
				patch.LastStatusUpdate,
				patch.Description(),
			})
		}

		t.AppendSeparator()
	}

	t.Render()

	legend := makeTable(conf.out)
	legend.AppendHeader(table.Row{"Status", "Description"})
	legend.SetColumnConfigs(
		[]table.ColumnConfig{
			{
				Name:  "Status",
				Align: text.AlignCenter,
			},
		})

	for _, status := range patch.Statuses {
		legend.AppendRow(table.Row{conf.icons.Status2Icon(status), status})
	}

	legend.AppendRow(table.Row{conf.icons.Status2Icon(DeleteAfterRingIconStatus), "complete after ring"})

	legend.Render()

	_, _ = conf.out.Write([]byte("\nDeployment pipelines: https://ops.gitlab.net/gitlab-com/gl-infra/cells/tissue/-/pipelines?scope=all&source=api\n"))
}

func PrintSinglePatch(meta *patch.Metadata, opts ...OutputOptionsFunc) {
	conf := buildOpts(opts)
	t := makeTable(conf.out)
	t.SetTitle(fmt.Sprintf("Patch %s", meta.ID))

	t.AppendRow(table.Row{"ID", meta.ID})
	t.AppendRow(table.Row{"Priority", meta.Priority})
	t.AppendRow(table.Row{"Only Upgrade", meta.OnlyGitlabUpgrade})
	t.AppendRow(table.Row{"Rollback", meta.Rollback})
	t.AppendRow(table.Row{"Related to", strings.Join(meta.RelatedTo, "\n")})

	if meta.PauseAfterRing != nil {
		t.AppendRow(table.Row{"Pause after Ring", *meta.PauseAfterRing})
	}

	if meta.DeleteAfterRing != nil {
		t.AppendRow(table.Row{"Delete after Ring", *meta.DeleteAfterRing})
	}

	t.AppendSeparator()
	t.AppendRow(table.Row{"Patch", string(meta.Patch)})

	t.AppendSeparator()
	t.AppendRow(table.Row{"Ring", meta.Ring})
	t.AppendRow(table.Row{"Status", meta.Status()})

	if meta.LastStatusUpdate != nil {
		t.AppendRow(table.Row{
			"Since",
			timediff.TimeDiff(*meta.LastStatusUpdate),
		})
	}

	t.AppendRow(table.Row{"Error", meta.Error})

	t.Render()
}

func makeTable(w io.Writer) table.Writer { //nolint:ireturn
	t := table.NewWriter()
	t.SetStyle(table.StyleRounded)
	t.SetOutputMirror(w)

	return t
}
