package patch

import (
	"context"
	"fmt"
	"log/slog"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

func ProgressCurrent(ctx context.Context, amp tissue.Cluster, client *tissue.Client, patchID string, lastRing int) error {
	if patchID == "" {
		return nil
	}

	meta, err := client.GetPatch(ctx, patchID)
	if err != nil {
		return fmt.Errorf("cannot fetch current patch: %w", err)
	}

	logger := slog.With(slog.String("patch_id", meta.ID), slog.Int("ring", meta.Ring))
	logger.LogAttrs(ctx, slog.LevelInfo, "Patch deployment completed")

	if meta.Status() != patch.StatusInRollout {
		logger.LogAttrs(ctx,
			slog.LevelError,
			"unexpected patch status, not In Rollout",
			slog.Any("current_status", meta.Status()),
		)
	}

	resolveAlert := meta.Error != ""
	meta.InRollout = false
	meta.Ring += 1
	meta.Error = ""
	meta.Updated()

	if meta.Ring > lastRing || (meta.DeleteAfterRing != nil && meta.Ring > *meta.DeleteAfterRing) {
		logger.LogAttrs(ctx, slog.LevelInfo, "Deployment complete")

		if err := client.DeletePatch(ctx, meta.ID); err != nil {
			return fmt.Errorf("cannot delete patch %s: %w", meta.ID, err)
		}

		return nil
	}

	if err := client.UpdatePatchStatus(ctx, meta); err != nil {
		return fmt.Errorf("cannot update current patch status: %w", err)
	}

	logger.LogAttrs(ctx, slog.LevelInfo, "Patch metadata upgraded")

	if resolveAlert {
		alertError := client.ResolveAlert(ctx, meta, tissue.GetCIJobURL(ctx))
		if alertError != nil {
			slog.LogAttrs(ctx, slog.LevelError, "Resolving the alert failed", slog.Any("error", alertError))
		}
	}

	return nil
}
