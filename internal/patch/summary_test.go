package patch

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

func TestPatchSummary(t *testing.T) {
	tests := []struct {
		name     string
		input    interface{}
		expected string
	}{
		{
			name:     "Nil input",
			input:    nil,
			expected: "",
		},
		{
			name:     "Non-metadata input",
			input:    "not a metadata",
			expected: "",
		},
		{
			name: "Metadata with Failed status",
			input: &patch.Metadata{
				Priority: 1,
				Error:    "test error",
			},
			expected: "🚨 P1",
		},
		{
			name: "Metadata with Pending status",
			input: &patch.Metadata{
				Priority:  2,
				InRollout: false,
			},
			expected: "⏳ P2",
		},
		{
			name: "Metadata with Pending status and pause after ring",
			input: &patch.Metadata{
				Priority:       3,
				InRollout:      false,
				PauseAfterRing: intPtr(4),
			},
			expected: "⏳ P3 ⏸️ 4",
		},
		{
			name: "Metadata with Paused status and pause after ring",
			input: &patch.Metadata{
				Priority:       3,
				InRollout:      false,
				Ring:           5,
				PauseAfterRing: intPtr(4),
			},
			expected: "⏸️ P3",
		},
		{
			name: "Metadata with InRollout status",
			input: &patch.Metadata{
				Priority:  4,
				InRollout: true,
			},
			expected: "🐿️ P4",
		},
		{
			name: "Metadata with Pending status and delete after ring",
			input: &patch.Metadata{
				Priority:        3,
				InRollout:       false,
				DeleteAfterRing: intPtr(4),
			},
			expected: "⏳ P3 🏁 4",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			transformer := patchSummary(EmojiStatusIcons)
			result := transformer(tt.input)

			assert.Equal(t, tt.expected, result)
		})
	}
}

// Helper function to create a pointer to an int.
func intPtr(i int) *int {
	return &i
}
