package patch

import "io"

type outputOpts struct {
	out   io.Writer
	icons StatusConverter
}

type OutputOptionsFunc func(*outputOpts)

const (
	OptionIcons = "icons"

	// OutputIcons is an enum which lists the icon types which are supported by various commands.
	OutputIconsNerdFonts    = "nerd_fonts"
	OutputIconsEmoji        = "emoji"
	OutputIconsTextStatuses = "text"
)

func WithEmoji() OutputOptionsFunc {
	return func(o *outputOpts) {
		o.icons = EmojiStatusIcons
	}
}

func WithNerdFonts() OutputOptionsFunc {
	return func(o *outputOpts) {
		o.icons = NerdFontsStatusIcons
	}
}

func WithTextStatuses() OutputOptionsFunc {
	return func(o *outputOpts) {
		o.icons = TextStatuses
	}
}

func WithWriteTo(w io.Writer) OutputOptionsFunc {
	return func(o *outputOpts) {
		o.out = w
	}
}

func buildOpts(funcs []OutputOptionsFunc) outputOpts {
	opts := outputOpts{
		icons: DefaultStatusIcons,
		out:   io.Discard,
	}

	for _, f := range funcs {
		f(&opts)
	}

	return opts
}
