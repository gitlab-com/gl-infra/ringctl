package patch

import (
	"fmt"

	"github.com/jedib0t/go-pretty/v6/text"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

// patchSummmary returns a text.Transformer that formats a patch.Metadata into
// a text string with a patch summary.
//
// The summary iscomposed of:
// - The status icon,
// - The patch priority, prefixed with a "P",
// - The pause after ring, prefixed with the paused icon, if it's set,
// - The delete after ring, prefixed with a checkmark, if it's set.
func patchSummary(icons StatusConverter) text.Transformer {
	return func(value interface{}) string {
		meta, valueIsMeta := value.(*patch.Metadata)
		if !valueIsMeta || meta == nil {
			return ""
		}

		icon := icons.Status2Icon(meta.Status())

		pauseAfter := ""
		if meta.PauseAfterRing != nil && meta.Status() != patch.StatusPaused {
			pauseAfter = fmt.Sprintf(" %s %d", icons.Status2Icon(patch.StatusPaused), *meta.PauseAfterRing)
		}

		deleteAfter := ""
		if meta.DeleteAfterRing != nil {
			deleteAfter = fmt.Sprintf(" %s %d", icons.Status2Icon(DeleteAfterRingIconStatus), *meta.DeleteAfterRing)
		}

		return fmt.Sprintf("%s P%d%s%s", icon, meta.Priority, pauseAfter, deleteAfter)
	}
}
