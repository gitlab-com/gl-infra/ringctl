package patch

import (
	"context"
	"errors"
	"fmt"
	"log/slog"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

var ErrCannotMarkAsDeployed = errors.New("cannot mark patch as deployed")

func ApplyNext(ctx context.Context, client *tissue.Client, lastRing int, patches patch.PatchesByRing, amp tissue.Cluster) error {
	for ringNr := 0; ringNr <= lastRing; ringNr++ {
		var patchSet []*patch.Metadata
		if len(patches) > ringNr {
			patchSet = patches[ringNr]
		}

		slog.LogAttrs(ctx, slog.LevelDebug, "Scanning patches",
			slog.Any("ring", ringNr), slog.Any("patches", len(patchSet)))

		if len(patchSet) == 0 {
			slog.LogAttrs(ctx, slog.LevelInfo, "No patches to apply", slog.Int("ring", ringNr))
			continue
		}

		firstPatch := patchSet[0]

		if firstPatch.Status() != patch.StatusPending {
			slog.LogAttrs(ctx, slog.LevelInfo, "patch cannot be applied",
				slog.Any("patch_status", firstPatch.Status()), slog.String("patch_error", firstPatch.Error), slog.Int("ring", firstPatch.Ring))

			continue
		}

		slog.LogAttrs(ctx, slog.LevelInfo, "Applying patch",
			slog.Int("ring", ringNr), slog.String("patch", firstPatch.ID))

		firstPatch.InRollout = true
		firstPatch.Updated()

		if errPatch := client.Patch(ctx, firstPatch); errPatch != nil {
			patchError := fmt.Errorf("failed to apply patch %s on ring %d. %w", firstPatch.ID, ringNr, errPatch)
			firstPatch.Error = patchError.Error()

			_ = client.UpdatePatchStatus(ctx, firstPatch)

			currentJobURL := tissue.GetCIJobURL(ctx)

			slog.LogAttrs(ctx, slog.LevelError, "patch failure",
				slog.String("job_url", currentJobURL),
				slog.Any("error", patchError), slog.Int("ring", ringNr))

			alertError := client.FireAlert(ctx, firstPatch, currentJobURL)
			if alertError != nil {
				slog.LogAttrs(ctx, slog.LevelError, "incident declaration failed", slog.Any("error", alertError))
			}

			continue
		}

		if client.Local() {
			slog.LogAttrs(ctx, slog.LevelDebug, "skip deployment trigger on local execution")

			return nil
		}

		if err := triggerDeployment(ctx, amp, firstPatch, client); err != nil {
			return fmt.Errorf("deployment trigger failed: %w", err)
		}
	}

	return nil
}

func triggerDeployment(ctx context.Context, amp tissue.Cluster, meta *patch.Metadata, client *tissue.Client) error {
	ringNr := meta.Ring

	slog.LogAttrs(ctx, slog.LevelInfo, "triggering ring deployment generation", slog.Int("ring", ringNr))

	pipeline, err := client.Deployment(ctx, amp.Ring(ringNr),
		tissue.WithOnlyGitlabUpgrade(meta.OnlyGitlabUpgrade),
		tissue.WithPatchID(meta.ID),
		tissue.IsRollback(meta.Rollback))
	if err != nil {
		slog.LogAttrs(ctx, slog.LevelError, "failed to trigger ring deployment",
			slog.Any("error", err),
			slog.Int("ring", ringNr),
			slog.String("patch_id", meta.ID),
			slog.Bool("rollback", meta.Rollback))

		return err //nolint:wrapcheck
	}

	if pipeline != nil {
		slog.LogAttrs(ctx, slog.LevelInfo, "🐿️ Ring deployment running ",
			slog.Int("ring", ringNr),
			slog.String("patch_id", meta.ID),
			slog.String("pipeline", pipeline.WebURL))
	}

	return nil
}
