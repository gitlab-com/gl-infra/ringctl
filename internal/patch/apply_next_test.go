package patch

import (
	"context"
	"encoding/json"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	gl "gitlab.com/gitlab-org/api/client-go"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/test"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/tissue"
)

var aPatch = json.RawMessage(`[]`)

func ampAndThreeRingsPatches() (tissue.Cluster, []*patch.Metadata) { //nolint:ireturn
	amp := test.GetFixturesCluster()
	patches := []*patch.Metadata{
		{
			ID:       "ID0",
			Ring:     0,
			Priority: patch.DefaultPriority,
			Patch:    aPatch,
		},
		{
			ID:       "ID1",
			Ring:     1,
			Priority: patch.DefaultPriority,
			Patch:    aPatch,
		},
		{
			ID:       "ID2",
			Ring:     2,
			Priority: patch.DefaultPriority,
			Patch:    aPatch,
		},
	}

	return amp, patches
}

func TestApplyNextTriggersAllRings(t *testing.T) {
	amp, patches := ampAndThreeRingsPatches()
	testApplyNext(t, amp, 3, patches, patches)
}

func TestApplyNextTriggersAllRingsWithStatus(t *testing.T) {
	t.Run("in rollout", func(t *testing.T) {
		amp, patches := ampAndThreeRingsPatches()
		toTrigger := []*patch.Metadata{patches[0], patches[2]}

		ring1Patch := patches[1]
		require.Equal(t, patch.StatusPending, ring1Patch.Status())
		ring1Patch.InRollout = true

		testApplyNext(t, amp, 3, patches, toTrigger)
	})

	t.Run("error", func(t *testing.T) {
		amp, patches := ampAndThreeRingsPatches()
		toTrigger := []*patch.Metadata{patches[0], patches[2]}

		ring1Patch := patches[1]
		require.Equal(t, patch.StatusPending, ring1Patch.Status())
		ring1Patch.Error = "an error"

		testApplyNext(t, amp, 3, patches, toTrigger)
	})

	t.Run("paused", func(t *testing.T) {
		amp, patches := ampAndThreeRingsPatches()
		toTrigger := []*patch.Metadata{patches[0], patches[2]}

		ring1Patch := patches[1]
		require.Equal(t, patch.StatusPending, ring1Patch.Status())
		ring0 := ring1Patch.Ring - 1
		ring1Patch.PauseAfterRing = &ring0
		require.Equal(t, patch.StatusPaused, ring1Patch.Status())

		testApplyNext(t, amp, 3, patches, toTrigger)
	})
}

func TestApplyNextTriggersAllRingsWithHoles(t *testing.T) {
	amp := test.GetFixturesCluster()
	patches := []*patch.Metadata{
		{
			ID:       "ID0",
			Ring:     0,
			Priority: patch.DefaultPriority,
			Patch:    aPatch,
		},
		{
			ID:       "ID2",
			Ring:     2,
			Priority: patch.DefaultPriority,
			Patch:    aPatch,
		},
	}

	testApplyNext(t, amp, 3, patches, patches)
}

func testApplyNext(t *testing.T, amp tissue.Cluster, rings int, patches []*patch.Metadata, toTrigger []*patch.Metadata) {
	t.Helper()

	branch := "a-branch"
	sorted := patch.Sort(patches)
	require.Len(t, sorted, rings)

	client, storage, gitlabClient := test.MockTissueClient(t, func(c *tissue.Config) {
		c.AmpCluster = amp
		c.Branch = branch
	})

	for _, patch := range toTrigger {
		storage.EXPECT().UpdateWithPatch(mock.Anything, amp, patch, mock.Anything, mock.Anything).Return(nil).Once()
	}

	triggeredPatch := make([]bool, len(toTrigger))

	gitlabClient.EXPECT().CreatePipeline(mock.Anything, mock.Anything, mock.Anything).
		Run(func(pid interface{}, opt *gl.CreatePipelineOptions, options ...gl.RequestOptionFunc) {
			require.NotNil(t, opt)
			require.NotNil(t, opt.Ref)
			require.Equal(t, branch, *opt.Ref)
			require.NotNil(t, opt.Variables)

			ring := ""
			patchID := ""

			for _, variable := range *opt.Variables {
				require.NotNil(t, variable)

				switch *(variable.Key) {
				case "RING":
					ring = *(variable.Value)
					t.Log("RING", ring)

				case tissue.CIVariablePatchID:
					patchID = *(variable.Value)
					t.Log("PATCH_ID", patchID)
				}
			}

			require.NotEmpty(t, ring, "missing ring variable")
			require.NotEmpty(t, patchID, "missing patch id variable")

			ringNr, errParse := strconv.ParseInt(ring, 10, 0)
			require.NoError(t, errParse)

			matchingPatch := sorted[ringNr][0]
			require.Equal(t, matchingPatch.ID, patchID)

			for idx, patch := range toTrigger {
				if patch.ID == patchID {
					triggeredPatch[idx] = true
					break
				}
			}
		}).Return(nil, nil, nil)

	localStorage := tissue.LocalDiskStorage{}
	storage.EXPECT().ListCells(mock.Anything, mock.Anything).RunAndReturn(localStorage.ListCells).Times(len(toTrigger))

	err := ApplyNext(context.Background(), client, rings, sorted, amp)
	require.NoError(t, err)

	for idx, triggered := range triggeredPatch {
		assert.True(t, triggered, "patch for ring %d not triggered", toTrigger[idx].Ring)
	}
}

func TestApplyNextCausingErrorsAndFiringAlerts(t *testing.T) {
	amp := test.GetFixturesCluster()
	patches := []*patch.Metadata{
		{
			ID:       "ID0",
			Ring:     0,
			Priority: patch.DefaultPriority,
			Patch:    json.RawMessage(`[{"op":"test","path":"/not-existing","value":"something"}]`),
		},
		{
			ID:       "ID2",
			Ring:     0,
			Priority: patch.DefaultPriority + 1,
			Patch:    aPatch,
		},
	}

	sorted := patch.Sort(patches)
	require.Len(t, sorted, 1)

	alertManager := tissue.NewMockIncidentEvents(t)
	client, storage, _ := test.MockTissueClient(t, func(c *tissue.Config) {
		c.AmpCluster = amp
		c.AlertManager = alertManager
	})

	alertManager.EXPECT().FireAlert(mock.Anything, patches[0], mock.Anything).Once().Return(nil)
	storage.EXPECT().UpdatePatchStatus(mock.Anything, mock.Anything, mock.Anything).Once().Run(func(args mock.Arguments) {
		patch, ok := args[2].(*patch.Metadata)
		require.True(t, ok)

		require.NotEmpty(t, patch.Error, "the patch error must be set")
		require.Equal(t, patch.ID, patches[0].ID)
	}).Return(nil)

	localStorage := tissue.LocalDiskStorage{}
	storage.EXPECT().ListCells(mock.Anything, amp.Ring(0)).RunAndReturn(localStorage.ListCells).Once()

	err := ApplyNext(context.Background(), client, 0, sorted, amp)
	require.NoError(t, err)
}
