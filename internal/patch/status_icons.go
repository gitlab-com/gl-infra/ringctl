package patch

import (
	"strconv"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

type StatusConverter interface {
	Status2Icon(status patch.Status) string
}

type iconsMap map[patch.Status]string

func (i iconsMap) Status2Icon(status patch.Status) string {
	return i[status]
}

var (
	// DeleteAfterRingIconStatus is an internal status that we use to generate an icon for a proporty of the patch metadata.
	DeleteAfterRingIconStatus = patch.Status("internal_delete_after_ring")

	EmojiStatusIcons = iconsMap{
		patch.StatusFailed:        "🚨",
		patch.StatusPending:       "⏳",
		patch.StatusPaused:        "⏸️",
		patch.StatusInRollout:     "🐿️",
		DeleteAfterRingIconStatus: "🏁",
	}

	NerdFontsStatusIcons = iconsMap{ // https://www.nerdfonts.com/cheat-sheet
		patch.StatusFailed:        convertNerdFontUTF16OrPanic("f0bea"), // nf-md-alarm_light_outline 󰯪
		patch.StatusPending:       convertNerdFontUTF16OrPanic("f250"),  // nf-fa-hourglass_o 
		patch.StatusPaused:        convertNerdFontUTF16OrPanic("f28c"),  // nf-fa-pause_circle_o 
		patch.StatusInRollout:     convertNerdFontUTF16OrPanic("eb58"),  // nf-cod-squirrel 
		DeleteAfterRingIconStatus: convertNerdFontUTF16OrPanic("f11e"),  // nf-fa-flag_checkered 
	}

	TextStatuses = iconsMap{
		patch.StatusFailed:        "<F>",
		patch.StatusPending:       "<P>",
		patch.StatusPaused:        "<II>",
		patch.StatusInRollout:     "<R>",
		DeleteAfterRingIconStatus: "<D>",
	}

	DefaultStatusIcons = TextStatuses
)

func convertNerdFontUTF16OrPanic(hex string) string {
	n, err := strconv.ParseInt(hex, 16, 32)
	if err != nil {
		panic(err)
	}

	return string(rune(n))
}
