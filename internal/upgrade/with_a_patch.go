package upgrade

import (
	"context"
	"fmt"
	"log/slog"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/ulid"
)

const (
	PerimeterRing    = 1
	OriginAutoDeploy = "auto_deploy"
)

func WithAPatch(ctx context.Context, newVersion *gitlab.Version, relatedTo []string, client patch.PatchUploader) error {
	slog.LogAttrs(ctx, slog.LevelInfo, "upgrading using a patch", slog.Any("new_version", newVersion))

	patchArgs := []string{"replace", "/prerelease_version", newVersion.String()}

	id, err := ulid.Make()
	if err != nil {
		return fmt.Errorf("cannot create ULID: %w", err)
	}

	return patch.Create(ctx, client, id, patchArgs, func(meta *patch.Metadata) error { //nolint:wrapcheck
		meta.PauseAfterRing = new(int)
		*meta.PauseAfterRing = PerimeterRing

		meta.RelatedTo = append(meta.RelatedTo, relatedTo...)
		meta.Origin = OriginAutoDeploy

		return nil
	})
}
