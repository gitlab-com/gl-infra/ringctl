package upgrade

import (
	"context"
	"testing"

	"github.com/oklog/ulid/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/gitlab"
	"gitlab.com/gitlab-com/gl-infra/ringctl/lib/patch"
)

const aValidAutoDeployVersion = "17.5.202409242201-d263ec23e08.a15b2f7c376"

func TestWithAPatch(t *testing.T) {
	version, err := gitlab.NewVersion(aValidAutoDeployVersion)
	require.NoError(t, err)
	require.NotNil(t, version)

	relatedTo := []string{"https://gitlab.com/gitlab-org/gitlab/-/issues/123456"}

	client := patch.NewMockPatchUploader(t)

	client.EXPECT().AddPatch(mock.Anything, mock.Anything).RunAndReturn(func(ctx context.Context, m *patch.Metadata) error {
		_, err := ulid.Parse(m.ID)
		require.NoError(t, err, "not a valid ulid")

		assert.Equal(t, patch.DefaultPriority, m.Priority)
		assert.Equal(t, 0, m.Ring)
		assert.Equal(t, relatedTo, m.RelatedTo)
		assert.True(t, m.OnlyGitlabUpgrade)
		assert.NotEmpty(t, m.Patch)
		assert.JSONEq(t, `[{"op":"replace","path":"/prerelease_version","value":"17.5.202409242201-d263ec23e08.a15b2f7c376"}]`, string(m.Patch))

		if assert.NotNil(t, m.PauseAfterRing) {
			assert.Equal(t, PerimeterRing, *m.PauseAfterRing)
		}

		return nil
	})

	err = WithAPatch(context.Background(), version, relatedTo, client)
	assert.NoError(t, err)
}
